import React, {useState} from 'react';
import {GlobalProvider} from './src/context/GlobalContext';
import AppNavigation from './src/navigation/index';
import {NavigationContainer} from '@react-navigation/native';
import {navigationRef} from './src/navigation/RootNavigator';
import {NativeBaseProvider} from 'native-base';
import {Block, GalioProvider} from 'galio-framework';
import {Images, articles, argonTheme} from './src/constants';

export default function App() {
  return (
    <GlobalProvider>
      <GalioProvider theme={argonTheme}>
        <NativeBaseProvider>
          <NavigationContainer ref={navigationRef}>
            <AppNavigation />
          </NavigationContainer>
        </NativeBaseProvider>
      </GalioProvider>
    </GlobalProvider>
  );
}
