import React, {useState, useEffect} from 'react';
import ConfirmMnemonic from '../../components/Mnemonic/Confirm';
import {useGlobalContext} from '../../context/GlobalContext';
import Loading from '../../components/common/Loading';
import Footer from '../../components/common/Footer';
import {shuffle} from '../../utils/Array';
import {storeMnemonic} from '../../context/action/Mnemonic/storeMnemonic';
import {INIT_NAVIGATOR} from '../../constants/routeName';
import { argonTheme } from '../../constants';

const ConfirmScreen = ({navigation}) => {
  const {
    mnemonicState: {words, mnemonic, randomString},
    mnemonicDispatch,
  } = useGlobalContext();
  const [shuffleWords, setShuffleWords] = useState([]);
  const [chosenWords, setChosenWords] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    const _shuffle_words = shuffle(words);
    setShuffleWords(_shuffle_words.map((word, index) => ({...word, index})));
    setChosenWords(() =>
      words.map(word => ({
        ...word,
        id: word.id,
        value: '',
        orderValue: word.value,
      })),
    );
    setLoading(false);
  }, []);

  const onFinish = () => {
    setLoading(true);
    storeMnemonic({mnemonic, randomString})(mnemonicDispatch);
    // createWallet({randomString})(walletDispatch);
    navigation.navigate(INIT_NAVIGATOR);
  };

  return (
    <>
      {loading ? (
        <Loading size={'large'} color={argonTheme.COLORS.PRIMARY} />
      ) : (
        <>
          <ConfirmMnemonic
            mnemonic={mnemonic}
            words={words}
            navigation={navigation}
            shuffleWords={shuffleWords}
            setShuffleWords={setShuffleWords}
            chosenWords={chosenWords}
            setChosenWords={setChosenWords}
            onFinish={onFinish}
          />

          <Footer />
        </>
      )}
    </>
  );
};

export default ConfirmScreen;
