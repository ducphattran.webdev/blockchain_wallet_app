import React, {useState, useEffect} from 'react';
import InitMnemonic from '../../components/Mnemonic/Init';
import {generate} from '../../context/action/Mnemonic/generate';
import {useGlobalContext} from '../../context/GlobalContext';
import Loading from '../../components/common/Loading';
import Footer from '../../components/common/Footer';
import {Alert} from 'react-native';

const InitScreen = ({navigation}) => {
  const {mnemonicState, mnemonicDispatch} = useGlobalContext();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (mnemonicState.words.length === 0 && mnemonicState.isInit) {
      generate()(mnemonicDispatch);
    }
  }, []);

  useEffect(() => {
    if (mnemonicState.done && mnemonicState.words.length !== 0) {
      if (mnemonicState.error) {
        Alert.alert('Error', 'Failed to get mnemonic data', [
          {text: 'OK', onPress: () => {}},
        ]);
      }
      setLoading(false);
    }
  }, [mnemonicState]);

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          <InitMnemonic
            mnemonic={mnemonicState.mnemonic}
            words={mnemonicState.words}
            navigation={navigation}
          />

          <Footer />
        </>
      )}
    </>
  );
};

export default InitScreen;
