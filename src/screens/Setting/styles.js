import {StyleSheet} from 'react-native';
import { argonTheme } from '../../constants';

export default StyleSheet.create({
  line_container: {
    paddingVertical: 10,
    borderBottomColor: argonTheme.COLORS.GRAY,
    borderBottomWidth: 0.5,
  },
  line_wrapper: {padding: 10, flexDirection: 'row'},
  btn_text: {marginLeft: 5, color: argonTheme.COLORS.ERROR},
});
