import {StyleSheet} from 'react-native';
import { argonTheme } from '../../../constants';

const borderBottom = {
  borderBottomColor: argonTheme.COLORS.GRAY_300,
  borderBottomWidth: 0.5,
};

export default StyleSheet.create({
  container: {
    flex: 0.5,
    width: '90%',
    backgroundColor: argonTheme.COLORS.WHITE,
    padding: 20,
    borderRadius: 10,
  },
  wrapper: {flex: 1, padding: 0},
  header_container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    ...borderBottom,
  },
  title_container: {
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    ...borderBottom,
  },
  title: {
    textAlign: 'center',
    textAlignVertical: 'center',
    fontWeight: 'bold',
    marginLeft: 5,
  },
  network_container: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    ...borderBottom,
  },
  current_icon_container: {
    justifyContent: 'center',
    width: 15,
    height: 15,
  },
  network_color: {
    marginHorizontal: 10,
    marginVertical: 5,
    backgroundColor: argonTheme.COLORS.WARNING,
    width: 10,
    height: 10,
    borderRadius: 10,
  },
  current_name: {textAlign: 'left', textAlignVertical: 'center'},
  others_title_container: {
    paddingBottom: 10,
    paddingLeft: 10,
    marginTop: 10,
    ...borderBottom,
  },
  others_title: {
    textAlign: 'left',
    textAlignVertical: 'center',
    fontSize: 12,
    fontWeight: 'bold',
  },
  btn_wrapper: {
    flex: 1,
    marginTop: 15,
    backgroundColor: argonTheme.COLORS.BLUE,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
  },
  btn_text:{color: argonTheme.COLORS.WHITE, fontWeight: 'bold'},
  form_title:{
    fontWeight: 'bold',
    fontSize: 15,
    color: argonTheme.COLORS.BLUE_100,
    textAlign: 'center',
  },
  form_description: {marginTop: 5, fontSize: 12},
  input_container:{padding: 5, marginTop: 10, marginBottom: 2},
  input: {
    fontSize: 12,
    borderWidth: 1,
    marginTop: 5,
    borderRadius: 5,
    paddingHorizontal: 10,
  },
  input_label: {fontWeight: '700'}
});
