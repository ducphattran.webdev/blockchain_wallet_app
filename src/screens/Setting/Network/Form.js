import React, {useState} from 'react';
import {useEffect} from 'react';
import {View} from 'react-native';
// import {Input} from 'native-base';
import {useGlobalContext} from '../../../context/GlobalContext';
import styles from './styles';
import Loading from '../../../components/common/Loading';
import Container from '../../../components/common/Container';
import {addNetwork} from '../../../context/action/Network/addNetwork';
import {
  SETTING_NETWORK,
  SETTING_NETWORK_LIST,
} from '../../../constants/routeName';
import {editNetwork} from '../../../context/action/Network/editNetwork';
import Input from '../../../components/common/Input';
import Button from '../../../components/common/Button';
import {argonTheme} from '../../../constants';
import {Center, Text} from 'native-base';

const Form = ({navigation, route}) => {
  const {networkState, networkDispatch} = useGlobalContext();
  const {network} = route.params;
  const [loading, setLoading] = useState(true);
  const [name, setName] = useState('');
  const [rpc, setRPC] = useState('');
  const [chainID, setChainID] = useState('');
  const [blockExplorer, setBlockExplorer] = useState('');
  const [errors, setErrors] = useState([]);

  const form = {
    title: !network && 'New RPC Network',
    description:
      !network &&
      'Use a custom RPC-capable network via URL instead of one of the provided networks',
  };

  useEffect(() => {
    navigation.setOptions({
      title: 'Networks',
      headerShown: true,
      headerTitleStyle: {
        fontSize: 14,
      },
      headerBackTitleStyle: {
        fontSize: 12,
      },
    });

    if (network) {
      setName(network.name);
      setRPC(network.RPC);
      setBlockExplorer(network.explorer);
      setChainID(network.id.toString());
    }
  }, []);

  useEffect(() => {
    setLoading(true);
    if (networkState.done) {
      setLoading(false);
    }
  }, [networkState.done]);

  const handleSubmitting = () => {
    if (network) {
      if (
        name !== network.name ||
        rpc !== network.RPC ||
        blockExplorer !== network.explorer ||
        chainID !== network.id.toString()
      ) {
        // Update
        editNetwork({
          id: network.id,
          name,
          blockExplorer,
          rpc,
        })(networkDispatch);
        return navigation.navigate(SETTING_NETWORK, {
          screen: SETTING_NETWORK_LIST,
        });
      }
      return setLoading(false);
    } else {
      const isProvided = validateRequiredInputs();
      if (isProvided) {
        addNetwork({
          name,
          chainID: Number.isNaN(parseInt(chainID))
            ? Math.floor(Math.random() * 100)
            : parseInt(chainID),
          blockExplorer,
          rpc,
        })(networkDispatch);
        return navigation.navigate(SETTING_NETWORK, {
          screen: SETTING_NETWORK_LIST,
        });
      }
    }
  };

  const validateRequiredInputs = () => {
    let errs = [];
    if (!name) {
      errs.push('name');
    }
    if (!rpc) {
      errs.push('rpc');
    }
    if (!blockExplorer) {
      errs.push('blockExplorer');
    }
    if (!chainID) {
      errs.push('chainID');
    }

    setErrors(prevErrors => [...prevErrors, ...errs]);
    return errs.length === 0;
  };

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          <Container>
            <View style={[styles.container, {width: '100%'}]}>
              <View style={styles.wrapper}>
                <Container style={[{flex: 1, padding: 0}]}>
                  {/* Title */}
                  {form.title && form.description && (
                    <>
                      <Text style={styles.form_title}>{form.title}</Text>
                      <Text style={styles.form_description}>
                        {form.description}
                      </Text>
                    </>
                  )}

                  {/* Network Name */}
                  <View style={styles.input_container}>
                    <Text style={styles.input_label}>Network Name</Text>
                    <Input
                      error={errors.includes('name')}
                      placeholder={'Network Name'}
                      blurOnSubmit={true}
                      onChangeText={val => setName(val)}
                      maxLength={30}
                      style={styles.input}
                      defaultValue={name}
                    />
                  </View>

                  {/* RPC URL */}
                  <View
                    style={[
                      styles.input_container,
                      {padding: 5, marginVertical: 2},
                    ]}>
                    <Text style={{fontWeight: '700'}}>RPC URL</Text>
                    <Input
                      error={errors.includes('rpc')}
                      textContentType={'URL'}
                      placeholder={'RPC URL'}
                      style={styles.input}
                      onChangeText={val => setRPC(val)}
                      defaultValue={rpc}
                    />
                  </View>

                  {/* Chain ID */}
                  <View
                    style={[
                      styles.input_container,
                      {padding: 5, marginVertical: 2},
                    ]}>
                    <Text style={{fontWeight: '700'}}>Chain ID</Text>
                    <Input
                      error={errors.includes('chainID')}
                      placeholder={'Chain ID'}
                      style={styles.input}
                      onChangeText={val => setChainID(val)}
                      maxLength={4}
                      defaultValue={chainID}
                    />
                  </View>
                  {/* Block explorer */}
                  <View
                    style={[
                      styles.input_container,
                      {padding: 5, marginVertical: 2},
                    ]}>
                    <Text style={{fontWeight: '700'}}>Block Explorer</Text>
                    <Input
                      error={errors.includes('blockExplorer')}
                      textContentType={'URL'}
                      placeholder={'Block Explorer (optional)'}
                      blurOnSubmit={true}
                      style={styles.input}
                      onChangeText={val => setBlockExplorer(val)}
                      defaultValue={blockExplorer}
                    />
                  </View>
                </Container>
              </View>
            </View>
            {/* Button */}
            <Center flex={1} m={3}>
              <Button
                onPress={() => handleSubmitting()}
                style={{width: '100%'}}
                opacity={0.5}
                bgColor={argonTheme.COLORS.PRIMARY}>
                <Text color={argonTheme.COLORS.WHITE}>
                  {!network ? 'Add' : 'Update'}
                </Text>
              </Button>
            </Center>
          </Container>
        </>
      )}
    </>
  );
};

export default Form;
