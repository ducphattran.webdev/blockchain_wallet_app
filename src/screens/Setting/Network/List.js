import React, {useState} from 'react';
import {useEffect} from 'react';
import {View} from 'react-native';
import {Center, Text} from 'native-base';
import {useGlobalContext} from '../../../context/GlobalContext';
import styles from './styles';
import NetworkItem from '../../../components/List/Network/network';
import Loading from '../../../components/common/Loading';
import Footer from '../../../components/common/Footer';
import Container from '../../../components/common/Container';
import {
  SETTING_NETWORK,
  SETTING_NETWORK_FORM,
} from '../../../constants/routeName';
import Button from '../../../components/common/Button';
import {argonTheme} from '../../../constants';

const List = ({navigation}) => {
  const {networkState} = useGlobalContext();
  const [loading, setLoading] = useState(true);
  const [networks, setNetworks] = useState([]);
  const [selectedNet, setSelectedNet] = useState(null);

  useEffect(() => {
    navigation.setOptions({
      title: 'Setting',
      headerShown: true,
      headerTitleStyle: {
        fontSize: 14,
      },
      headerBackTitle: '',
      headerBackTitleStyle: {
        fontSize: 12,
      },
    });
  }, []);

  useEffect(() => {
    setLoading(true);
    if (networkState.done) {
      setNetworks(networkState.networks);
      setSelectedNet(networkState.networks.find(n => n.current));
      setLoading(false);
    }
  }, [networkState.done]);

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          <Container>
            <View style={[styles.container, {width: '100%'}]}>
              <View style={styles.wrapper}>
                <Container style={[{flex: 1, padding: 0}]}>
                  {/* Current network */}
                  <NetworkItem
                    network={selectedNet}
                    color={selectedNet.color}
                    style={{borderBottomWidth: 0}}
                    showCurrent={true}
                    onPress={() =>
                      navigation.navigate(SETTING_NETWORK, {
                        screen: SETTING_NETWORK_FORM,
                        params: {network: selectedNet},
                      })
                    }
                  />

                  {/* Others */}
                  <View style={styles.others_title_container}>
                    <Text style={styles.others_title}>Other Networks</Text>
                  </View>

                  {networks.map(
                    network =>
                      network.id !== selectedNet.id && (
                        <NetworkItem
                          key={network.id}
                          network={network}
                          color={network.color}
                          style={{borderBottomWidth: 0}}
                          onPress={() =>
                            navigation.navigate(SETTING_NETWORK, {
                              screen: SETTING_NETWORK_FORM,
                              params: {network},
                            })
                          }
                        />
                      ),
                  )}
                </Container>
              </View>
            </View>
            
            {/* Button */}
            <Center flex={1} m={3}>
              <Button
                onPress={() =>
                  navigation.navigate(SETTING_NETWORK, {
                    screen: SETTING_NETWORK_FORM,
                    params: {title: 'Add Network'},
                  })
                }
                style={{width: '100%'}}
                opacity={0.5}
                bgColor={argonTheme.COLORS.PRIMARY}>
                <Text color={argonTheme.COLORS.WHITE}>Add Network</Text>
              </Button>
            </Center>
          </Container>

          <Footer />
        </>
      )}
    </>
  );
};

export default List;
