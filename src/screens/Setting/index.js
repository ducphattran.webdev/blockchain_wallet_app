import React, {useState} from 'react';
import {Alert, TouchableOpacity} from 'react-native';
import Footer from '../../components/common/Footer';
import PinModal from '../../components/Modal/PIN';
import {SETTING_NETWORK, AUTH_NAVIGATOR, LOGIN} from '../../constants/routeName';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import logout from '../../context/action/Auth/logout';
import {useGlobalContext} from '../../context/GlobalContext';
import {argonTheme} from '../../constants';
import {Block, Icon} from 'galio-framework';
import {Box, Text} from 'native-base';

const SettingScreen = ({navigation}) => {
  const {authDispatch} = useGlobalContext();
  const [showModal, setShowModal] = useState(false);

  const handleLogout = () => {
    Alert.alert('Warning', 'Are you sure you want to logout', [
      {
        text: 'No',
        onPress: () => {},
      },
      {
        text: 'Yes',
        onPress: () => {
          logout()(authDispatch);
          navigation.navigate(AUTH_NAVIGATOR, {screen: LOGIN});
        },
      },
    ]);
  };

  return (
    <>
      <Box flex={1} p={3}>
        {/* Change PIN code */}
        <Box
          p={2}
          my={2}
          borderBottomWidth={1}
          borderBottomColor={argonTheme.COLORS.GRAY_300}>
          <TouchableOpacity onPress={() => setShowModal(true)}>
            <Block row style={{paddingTop: 7}}>
              <MaterialIcon
                size={20}
                name="fiber-pin"
                family="entypo"
                style={{paddingRight: 5}}
              />
              <Text fontSize={15}>Change PIN code</Text>
            </Block>
          </TouchableOpacity>
        </Box>

        {/* Logout */}
        <Box
          p={2}
          my={2}
          borderBottomWidth={1}
          borderBottomColor={argonTheme.COLORS.GRAY_300}>
          <TouchableOpacity onPress={handleLogout}>
            <Block row style={{paddingTop: 7}}>
              <MaterialIcon
                size={20}
                name="logout"
                family="entypo"
                color={argonTheme.COLORS.ERROR}
                style={{paddingRight: 5}}
              />
              <Text fontSize={15} color={argonTheme.COLORS.ERROR}>
                Logout
              </Text>
            </Block>
          </TouchableOpacity>
        </Box>

        {/* Networks */}
        <Box
          p={2}
          my={2}
          borderBottomWidth={1}
          borderBottomColor={argonTheme.COLORS.GRAY_300}>
          <TouchableOpacity
            onPress={() => navigation.navigate(SETTING_NETWORK)}>
            <Block row middle space="between" style={{paddingTop: 7}}>
              <Text fontSize={15}>Networks</Text>
              <Icon
                size={20}
                name="chevron-right"
                family="entypo"
                style={{paddingRight: 5}}
              />
            </Block>
          </TouchableOpacity>
        </Box>
      </Box>
      <Footer />

      {/* Modal */}
      <PinModal visible={showModal} setShowModal={setShowModal} />
    </>
  );
};

export default SettingScreen;
