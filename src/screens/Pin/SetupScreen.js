import React, {useState, useEffect} from 'react';
import {View, Vibration} from 'react-native';
import PinCode from '../../components/PinCode/index';
import Block from '../../components/PinCode/Block';
import Loading from '../../components/common/Loading';
import {useGlobalContext} from '../../context/GlobalContext';
import {blockAuth, getAuth} from '../../context/action/Auth/auth';
import {pinInitStates} from '../../context/reducer/pinReducer';
import { argonTheme } from '../../constants';

/**
 *  SetupState: {
 *    -1: Blocked,
 *     0: INCORRECT,
 *     1: Input Pin code
 *     2: Confirm Pin code
 * }
 * */
const STATES = pinInitStates.STATES;
const {BLOCK_ATTEMPT_TIME, BLOCK_MAX_ATTEMPTS} = pinInitStates;
const SetupScreen = ({handleSubmit}) => {
  const {pinState, authState, authDispatch} = useGlobalContext();
  const [setupState, setSetupState] = useState(STATES.INPUT);
  const [pinCode, setPinCode] = useState('');
  const [confirmPinCode, setConfirmPinCode] = useState('');
  const [attempts, setAttempts] = useState(0);
  const [errorMsg, setErrorMsg] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getAuth()(authDispatch);
  }, []);

  useEffect(() => {
    if (authState.done && pinState.done) {
      if (authState.blocked) {
        setSetupState(STATES.BLOCKED);
      }
      setLoading(false);
    }
  }, [authState.done, pinState.done]);

  useEffect(() => {
    if (setupState === STATES.INPUT && pinCode.length === pinState.length) {
      return setSetupState(STATES.CONFIRM);
    }
    if (
      setupState === STATES.CONFIRM &&
      confirmPinCode.length === pinState.length
    ) {
      return onFinish();
    }
  }, [pinCode, confirmPinCode]);

  useEffect(() => {
    // Block
    if (authState.blocked || setupState === STATES.BLOCKED) {
      return blockEntering();
    }
  }, [setupState]);

  useEffect(() => {
    if (setupState === STATES.BLOCKED) {
      const timerInterval = setInterval(() => {
        if (authState.blocked) {
          // Count down
          if (authState.block_in === 0) {
            blockAuth({block_in: 0, blocked: false})(authDispatch);
            setSetupState(STATES.INPUT);
          } else {
            blockAuth({
              block_in: authState.block_in - 1000,
              blocked: authState.blocked,
            })(authDispatch);
          }
        }
      }, 1000);

      return () => {
        clearInterval(timerInterval);
      };
    }
  });

  const blockEntering = () => {
    setSetupState(STATES.BLOCKED);
    setPinCode('');
    setConfirmPinCode('');
    setAttempts(0);
    setErrorMsg(null);

    if (!authState.blocked) {
      blockAuth({block_in: BLOCK_ATTEMPT_TIME, blocked: true})(authDispatch);
    } else {
      blockAuth({
        block_in: authState.block_in !== 0 ? authState.block_in : 0,
        blocked: authState.block_in !== 0,
      })(authDispatch);
    }
    setLoading(false);
  };

  const displayError = () => {
    setErrorMsg({
      title: 'Your entries did not match',
      message: `Please try again. You have ${
        BLOCK_MAX_ATTEMPTS - attempts - 1
      } times to try`,
    });
    setSetupState(STATES.INCORRECT);
    Vibration.vibrate();
    setLoading(false);

    // Delay to show error message
    setTimeout(() => {
      setPinCode('');
      setConfirmPinCode('');
      setAttempts(prevAttempt => prevAttempt + 1);
      setSetupState(STATES.INPUT);
      setErrorMsg(null);
    }, 1000);
  };

  const onFinish = () => {
    setLoading(true);
    // Incorrect
    if (pinCode.toString() !== confirmPinCode.toString()) {
      // Block
      if (attempts === BLOCK_MAX_ATTEMPTS - 1) {
        return blockEntering();
      }

      // Display error
      return displayError();
    }

    // Success
    setAttempts(0);
    setErrorMsg(null);
    handleSubmit(pinCode);
  };

  return (
    <View style={{flex: 1}}>
      {loading ? (
        <Loading size={'large'} color={argonTheme.COLORS.PRIMARY} />
      ) : (
        <>
          {/* [1] - Enter */}
          {setupState === STATES.INPUT && (
            <PinCode
              title={`${setupState} - Setup PIN code`}
              message={'to keep your information secure'}
              messageType={''}
              length={pinState.length}
              setPinCode={setPinCode}
              pinCode={pinCode}
            />
          )}
          {/* [2] - Confirm */}
          {setupState === STATES.CONFIRM && !errorMsg && (
            <PinCode
              title={
                errorMsg ? errorMsg.title : `${setupState} - Confirm PIN code`
              }
              message={errorMsg && errorMsg.message}
              messageType={errorMsg ? 'errorMsg' : ''}
              length={pinState.length}
              setPinCode={setConfirmPinCode}
              pinCode={confirmPinCode}
            />
          )}

          {/* [0] - Error */}
          {setupState === STATES.INCORRECT && (
            <PinCode
              title={errorMsg.title}
              message={errorMsg.message}
              messageType={'error'}
              length={pinState.length}
              setPinCode={null}
              pinCode={undefined}
            />
          )}

          {/* // [-1] - Block */}
          {setupState === STATES.BLOCKED && (
            <Block time={authState.block_in} blocked={authState.blocked} />
          )}
        </>
      )}
    </View>
  );
};

export default SetupScreen;
