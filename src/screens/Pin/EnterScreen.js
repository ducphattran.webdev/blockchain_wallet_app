import React, {useState, useEffect} from 'react';
import {View, Platform, Vibration, Alert} from 'react-native';
import PinCode from '../../components/PinCode/index';
import Loading from '../../components/common/Loading';
import Block from '../../components/PinCode/Block';
import {useGlobalContext} from '../../context/GlobalContext';
import {blockAuth, getAuth} from '../../context/action/Auth/auth';
import {pinInitStates} from '../../context/reducer/pinReducer';
import {
  checkAvailableBiometrics,
  promptBiometrics,
} from '../../utils/Biometrics';
/**
 *  EnterState: {
 *    -1: Blocked,
 *     0: INCORRECT,
 *     1: Input Pin code
 * }
 * */
const STATES = pinInitStates.STATES;
const {BLOCK_ATTEMPT_TIME, BLOCK_MAX_ATTEMPTS} = pinInitStates;
const EnterScreen = ({handleSubmit, route}) => {
  const {pinState, authState, authDispatch} = useGlobalContext();
  const [enterState, setEnterState] = useState(STATES.INPUT);
  const [pinCode, setPinCode] = useState('');
  const [attempts, setAttempts] = useState(0);
  const [errorMsg, setErrorMsg] = useState(null);
  const [loading, setLoading] = useState(true);
  const [authWithBiometrics, setAuthWithBiometrics] = useState(false);

  useEffect(() => {
    setLoading(true);
    getAuth()(authDispatch);

    if (
      !route ||
      (route && !route.params) ||
      (route && route.params && !route.params.biometricsOff)
    ) {
      handleBiometrics();
    }
  }, []);

  useEffect(() => {
    if (authWithBiometrics) {
      handleBiometrics();
    }
  }, [authWithBiometrics]);

  useEffect(() => {
    if (authState.done && pinState.done) {
      if (authState.blocked) {
        return setEnterState(STATES.BLOCKED);
      }

      setLoading(false);
    }
  }, [authState.done, pinState.done]);

  useEffect(() => {
    if (pinCode.length === pinState.length) {
      setLoading(true);
      onFinish();
    }
  }, [pinCode]);

  useEffect(() => {
    if (enterState === STATES.INPUT) {
      setPinCode('');
    }
    // Block
    if (authState.blocked || enterState === STATES.BLOCKED) {
      blockEntering();
    }
  }, [enterState]);

  useEffect(() => {
    if (enterState === STATES.BLOCKED) {
      const timerInterval = setInterval(() => {
        if (authState.blocked) {
          // Count down
          if (authState.block_in === 0) {
            blockAuth({block_in: 0, blocked: false})(authDispatch);
            setEnterState(STATES.INPUT);
          } else {
            blockAuth({
              block_in: authState.block_in - 1000,
              blocked: authState.blocked,
            })(authDispatch);
          }
        }
      }, 1000);

      return () => {
        clearInterval(timerInterval);
      };
    }
  });

  const blockEntering = () => {
    setEnterState(STATES.BLOCKED);
    if (!authState.blocked) {
      blockAuth({block_in: BLOCK_ATTEMPT_TIME, blocked: true})(authDispatch);
    } else {
      blockAuth({
        block_in: authState.block_in !== 0 ? authState.block_in : 0,
        blocked: authState.block_in !== 0,
      })(authDispatch);
    }
    setPinCode('');
    setAttempts(0);
    setErrorMsg(null);
    setLoading(false);
  };

  const displayError = () => {
    setErrorMsg({
      title: 'Incorrect PIN code',
      message: `Please try again. You have ${
        BLOCK_MAX_ATTEMPTS - attempts - 1
      } times to try`,
    });
    setEnterState(STATES.INCORRECT);
    Vibration.vibrate();
    setLoading(false);

    // Delay to show error message
    setTimeout(() => {
      setPinCode('');
      setAttempts(prevAttempt => prevAttempt + 1);
      setEnterState(STATES.INPUT);
      setErrorMsg(null);
    }, 1000);
  };

  const onFinish = () => {
    if (pinState.PIN !== pinCode.toString()) {
      // Block
      if (attempts === BLOCK_MAX_ATTEMPTS - 1) {
        return blockEntering();
      }

      // Display error
      return displayError();
    }

    // Success
    setPinCode('');
    setAttempts(0);
    setErrorMsg(null);
    handleSubmit();
  };

  const handleBiometrics = async () => {
    const available = await checkAvailableBiometrics({OS: Platform.OS});
    if (!available && authWithBiometrics) {
      return Alert.alert('Error', 'Please enable FaceID for authentication!', [
        {text: 'OK', onPress: () => setAuthWithBiometrics(false)},
      ]);
    }

    if (available && authWithBiometrics) {
      promptBiometrics({
        handler: () => {
          setLoading(true);
          setPinCode(pinState.PIN);
        },
      });
    }
  };

  // const authWithBiometrics = () => {
  //   ReactNativeBiometrics.isSensorAvailable().then(resultObject => {
  //     const {available, biometryType} = resultObject;
  //     const {OS} = Platform;
  //     switch (OS) {
  //       case 'android': {
  //         if (available && biometryType === ReactNativeBiometrics.Biometrics) {
  //           // Prompt
  //           ReactNativeBiometrics.simplePrompt({
  //             promptMessage: 'Confirm with biometrics',
  //           })
  //             .then(resultObject => {
  //               const {success} = resultObject;

  //               if (success) {
  //                 setLoading(true);
  //                 setPinCode(pinState.PIN);
  //               }
  //             })
  //             .catch(e => console.log(e));
  //         } else {
  //           // Not supported
  //           console.log('Biometrics is not supported');
  //         }
  //         break;
  //       }
  //       case 'ios': {
  //         if (available && biometryType === ReactNativeBiometrics.TouchID) {
  //           console.log('TouchID is supported');
  //           // Prompt
  //           ReactNativeBiometrics.simplePrompt({
  //             promptMessage: 'Confirm with biometrics',
  //           })
  //             .then(resultObject => {
  //               const {success} = resultObject;

  //               if (success) {
  //                 setLoading(true);
  //                 setPinCode(pinState.PIN);
  //               }
  //             })
  //             .catch(e => console.log(e));
  //         } else if (
  //           available &&
  //           biometryType === ReactNativeBiometrics.FaceID
  //         ) {
  //           console.log('FaceID is supported');
  //           // Prompt
  //           ReactNativeBiometrics.simplePrompt({
  //             promptMessage: 'Confirm with biometrics',
  //           })
  //             .then(resultObject => {
  //               const {success} = resultObject;

  //               if (success) {
  //                 setLoading(true);
  //                 setPinCode(pinState.PIN);
  //               }
  //             })
  //             .catch(e => console.log(e));
  //         }
  //         break;
  //       }
  //       default:
  //         console.log('No support')
  //         break;
  //     }
  //   });
  // };

  return (
    <View style={{flex: 1}}>
      {loading ? (
        <Loading />
      ) : (
        <>
          {/* [1] - Enter */}
          {enterState === STATES.INPUT && (
            <PinCode
              title={`Enter PIN code`}
              message={''}
              messageType={''}
              length={pinState.length}
              setPinCode={setPinCode}
              pinCode={pinCode}
              authWithBiometrics={
                route && route.params && route.params.biometricsOff
                  ? undefined
                  : setAuthWithBiometrics
              }
            />
          )}
          {/* [1] - Error */}
          {enterState === STATES.INCORRECT && (
            <PinCode
              title={errorMsg.title}
              message={errorMsg.message}
              messageType={'error'}
              length={pinState.length}
              setPinCode={null}
              pinCode={undefined}
            />
          )}

          {/* // [-1] - Block */}
          {enterState === -1 && (
            <Block time={authState.block_in} blocked={authState.blocked} />
          )}
        </>
      )}
    </View>
  );
};

export default EnterScreen;
