import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Loading from '../../components/common/Loading';
import ImportWalletModal from '../../components/Modal/ImportWallet';
import {useGlobalContext} from '../../context/GlobalContext';
import {navigate, navigationRef} from '../../navigation/RootNavigator';
import {HOME_NAVIGATOR, TAB_NAVIGATOR} from '../../constants/routeName';
import {createWallet} from '../../context/action/Wallet/createWallet';
import Button from '../../components/common/Button';
import {argonTheme} from '../../constants';

const Init = () => {
  const {mnemonicState, walletState, walletDispatch} = useGlobalContext();
  const [loading, setLoading] = useState(true);
  const [showImportModal, setShowImportModal] = useState(false);

  useEffect(() => {
    if (walletState.done && walletState.wallets.length !== 0) {
      setLoading(false);
      navigate(HOME_NAVIGATOR, {screen: TAB_NAVIGATOR});
    }
  }, [walletState]);

  useEffect(() => {
    if (mnemonicState.done) {
      setLoading(false);
    }
  }, [mnemonicState]);

  const handleCreateNew = () => {
    createWallet({randomString: mnemonicState.randomString})(walletDispatch);
    navigate(HOME_NAVIGATOR, {screen: TAB_NAVIGATOR});
  };

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <View style={styles.container}>
          <Button onPress={() => handleCreateNew()} color={'primary'}>
            <Text style={{color: argonTheme.COLORS.WHITE}}>
              Create new wallet
            </Text>
          </Button>

          <Button onPress={() => setShowImportModal(true)} color={'primary'}>
            <Text style={{color: argonTheme.COLORS.WHITE}}>
              Import by Private Key
            </Text>
          </Button>

          {/* Modal */}
          <ImportWalletModal
            setShowImportModal={setShowImportModal}
            visible={showImportModal}
          />
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
});

export default Init;
