import React, {useEffect} from 'react';
import Loading from '../../components/common/Loading';
import {useGlobalContext} from '../../context/GlobalContext';
import logout from '../../context/action/Auth/logout';

const Logout = ({navigation}) => {
  const {authDispatch, authState} = useGlobalContext();

  useEffect(() => {
    if (authState.done && !authState.isLoggedIn) {
      logout()(authDispatch);
    }
  }, []);

  return <Loading />;
};

export default Logout;
