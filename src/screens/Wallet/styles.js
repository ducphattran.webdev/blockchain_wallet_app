import {StyleSheet} from 'react-native';
import {argonTheme} from '../../constants';

export default StyleSheet.create({
  container: {
    backgroundColor: argonTheme.COLORS.WHITE,
    flex: 1,
    padding: 0,
  },
  header_container: {
    alignItems: 'center',
  },
  close_btn_container: {
    paddingRight: 10,
    alignItems: 'flex-end',
  },
  section_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
  },
  surface: {
    flex: 1,
    marginHorizontal: 1,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
