import React, {useEffect, useState} from 'react';
import {Text, Pressable, Alert, Linking, SafeAreaView} from 'react-native';
import Clipboard from '@react-native-clipboard/clipboard';
import {IconButton, Box} from 'native-base';
import styles from './styles';
import {useGlobalContext} from '../../context/GlobalContext';
import {decryptWithAES} from '../../utils/Encrypt';
import {deleteWallet} from '../../context/action/Wallet/deleteWallet';
import {updateInfo} from '../../context/action/Wallet/updateInfo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import PINModal from '../../components/Modal/PIN';
import AddTokenModal from '../../components/Modal/AddToken';
import CopiedModal from '../../components/Popup/Copied';

import InfoTab from '../../components/Wallet/Info';
import AssetsTab from '../../components/Wallet/Assets';
import TransactionTab from '../../components/Wallet/Transaction';
import {argonTheme} from '../../constants';
import Loading from '../../components/common/Loading';

const TABS = {
  Info: 1,
  Assets: 2,
  Transactions: 3,
};
const WalletDetail = ({navigation}) => {
  const {
    mnemonicState: {randomString},
    walletState,
    walletDispatch,
    networkState,
  } = useGlobalContext();
  const [privateKey, setPrivateKey] = useState(undefined);
  const [showPINModal, setShowPINModal] = useState(false);
  const [showAddTokenModal, setShowAddTokenModal] = useState(false);
  const [currentTab, setCurrentTab] = useState(TABS.Info);
  const [wallet, setWallet] = useState(undefined);
  const [loading, setLoading] = useState(true);
  const [showCopiedModal, setShowCopiedModal] = useState(false);

  useEffect(() => {
    if (walletState.done && walletState.wallet) {
      setWallet(walletState.wallet);
      setLoading(false);
    }
  }, [walletState]);

  useEffect(() => {
    if (showCopiedModal) {
      return setTimeout(() => setShowCopiedModal(false), 2000);
    }
  }, [showCopiedModal]);

  const decryptPrivateKey = () => {
    setShowPINModal(false);
    setPrivateKey(decryptWithAES(wallet.privateKey, randomString)[0]);
  };

  const handleDeleteWallet = () => {
    Alert.alert('Confirm', 'Are you sure you want to delete the wallet?', [
      {
        text: 'Cancel',
        onPress: () => {},
      },
      {
        text: 'Yes, delete it',
        onPress: () => {
          deleteWallet({address: wallet.address})(walletDispatch);
          // setShowViewModal(false);
        },
      },
    ]);
  };

  const handleUpdate = (name, group) => {
    updateInfo({name, address: wallet.address, groupId: group.id})(
      walletDispatch,
    );
    // setShowViewModal(false);
  };

  const handleViewWalletOnNetwork = () => {
    const currentNetwork = networkState.networks.find(n => n.current);
    if (currentNetwork) {
      Linking.openURL(`${currentNetwork.explorer}address/${wallet.address}`);
    } else {
      Alert.alert('Error', 'No explorer URL provided.', [
        {
          text: 'OK',
          onPress: () => {},
        },
      ]);
    }
  };

  const copyToClipboard = string => {
    Clipboard.setString(string);
    setShowCopiedModal(true);
  };

  return (
    <SafeAreaView flex={1} width="100%">
      {loading ? (
        <Loading />
      ) : (
        <>
          <Box flex={1}>
            {/* Header */}
            <Box>
              <IconButton
                onPress={() => navigation.goBack()}
                variant="solid"
                icon={
                  <MaterialCommunityIcons
                    name={'close-circle'}
                    color={argonTheme.COLORS.ERROR}
                    size={25}
                  />
                }
                bgColor="transparent"
                _pressed={{backgroundColor: 'red.200'}}
                p={1}
                m={1}
              />
              {/* Sections */}
              <Box flexDirection="row">
                {Object.keys(TABS).map(tab => (
                  <Pressable
                    key={tab}
                    onPress={() => setCurrentTab(TABS[tab])}
                    style={styles.section_wrapper}>
                    <Box
                      bgColor={
                        TABS[tab] === currentTab ? 'indigo.100' : 'gray.200'
                      }
                      style={[
                        styles.surface,
                        {
                          borderBottomWidth: TABS[tab] === currentTab ? 5 : 0,
                          borderBottomColor:
                            TABS[tab] === currentTab
                              ? argonTheme.COLORS.PRIMARY
                              : '',
                          elevation: TABS[tab] === currentTab ? 10 : 2,
                        },
                      ]}>
                      <Text
                        style={{
                          color:
                            TABS[tab] === currentTab
                              ? argonTheme.COLORS.PRIMARY
                              : argonTheme.COLORS.BLACK,
                          fontWeight: 'bold',
                        }}>
                        {tab}
                      </Text>
                    </Box>
                  </Pressable>
                ))}
              </Box>
            </Box>

            {/* Info  */}
            {currentTab === TABS.Info && (
              <InfoTab
                wallet={wallet}
                privateKey={privateKey}
                setShowPINModal={setShowPINModal}
                handleUpdate={handleUpdate}
                handleDeleteWallet={handleDeleteWallet}
                handleViewWalletOnNetwork={handleViewWalletOnNetwork}
                copyToClipboard={copyToClipboard}
              />
            )}

            {/* Assets  */}
            {currentTab === TABS.Assets && (
              <AssetsTab setShowAddModal={setShowAddTokenModal} />
            )}

            {/* Transaction  */}
            {currentTab === TABS.Transactions && (
              <TransactionTab
                setShowAddModal={setShowAddTokenModal}
                wallet={wallet}
              />
            )}
          </Box>

          {/* PIN Modal */}
          <PINModal
            visible={showPINModal}
            setShowModal={setShowPINModal}
            onlyVerify={true}
            onFinish={decryptPrivateKey}
          />

          {/* Add Token Modal */}
          <AddTokenModal
            visible={showAddTokenModal}
            setShowModal={setShowAddTokenModal}
            wallet={wallet}
          />

          {/* Copy Modal */}
          <CopiedModal
            transparent={true}
            visible={showCopiedModal}
            animationType={'fade'}
            text={'Copied'}
          />
        </>
      )}
    </SafeAreaView>
  );
};

export default WalletDetail;
