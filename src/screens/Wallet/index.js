import React, {useEffect, useState} from 'react';
import {ScrollView, Alert, SafeAreaView} from 'react-native';
import {IconButton, Box, VStack} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useGlobalContext} from '../../context/GlobalContext';
import WalletItem from '../../components/List/Wallet';
import Loading from '../../components/common/Loading';
import {checkValidAddress} from '../../utils/Web3/Utility';
import {addWalletToGroup} from '../../context/action/Wallet/addWalletToGroup';
// Modal
import GroupFormModal from '../../components/Modal/GroupForm';
import AddWalletModal from '../../components/Modal/AddWalletToGroup';
import ImportWalletModal from '../../components/Modal/ImportWallet';
import Footer from '../../components/common/Footer';
import ScannerModal from '../../components/Modal/Scanner';
import {getOneWallet} from '../../context/action/Wallet/getWallet';
import {navigate} from '../../navigation/RootNavigator';
import {WALLET_DETAIL, WALLET_NAVIGATOR} from '../../constants/routeName';
import {argonTheme} from '../../constants';

const Index = ({route, navigation}) => {
  const {walletState, walletDispatch} = useGlobalContext();
  const [loading, setLoading] = useState(true);
  const [showGroupForm, setShowGroupForm] = useState(false);
  const [showAddWalletModal, setShowAddWalletModal] = useState(false);
  const [showImportModal, setShowImportModal] = useState(false);
  const [showScannerModal, setShowScannerModal] = useState(false);
  const [formType, setFormType] = useState('add');
  const [group, setGroup] = useState(undefined);

  useEffect(() => {
    if (walletState.done) {
      setLoading(false);
    }
  }, [walletState]);

  const showUpdateGroupForm = _group => {
    setFormType('update');
    setGroup(_group);
    setShowGroupForm(true);
  };

  const showCreateGroupForm = () => {
    setFormType('add');
    setShowGroupForm(true);
  };

  const onAddWallet = _group => {
    setShowAddWalletModal(true);
    setGroup(_group);
  };

  const onViewWallet = w => {
    setLoading(true);
    getOneWallet({address: w.address})(walletDispatch);
    navigate(WALLET_NAVIGATOR, {screen: WALLET_DETAIL});
  };

  const handleScanning = async e => {
    setShowScannerModal(false);
    const address = e.data.includes(':') ? e.data.split(':')[1] : e.data;

    // Check valid address
    const [isValidAddress] = await checkValidAddress(address);
    if (isValidAddress) {
      addWalletToGroup({address, groupId: group.id})(walletDispatch);
    } else {
      Alert.alert('Error', 'Invalid address', [
        {text: 'OK', onPress: () => {}},
      ]);
    }
  };

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          <SafeAreaView flex={1}>
            <Box flex={1}>
              {/* Header */}
              <Box flexDirection="row" justifyContent="center">
                <IconButton
                  onPress={() => showCreateGroupForm()}
                  variant="solid"
                  icon={
                    <MaterialCommunityIcons
                      name={'plus-circle'}
                      size={28}
                      color={argonTheme.COLORS.PRIMARY}
                    />
                  }
                  p={1}
                  m={1}
                  bgColor="transparent"
                  _pressed={{opacity: 0.5}}
                />
              </Box>

              {/* Groups */}
              <ScrollView flex={1}>
                {walletState.groups.map((_group, index) => (
                  <VStack flex={1} key={index} width="100%">
                    <WalletItem
                      key={index}
                      group={_group}
                      items={walletState.wallets.filter(
                        w => w.groupId === _group.id,
                      )}
                      onAddWallet={onAddWallet}
                      onViewWallet={onViewWallet}
                      onUpdateGroup={showUpdateGroupForm}
                    />
                  </VStack>
                ))}
              </ScrollView>

              {/* GroupForm Modal */}
              <GroupFormModal
                visible={showGroupForm}
                setShowModal={setShowGroupForm}
                type={formType}
                group={formType === 'update' && group}
              />

              {/* AddWallet Modal */}
              <AddWalletModal
                visible={showAddWalletModal}
                setShowModal={setShowAddWalletModal}
                groupId={group && group.id}
                setShowImportModal={setShowImportModal}
                showScanner={setShowScannerModal}
              />

              {/* ImportWallet Modal */}
              <ImportWalletModal
                setShowImportModal={setShowImportModal}
                visible={showImportModal}
                groupId={group && group.id}
              />

              {/* Scanner Modal */}
              <ScannerModal
                visible={showScannerModal}
                setShowModal={setShowScannerModal}
                onSuccess={handleScanning}
              />
              <Footer flex={0.001} />
            </Box>
          </SafeAreaView>
        </>
      )}
    </>
  );
};

export default Index;
