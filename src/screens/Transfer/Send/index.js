import React from 'react';
import {View, Text, TextInput, Pressable} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Button from '../../../components/common/Button';
import {argonTheme} from '../../../constants';
import {Box, Center} from 'native-base';

const SendForm = ({
  fromWallet,
  toAddress,
  setToAddress,
  setShowWalletModal,
  setShowScannerModal,
  error,
  onNext,
  setSelectType,
}) => {
  const chooseFromWallet = () => {
    setSelectType('from');
    setShowWalletModal(true);
  };

  const chooseToWallet = () => {
    setSelectType('to');
    setShowWalletModal(true);
  };

  return (
    <View style={styles.form_container}>
      <Pressable onPress={chooseFromWallet} style={styles.form_field}>
        <Text style={styles.form_title}>From</Text>
        <View style={styles.form_input_wrapper}>
          <TextInput
            editable={false}
            style={styles.form_input}
            value={fromWallet ? fromWallet.address : ''}
          />
          <View style={styles.form_input_icon}>
            <Icon
              name={'arrow-drop-down'}
              size={25}
              color={argonTheme.COLORS.BLACK}
            />
          </View>
        </View>
      </Pressable>

      <View style={styles.form_field}>
        <Text style={styles.form_title}>To</Text>
        <View style={styles.form_input_wrapper}>
          <TextInput
            onChangeText={val => setToAddress(val)}
            blurOnSubmit={true}
            style={styles.form_input}
            // value={trimAddress(toAddress, 20)}
            value={toAddress}
          />
          <Pressable
            onPress={() => setShowScannerModal(true)}
            style={styles.form_input_icon}>
            <Icon
              name={'qr-code-scanner'}
              size={25}
              color={argonTheme.COLORS.black}
            />
          </Pressable>
        </View>
      </View>

      {/* Choose from groups */}
      <Pressable onPress={chooseToWallet}>
        <Text
          style={{
            fontSize: 12,
            color: argonTheme.COLORS.INFO,
            fontWeight: 'bold',
          }}>
          Choose a wallet from groups
        </Text>
      </Pressable>

      {/* Error */}
      <Box mt={3}>
        <Text style={{fontSize: 13, color: argonTheme.COLORS.ERROR}}>
          {error}
        </Text>
      </Box>

      {/* Next BTN */}
      <Center>
        <Button
          style={{width: '100%'}}
          onPress={onNext}
          bgColor={argonTheme.COLORS.PRIMARY}>
          <Text style={styles.form_btn_text}>Next</Text>
        </Button>
      </Center>
    </View>
  );
};

export default SendForm;
