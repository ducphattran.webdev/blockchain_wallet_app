import {StyleSheet} from 'react-native';
import {argonTheme} from '../../../constants';

export default StyleSheet.create({
  form_container: {
    flex: 1,
    backgroundColor: argonTheme.COLORS.GRAY_100,
    padding: 20,
  },
  form_field: {justifyContent: 'center', marginBottom: 10},
  form_input_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  form_input: {
    flex: 1,
    fontSize: 10,
    borderWidth: 0.5,
    padding: 10,
    borderRadius: 5,
    color: argonTheme.COLORS.BLACK,
    borderColor: argonTheme.COLORS.GRAY,
  },
  form_input_icon: {flex: 0.1, alignItems: 'flex-end'},
  form_btn: {
    backgroundColor: argonTheme.COLORS.BLUE,
    marginTop: 10,
    padding: 10,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  form_btn_text: {color: argonTheme.COLORS.WHITE, textTransform: 'uppercase'},
});
