import React from 'react';
import {View, Text, Pressable} from 'react-native';
import BigNumber from '../../../utils/BigNumber';
import styles from './styles';
import {trimAddress} from '../../../utils/Wallet';
import {Box, Center} from 'native-base';
import {argonTheme} from '../../../constants';
import Button from '../../../components/common/Button';

const ConfirmForm = ({
  fromWallet,
  toAddress,
  amount,
  gasFee,
  selectedToken,
  error,
  onNext,
  onPrevious,
}) => {
  return (
    <>
      <View style={styles.form_container}>
        <View style={styles.form_field}>
          <Text style={styles.form_title}>From</Text>
          <View style={styles.form_input_wrapper}>
            <Text style={styles.form_input}>
              {trimAddress(fromWallet.address, 20)}
            </Text>
          </View>
        </View>

        <View style={styles.form_field}>
          <Text style={styles.form_title}>To</Text>
          <View style={styles.form_input_wrapper}>
            <Text style={styles.form_input}>{trimAddress(toAddress, 20)}</Text>
          </View>
        </View>
      </View>

      {/* Amount */}
      <View style={styles.amount_container}>
        <Text style={styles.amount_title}>Amount</Text>
        <View style={styles.amount_wrapper}>
          <Text style={styles.amount_text}>
            {amount} {selectedToken.symbol}
          </Text>
        </View>
      </View>

      {/* Gas & Total */}
      <View style={styles.final_wrapper}>
        {/* Estimated Gas */}
        <View style={styles.gas_wrapper}>
          <Text style={styles.gas_title}>Estimated gas fee</Text>
          <View style={styles.gas_value_wrapper}>
            <Text style={styles.gas_value}>{`${gasFee.toString()} BNB`}</Text>
          </View>
        </View>

        {/* Total */}
        <View style={styles.total_container}>
          <Text style={styles.total_title}>Total</Text>
          <View style={styles.total_value_wrapper}>
            <Text style={styles.total_value}>
              {selectedToken.address
                ? `${amount.toString()} ${
                    selectedToken.symbol
                  } + ${gasFee.toString()} BNB`
                : `${new BigNumber(amount).plus(gasFee).toString()} BNB`}
            </Text>
          </View>
        </View>
      </View>

      {/* Error */}
      {error !== '' && (
        <Box p={1}>
          <Text style={styles.error_msg}>{error}</Text>
        </Box>
      )}

      {/* Buttons */}
      <Center p={1} flexDirection="row">
        {/* Cancel BTN */}
        <Button
          onPress={onPrevious}
          p={2}
          style={{backgroundColor: argonTheme.COLORS.GRAY_300}}>
          <Text style={[styles.form_btn_text, {color: argonTheme.COLORS.GRAY}]}>
            Back
          </Text>
        </Button>
        {/* Next BTN */}
        <Button onPress={onNext} p={2}>
          <Text style={styles.form_btn_text}>Send</Text>
        </Button>
      </Center>
    </>
  );
};

export default ConfirmForm;
