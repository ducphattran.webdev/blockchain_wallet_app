import {StyleSheet} from 'react-native';
import {argonTheme} from '../../../constants';

export default StyleSheet.create({
  form_container: {flex: 1, padding: 20},
  form_field: {justifyContent: 'center', marginBottom: 10},
  form_input_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  form_input: {
    flex: 1,
    fontSize: 12,
    borderWidth: 0.5,
    padding: 10,
    borderRadius: 5,
    color: argonTheme.COLORS.BLACK,
    borderColor: argonTheme.COLORS.GRAY,
  },
  form_input_icon: {flex: 0.1, alignItems: 'flex-end'},
  form_btn: {
    flex: 1,
    margin: 10,
    marginTop: 10,
    padding: 10,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  form_btn_text: {color: argonTheme.COLORS.WHITE, textTransform: 'uppercase'},
  form_next_btn: {backgroundColor: argonTheme.COLORS.BLUE},
  form_cancel_btn: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderColor: argonTheme.COLORS.GRAY,
    borderWidth: 0.5,
  },
  amount_container: {alignItems: 'center', marginVertical: 10},
  amount_title: {
    fontSize: 16,
    textTransform: 'uppercase',
    color: argonTheme.COLORS.GRAY,
  },
  amount_wrapper: {marginVertical: 5},
  amount_text: {fontSize: 30},
  final_wrapper: {
    margin: 15,
    borderWidth: 0.5,
    borderRadius: 10,
    borderColor: argonTheme.COLORS.GRAY,
    padding: 10,
  },
  gas_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: argonTheme.COLORS.GRAY,
    borderBottomWidth: 1,
    padding: 5,
  },
  gas_title: {flex: 0.5, fontSize: 12, fontWeight: 'bold'},
  gas_value_wrapper: {flex: 0.5},
  gas_value: {
    fontSize: 12,
    textAlign: 'right',
  },
  total_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
  },
  total_title: {flex: 0.5, fontSize: 12, fontWeight: 'bold'},
  total_value_wrapper: {flex: 0.5},
  total_value: {
    fontSize: 12,
    textAlign: 'right',
    fontWeight: 'bold',
  },
});
