import {StyleSheet} from 'react-native';
import {argonTheme} from '../../../constants';

export default StyleSheet.create({
  form_container: {
    flex: 1,
    backgroundColor: argonTheme.COLORS.GRAY_100,
    padding: 20,
  },
  asset_container: {marginVertical: 10, maxHeight: '30%'},
  asset_dropdown: {padding: 0, borderWidth: 1, borderRadius: 4},
  asset_title: {marginBottom: 10},
  form_btn_text: {color: argonTheme.COLORS.WHITE, textTransform: 'uppercase'},
  token_wrapper: {
    width: '100%',
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderBottomColor: argonTheme.COLORS.GRAY_300,
  },
  token_img: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  token_detail_icon: {textAlign: 'right', flex: 1, fontWeight: 'bold'},
  error_msg: {fontSize: 13, fontWeight: 'bold', color: argonTheme.COLORS.ERROR},
  max_btn_wrapper: {
    width: 70,
    borderWidth: 0.5,
    borderColor: argonTheme.COLORS.PRIMARY,
    backgroundColor: argonTheme.COLORS.INDIGO_100,
  },
  max_btn_text: {
    color: argonTheme.COLORS.PRIMARY,
    fontSize: 11,
    textTransform: 'uppercase',
  },
});
