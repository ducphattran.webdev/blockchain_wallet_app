import React from 'react';
import {View, Text,  Pressable, Image} from 'react-native';
import {Box, Center} from 'native-base';
import styles from './styles';
import Container from '../../../components/common/Container';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {argonTheme} from '../../../constants';
import Input from '../../../components/common/Input';
import Button from '../../../components/common/Button';

const GasForm = ({
  amount,
  setAmount,
  setToMax,
  gasLimit,
  setGasLimit,
  gasPrice,
  setGasPrice,
  assets,
  selectedToken,
  setSelectedToken,
  onNext,
  onPrevious,
  error,
}) => {
  return (
    <>
      <View style={styles.form_container}>
        {/* List of tokens */}
        <View style={styles.asset_container}>
          <Text style={styles.asset_title}>Choose a token</Text>
          <Container style={styles.asset_dropdown}>
            {assets.map(
              (asset, index) =>
                asset && (
                  <Pressable
                    onPress={() => setSelectedToken(asset)}
                    key={index}
                    style={[
                      styles.token_wrapper,
                      {
                        backgroundColor:
                          selectedToken.address === asset.address
                            ? argonTheme.COLORS.BLUE_50
                            : 'transparent',
                      },
                    ]}>
                    <View style={styles.token_img}>
                      <Image
                        source={{uri: asset.logo}}
                        resizeMode={'center'}
                        style={{width: '100%', height: '100%'}}
                      />
                    </View>
                    <Text>
                      {asset.amount.toString()} {asset.symbol}
                    </Text>
                    {asset.address === selectedToken.address && (
                      <Icon
                        name={'check'}
                        size={25}
                        color={argonTheme.COLORS.SUCCESS}
                        style={styles.token_detail_icon}
                      />
                    )}
                  </Pressable>
                ),
            )}
          </Container>
        </View>

        <Box m={1}>
          <Text style={styles.form_title}>Amount</Text>
          <Box
            flexDirection="row"
            alignItems="center"
            justifyContent="space-between">
            <Input
              style={{width: '130%'}}
              keyboardType={'numeric'}
              onChangeText={val => setAmount(val)}
              blurOnSubmit={true}
              value={amount.toString()}
            />
            <Button onPress={setToMax} style={styles.max_btn_wrapper}>
              <Text style={styles.max_btn_text}>MAX</Text>
            </Button>
          </Box>
        </Box>

        <Box m={1}>
          <Text style={styles.form_title}>Gas Price (GWEI)</Text>
          <Input
            style={{width: '100%'}}
            placeholder={'eg. 10'}
            keyboardType={'numeric'}
            onChangeText={val => setGasPrice(val)}
            blurOnSubmit={true}
            value={gasPrice.toString()}
          />
        </Box>

        <Box m={1}>
          <Text style={styles.form_title}>Gas Limit</Text>
          <Input
            placeholder={'eg. 21000'}
            keyboardType={'numeric'}
            onChangeText={val => setGasLimit(val)}
            blurOnSubmit={true}
            value={gasLimit.toString()}
          />
        </Box>

        {/* Error */}
        {error !== '' && (
          <Box p={1}>
            <Text style={styles.error_msg}>{error}</Text>
          </Box>
        )}

        {/* Buttons */}
        <Center p={1} flexDirection="row">
          {/* Cancel BTN */}
          <Button
            onPress={onPrevious}
            p={2}
            style={{backgroundColor: argonTheme.COLORS.GRAY_300}}>
            <Text style={[styles.form_btn_text, {color: argonTheme.COLORS.GRAY}]}>
              Back
            </Text>
          </Button>
          {/* Next BTN */}
          <Button onPress={onNext} p={2}>
            <Text style={styles.form_btn_text}>Next</Text>
          </Button>
        </Center>
      </View>
    </>
  );
};

export default GasForm;
