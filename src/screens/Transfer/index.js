import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import SendForm from './Send';
import GasForm from './Gas';
import ConfirmForm from './Confirm';
import WalletList from '../../components/Modal/SelectWallet';
import ScannerModal from '../../components/Modal/Scanner';
import PINModal from '../../components/Modal/PIN';
import Notification from '../../components/Notification';
import {useGlobalContext} from '../../context/GlobalContext';
import Loading from '../../components/common/Loading';
import Container from '../../components/common/Container';
import Footer from '../../components/common/Footer';
import {
  checkValidAddress,
  convertFromWei,
  convertToWei,
} from '../../utils/Web3/Utility';
import {getGasPrice, getEstimatedGas} from '../../utils/Web3/Transaction';
import {sendTx} from '../../context/action/Transaction/sendTx';
import {sendTokenTx} from '../../context/action/Transaction/sendContractTx';
import BEP20_ABI from '../../constants/ABI/BEP20.json';
import {CLEAR_RECEIPT} from '../../context/types/Transaction';
import {getOneWallet} from '../../context/action/Wallet/getWallet';

const STAGES = {
  SEND_FORM: 0,
  GAS_FORM: 1,
  CONFIRM_FORM: 2,
};

const Index = ({navigation, route}) => {
  const [stage, setStage] = useState(STAGES.SEND_FORM);
  const {
    mnemonicState,
    transactionDispatch,
    walletState,
    walletDispatch,
    transactionState,
    networkState,
  } = useGlobalContext();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [showWalletModal, setShowWalletModal] = useState(false);
  const [showScannerModal, setShowScannerModal] = useState(false);
  const [showPINModal, setShowPINModal] = useState(false);
  const [showNotiModal, setShowNotiModal] = useState(false);
  // Send form
  const [fromWallet, setFromWallet] = useState(undefined);
  const [toAddress, setToAddress] = useState(undefined);
  const [selectType, setSelectType] = useState(undefined);
  const [assets, setAssets] = useState(null);
  const [selectedToken, setSelectedToken] = useState(null);
  // Gas form
  const [amount, setAmount] = useState(0);
  const [gasPrice, setGasPrice] = useState(0);
  const [gasLimit, setGasLimit] = useState(21000);
  // Confirm
  const [gasFee, setGasFee] = useState(0);
  const [notiMsg, setNotiMsg] = useState(undefined);
  const [txStatus, setTxStatus] = useState(undefined);

  useEffect(() => {
    if (stage === STAGES.SEND_FORM) {
      resetAll();
    }
  }, [stage]);

  useEffect(() => {
    setLoading(true);
    if (walletState.done && networkState.done) {
      // Send form
      if (walletState.wallet) {
        setFromWallet(walletState.wallet);
        setSelectedToken(walletState.wallet.assets[0]);
        setError('');
      }
      setShowWalletModal(false);
      setLoading(false);
    }
  }, [walletState, networkState]);

  useEffect(() => {
    if (networkState.done) {
      getOneWallet({address: fromWallet !== undefined && fromWallet.address})(walletDispatch);
    }
  }, [networkState]);

  useEffect(() => {
    if (showNotiModal && notiMsg) {
      return setTimeout(() => {
        setShowNotiModal(false);
      }, 1500);
    }
  }, [showNotiModal]);

  useEffect(() => {
    if (!selectedToken || !selectedToken.address) {
      setGasLimit(21000);
    } else {
      getEstimatedGas({
        TOKEN: {
          ABI: BEP20_ABI,
          address: selectedToken.address,
          decimals: selectedToken.decimals,
        },
        fromAddress: fromWallet.address,
        toAddress,
        amountOfToken: amount,
      }).then(([estimatedGas, err]) => {
        if (estimatedGas) {
          setGasLimit(estimatedGas.toString());
        } else if (err) {
          setGasLimit(21000);
        }
      });
    }
  }, [selectedToken, fromWallet, toAddress, amount]);

  useEffect(() => {
    if (transactionState.done && transactionState.receipt) {
      if (transactionState.receipt.status) {
        setNotiMsg('Successfully. Your transaction was confirmed.');
        setTxStatus('SUCCESSFUL');
        setShowNotiModal(true);
      } else {
        // Error from network
        setNotiMsg('Failed. Your transaction was not confirmed.');
        setTxStatus('FAILED');
        setShowNotiModal(true);
      }
    }
    // Error from device
    if (transactionState.done && transactionState.error) {
      setNotiMsg('Failed. Your transaction was not confirmed.');
      setTxStatus('FAILED');
      setShowNotiModal(true);
    }
    setLoading(false);
    setTimeout(() => {
      transactionDispatch({type: CLEAR_RECEIPT});
    }, 1000);
  }, [transactionState.done && transactionState.receipt]);

  const setGasPriceFromNet = async () => {
    const [currentPrice, err] = await getGasPrice();
    const [convertedPrice, convertErr] =
      currentPrice && (await convertFromWei(currentPrice, 'gwei'));

    if (err || convertErr) {
      return setGasPrice(0);
    }

    if (convertedPrice) {
      // Convert
      setGasPrice(convertedPrice);
    }
  };

  const onScanning = async e => {
    setShowScannerModal(false);
    setError('');
    const address = e.data.includes(':') ? e.data.split(':')[1] : e.data;
    // Check valid address
    const [isValidAddress] = await checkValidAddress(address);
    if (isValidAddress) {
      setToAddress(address);
    } else {
      setError('Invalid address');
    }
  };

  const setToMax = () => {
    const tokenIndex = walletState.wallet.assets.findIndex(
      a => a && a.address === selectedToken.address,
    );
    if (tokenIndex !== -1) {
      setAmount(walletState.wallet.assets[tokenIndex].amount);
    }
  };

  const calcGasFee = async () => {
    const [weiPrice] = await convertToWei(gasPrice, 'gwei');
    const [totalGasFee] = await convertFromWei(gasLimit * weiPrice, 'ether');
    if (totalGasFee) {
      setGasFee(totalGasFee);
    }
  };

  const toGasForm = () => {
    if (!toAddress) {
      return setError('Required to provide recipient address.');
    }

    if (toAddress.toLowerCase() === fromWallet.address.toLowerCase()) {
      return setError('Can not send transaction to the same address.');
    }

    setError('');
    setGasPriceFromNet();
    // setAssets(walletState.wallet.assets);
    return setStage(STAGES.GAS_FORM);
  };

  const toConfirmForm = () => {
    if (!amount || amount <= 0) {
      return setError('Amount is invalid.');
    }

    if (!gasPrice || parseInt(gasPrice) <= 0) {
      return setError('Gas Price is invalid.');
    }

    if (!gasLimit || parseInt(gasLimit) <= 0) {
      return setError('Gas Limit is invalid.');
    }

    const tokenIndex = walletState.wallet.assets.findIndex(
      a => a && a.address === selectedToken.address,
    );

    const balance =
      tokenIndex !== -1 && walletState.wallet.assets[tokenIndex].amount;
    if (parseFloat(amount) > parseFloat(balance)) {
      return setError('Amount exceeds current balance.');
    }

    calcGasFee();
    setError('');
    return setStage(STAGES.CONFIRM_FORM);
  };

  const onFinish = async () => {
    const [weiPrice] = await convertToWei(gasPrice, 'gwei');
    if (selectedToken.address) {
      // Send contract tx
      sendTokenTx({
        randomString: mnemonicState.randomString,
        fromWallet,
        toAddress,
        amount,
        gasPrice: weiPrice,
        gasLimit,
        TOKEN: {
          ABI: BEP20_ABI,
          address: selectedToken.address,
          decimals: selectedToken.decimals,
        },
      })(transactionDispatch);
    } else {
      // Send tx
      sendTx({
        randomString: mnemonicState.randomString,
        fromWallet,
        toAddress,
        amount,
        gasPrice: weiPrice,
      })(transactionDispatch);
    }
    setNotiMsg('Transaction was sending to the network...');
    setTxStatus('PENDING');
    setShowNotiModal(true);
    setLoading(true);
    setStage(STAGES.SEND_FORM);
    setShowPINModal(false);
  };

  const resetAll = () => {
    setError('');
    setToAddress(undefined);
    setAmount(0);
    setGasPrice(0);
    setGasLimit(21000);
    setGasFee(0);
    setNotiMsg(undefined);
    setTxStatus(undefined);
  };

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          <Container style={{flex: 1, padding: 0}}>
            {stage === STAGES.SEND_FORM && (
              <SendForm
                fromWallet={fromWallet}
                toAddress={toAddress}
                setToAddress={setToAddress}
                setShowWalletModal={setShowWalletModal}
                setShowScannerModal={setShowScannerModal}
                error={error}
                onNext={toGasForm}
                setSelectType={setSelectType}
              />
            )}

            {stage === STAGES.GAS_FORM && (
              <GasForm
                assets={walletState.wallet.assets}
                selectedToken={selectedToken}
                setSelectedToken={setSelectedToken}
                amount={amount}
                setAmount={setAmount}
                setToMax={setToMax}
                gasLimit={gasLimit}
                setGasLimit={setGasLimit}
                gasPrice={gasPrice}
                setGasPrice={setGasPrice}
                error={error}
                onNext={toConfirmForm}
                onPrevious={() => setStage(STAGES.SEND_FORM)}
              />
            )}

            {stage === STAGES.CONFIRM_FORM && (
              <ConfirmForm
                fromWallet={fromWallet}
                toAddress={toAddress}
                selectedToken={selectedToken}
                amount={amount}
                gasFee={gasFee}
                error={error}
                onNext={() => setShowPINModal(true)}
                onPrevious={() => setStage(STAGES.GAS_FORM)}
              />
            )}
          </Container>

          <Footer />
        </>
      )}

      {/* Wallet List */}
      <WalletList
        visible={showWalletModal}
        setShowWalletModal={setShowWalletModal}
        fromWallet={fromWallet}
        setFromWallet={setFromWallet}
        toAddress={toAddress}
        setToAddress={setToAddress}
        selectType={selectType}
      />

      {/* Scanner Modal */}
      <ScannerModal
        visible={showScannerModal}
        setShowModal={setShowScannerModal}
        onSuccess={onScanning}
      />

      {/* PIN modal */}
      <PINModal
        visible={showPINModal}
        setShowModal={setShowPINModal}
        onlyVerify={true}
        onFinish={onFinish}
      />

      {/* Notification modal */}
      {notiMsg && txStatus && (
        <Notification
          visible={showNotiModal}
          text={notiMsg}
          status={txStatus}
        />
      )}
    </>
  );
};

export default Index;
