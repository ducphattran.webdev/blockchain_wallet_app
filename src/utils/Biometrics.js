import ReactNativeBiometrics from 'react-native-biometrics';

export const checkAvailableBiometrics = async ({OS}) => {
  const {available, biometryType} =
    await ReactNativeBiometrics.isSensorAvailable();

  if (OS === 'android') {
    return available && biometryType === ReactNativeBiometrics.Biometrics;
  } else if (OS === 'ios') {
    return (
      available &&
      (biometryType === ReactNativeBiometrics.TouchID ||
        biometryType === ReactNativeBiometrics.FaceID)
    );
  }
};

export const promptBiometrics = ({handler}) => {
  ReactNativeBiometrics.simplePrompt({
    promptMessage: 'Confirm with biometrics',
  })
    .then(resultObject => {
      const {success} = resultObject;

      if (success) {
        handler();
      }
    })
    .catch(e => console.log(e));
};
