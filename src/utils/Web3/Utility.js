import AsyncStorage from '@react-native-async-storage/async-storage';
import {networkInitStates} from '../../context/reducer/networkReducer';
import Web3 from 'web3';
import {randomBytes} from 'react-native-randombytes';
import BigNumber from '../BigNumber';
import BEP20_ABI from '../../constants/ABI/BEP20.json';

const getWeb3Instance = async () => {
  try {
    const networkData = await AsyncStorage.getItem(
      networkInitStates.STORAGE_NAME,
    );

    if (networkData) {
      const networkState = JSON.parse(networkData);

      const currentNetwork = networkState.networks.find(n => n.current);
      if (currentNetwork) {
        return new Web3(currentNetwork.RPC);
      }
    }
    throw new Error('Can not find current network.');
  } catch (error) {
    return error.message ? error.message : error;
  }
};

// Generate wallet
const generateOneWallet = async () => {
  try {
    const web3 = await getWeb3Instance();
    const newWallet = web3.eth.accounts.create(randomBytes(16).toString('hex'));

    return [newWallet, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

// Get wallet by private key
const getWalletByPK = async _privateKey => {
  try {
    const web3 = await getWeb3Instance();
    const {address, privateKey} =
      web3.eth.accounts.privateKeyToAccount(_privateKey);

    return [{address, privateKey}, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

// Convert from decimals to token
const convertToToken = function (_amount, _decimal) {
  return new BigNumber(_amount.toString()).dividedBy(10 ** parseInt(_decimal));
};

// Convert from decimals to token
const convertToDecimal = function (_amount, _decimal) {
  return new BigNumber(_amount.toString()).multipliedBy(
    10 ** parseInt(_decimal),
  );
};

// Get token info
const getToken = async tokenAddress => {
  try {
    const web3 = await getWeb3Instance();
    const tokenInstance = new web3.eth.Contract(BEP20_ABI, tokenAddress);

    const symbol = await tokenInstance.methods.symbol().call();
    const name = await tokenInstance.methods.name().call();
    const decimals = await tokenInstance.methods.decimals().call();

    return [
      {
        address: tokenInstance.options.address,
        symbol,
        name,
        decimals,
        logo: null,
      },
      null,
    ];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

// Check valid address
const checkValidAddress = async _address => {
  try {
    const web3 = await getWeb3Instance();
    const isValid = web3.utils.isAddress(_address);

    return [isValid, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

// Convert to wei
const convertToWei = async (_amount, unit) => {
  try {
    const web3 = await getWeb3Instance();
    const convertedAmount = web3.utils.toWei(
      _amount.toString(),
      unit.toString(),
    );

    return [convertedAmount, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

// Convert from wei
const convertFromWei = async (_amount, unit) => {
  try {
    const web3 = await getWeb3Instance();
    const convertedAmount = web3.utils.fromWei(
      _amount.toString(),
      unit.toString(),
    );

    return [convertedAmount, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

export {
  getWeb3Instance,
  generateOneWallet,
  getWalletByPK,
  convertToDecimal,
  convertToToken,
  getToken,
  checkValidAddress,
  convertToWei,
  convertFromWei,
};
