import {getWeb3Instance, convertToDecimal} from './Utility';
import BigNumber from '../BigNumber';

const getGasPrice = async () => {
  try {
    const web3 = await getWeb3Instance();
    const gasPrice = await web3.eth.getGasPrice();
    return [gasPrice, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

const getEstimatedGas = async function ({
  TOKEN,
  fromAddress,
  toAddress,
  amountOfToken,
}) {
  try {
    const web3 = await getWeb3Instance();
    const contractInstance = new web3.eth.Contract(TOKEN.ABI, TOKEN.address);
    const amountOfWei = convertToDecimal(amountOfToken, TOKEN.decimals);
    // Calculate gas
    const estimateGas = await contractInstance.methods
      .transfer(toAddress, amountOfWei.toString())
      .estimateGas({from: fromAddress, gas: 5000000});

    return [Math.floor(estimateGas * 1.1), null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

const getGasFee = async function ({
  TOKEN,
  fromAddress,
  toAddress,
  amountOfToken,
}) {
  try {
    const web3 = await getWeb3Instance();
    const contractInstance = new web3.eth.Contract(TOKEN.ABI, TOKEN.address);
    const amountOfWei = convertToDecimal(amountOfToken, TOKEN.decimals);
    const gasFee = await web3.eth.getGasPrice();
    // Calculate gas
    const estimateGas = await contractInstance.methods
      .transfer(toAddress, amountOfWei.toString())
      .estimateGas({from: fromAddress, gas: 5000000, value: 0});

    const totalFee = new BigNumber(gasFee).multipliedBy(estimateGas);

    return [totalFee, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

const sendTransaction = async function ({
  wallet,
  toAddress,
  amountOfEther,
  _gasPrice,
}) {
  try {
    const web3 = await getWeb3Instance();
    const amountOfWei = web3.utils.toWei(amountOfEther.toString(), 'ether');
    const gasPrice = !_gasPrice ? await web3.eth.getGasPrice() : _gasPrice;
    const gas = 21000;

    const transferAmount = new BigNumber(amountOfWei).minus(gas * gasPrice);

    // Insufficient amount
    if (transferAmount.lt(0)) {
      return [null, 'Amount is too small, below the gas fee'];
    }
    const txObject = {
      from: wallet.address,
      to: toAddress.toString(),
      gasPrice,
      gas,
      value: web3.utils.toHex(transferAmount.toString()),
    };

    const signedTransaction = await web3.eth.accounts.signTransaction(
      txObject,
      wallet.privateKey,
    );

    const receipt = await web3.eth.sendSignedTransaction(
      signedTransaction.rawTransaction,
    );

    return [receipt, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

const sendTokenTransaction = async function ({
  wallet,
  toAddress,
  amountOfToken,
  TOKEN,
  _gasPrice,
  _gasLimit,
}) {
  try {
    const web3 = await getWeb3Instance();
    const contractInstance = new web3.eth.Contract(TOKEN.ABI, TOKEN.address);
    // Convert amount of Token
    const amountOfWei = convertToDecimal(amountOfToken, TOKEN.decimals);
    // Calculate transfer data
    const transferData = await contractInstance.methods
      .transfer(toAddress, amountOfWei.toString())
      .encodeABI();

    // Calculate gas
    const estimateGas = await contractInstance.methods
      .transfer(toAddress, amountOfWei.toString())
      .estimateGas({from: wallet.address, gas: 5000000, value: 0});

    // Gas fee
    const gasPrice = await web3.eth.getGasPrice();

    // Create transaction
    const transactionObject = {
      from: wallet.address,
      gasPrice: _gasPrice ? _gasPrice : gasPrice,
      gas: _gasLimit ? _gasLimit : Math.floor(estimateGas * 1.15), // Gas limit
      to: TOKEN.address,
      value: '0', // in wei
      // data: web3.utils.toHex(transferData),
      data: transferData,
    };
console.log(transactionObject)
    // Sign transaction
    const signedTransaction = await web3.eth.accounts.signTransaction(
      transactionObject,
      wallet.privateKey,
    );

    // Send signed transaction
    const receipt = await web3.eth.sendSignedTransaction(
      signedTransaction.rawTransaction,
    );

    return [receipt, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

module.exports = {
  getGasPrice,
  getGasFee,
  getEstimatedGas,
  sendTransaction,
  sendTokenTransaction,
};
