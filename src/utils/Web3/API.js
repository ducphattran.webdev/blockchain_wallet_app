import axios from 'axios';
import BigNumber from '../BigNumber';
import {convertFromWei} from './Utility';
import Config from 'react-native-config'

// const BSC_API_KEY = '559XGYW1H3AFD67FC93W66WFQ32NK8CI4C';

// [BSC] Get a list of transactions by an address
export const getBscNormalTxHistory = async ({
  networkId,
  page,
  limit,
  fromAddress,
}) => {
  // [97] => TESTNET
  // [56] => MAINNET
  const httpURL = networkId === 97 ? 'api-testnet.bscscan' : 'api.bscscan';

  try {
    const transaction_response = await axios.get(
      `https://${httpURL}.com/api?module=account&action=txlist&address=${fromAddress}&startblock=1&endblock=99999999&page=${page}&offset=${limit}&sort=desc&apikey=${Config.BSC_API_KEY}`,
    );

    const transaction_data =
      transaction_response.status === 200 && transaction_response.data;

    const transactions = await Promise.all(
      transaction_data.result.map(async tx => {
        const {
          timeStamp,
          value,
          txreceipt_status,
          from,
          to,
          hash,
          gasUsed,
          gasPrice,
        } = tx;
        const [fee] = await convertFromWei(
          new BigNumber(gasPrice).multipliedBy(gasUsed).toString(),
          'ether',
        );
        const [convertedValue] = await convertFromWei(value, 'ether');
        return {
          hash,
          from,
          to,
          timestamp: timeStamp,
          fee,
          value: convertedValue,
          symbol: 'BNB',
          status: txreceipt_status === '1' ? true : false,
          type:
            from.toLowerCase() === fromAddress.toLowerCase()
              ? 'sent'
              : 'received',
        };
      }),
    );

    return [transactions, null];
  } catch (error) {
    return [null, error.message ? error.message : error];
  }
};

// [BSC] Get a list of BEP20 transactions by an address
export const getBscBEP20TxHistory = async ({networkId, page, limit, fromAddress}) => {
  // [97] => TESTNET
  // [56] => MAINNET
  const httpURL = networkId === 97 ? 'api-testnet.bscscan' : 'api.bscscan';

  try {
    const token_response = await axios.get(
      `https://${httpURL}.com/api?module=account&action=tokentx&address=${fromAddress}&startblock=1&endblock=99999999&page=${page}&offset=${limit}&sort=desc&apikey=${Config.BSC_API_KEY}`,
    );

    const token_data = token_response.status === 200 && token_response.data;

    const transactions = await Promise.all(
      token_data.result.map(async tx => {
        const {
          timeStamp,
          value,
          from,
          to,
          hash,
          tokenDecimal,
          tokenSymbol,
          gasPrice,
          gasUsed,
        } = tx;
        const [fee] = await convertFromWei(
          new BigNumber(gasPrice).multipliedBy(gasUsed).toString(),
          'ether',
        );

        return {
          hash,
          from,
          to,
          timestamp: timeStamp,
          fee: fee,
          value: new BigNumber(value)
            .dividedBy(10 ** parseInt(tokenDecimal))
            .toFixed(2),
          symbol: tokenSymbol,
          status: true,
          type:
            from.toLowerCase() === fromAddress.toLowerCase()
              ? 'sent'
              : 'received',
        };
      }),
    );

    return [transactions, null];
  } catch (error) {
    return [null, error.message ? error.message : error];
  }
};
