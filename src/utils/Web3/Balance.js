import {getWeb3Instance} from './Utility';

// Get balance of Bnb
const getBalanceBNB = async _address => {
  try {
    const web3 = await getWeb3Instance();
    const balance = await web3.eth.getBalance(_address);

    return [balance, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

// Get balance of Bep20
const getBalanceBEP20 = async function (_address, {address, ABI}) {
  try {
    const web3 = await getWeb3Instance();
    const contractInstance = new web3.eth.Contract(ABI, address);
    let balanceBep20 = await contractInstance.methods
      .balanceOf(_address)
      .call();
    balanceBep20 = balanceBep20.toString();

    return [balanceBep20, null];
  } catch (error) {
    return [null, error ? error.message : error];
  }
};

module.exports = {getBalanceBEP20, getBalanceBNB};
