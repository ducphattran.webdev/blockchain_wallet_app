// Trim wallet's address to display
export const trimAddress = function (address, numOfChars = 6) {
  if (address) {
    const head = address.slice(0, numOfChars + 2);
    const tail = address.slice(address.length - 6, address.length);

    return head + '...' + tail;
  }

  return null;
};
