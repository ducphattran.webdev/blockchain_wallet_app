export function shuffle(array) {
  let tempArray = Array.from(array);
  var m = tempArray.length,
    t,
    i;

  // While there remain elements to shuffle…
  while (m) {
    // Pick a remaining element…
    i = Math.floor(Math.random() * m--);

    // And swap it with the current element.
    t = tempArray[m];
    tempArray[m] = tempArray[i];
    tempArray[i] = t;
  }

  return tempArray;
}

export function stringToWords(str) {
  if (str.length !== 0) {
    return str.split(' ').map((word, index) => ({id: index, value: word}));
  }

  return [];
}