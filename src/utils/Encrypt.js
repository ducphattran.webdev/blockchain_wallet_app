import CryptoJS from 'crypto-js';

export const encryptWithAES = (text, key) => {
  try {
    return [CryptoJS.AES.encrypt(text, key).toString(), null];
  } catch (error) {
    return [null, error.message ? error.message : error];
  }
};

export const decryptWithAES = (cipher, key) => {
  try {
    return [
      CryptoJS.AES.decrypt(cipher, key).toString(CryptoJS.enc.Utf8),
      null,
    ];
  } catch (error) {
    return [null, error.message ? error.message : error];
  }
};
