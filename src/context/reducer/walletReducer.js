import {
  LOADING,
  ERROR,
  // GROUP
  GET_GROUPS,
  CREATE_GROUP,
  UPDATE_GROUP,
  DELETE_GROUP,
  // WALLET
  GET_ONE_WALLET,
  GET_WALLETS,
  CREATE_WALLET,
  IMPORT_WALLET,
  DELETE_WALLET,
  UPDATE_WALLET,
  ADD_WALLET_TO_GROUP,
  // BALANCE
  ADD_TOKEN,
} from '../types/Wallet';

export const walletInitStates = {
  done: false,
  error: null,
  groups: [],
  STORAGE_NAME: 'WALLET_STORAGE',
  wallets: [],
  wallet: null, // {address, privateKey, name, assets: [{symbol, address, amount, decimal, logo, name, id}]}
};

export function walletReducer(state, {type, payload}) {
  switch (type) {
    case LOADING: {
      return {
        ...state,
        done: false,
      };
    }
    case ERROR: {
      return {
        ...state,
        done: true,
        error: payload.error,
      };
    }
    case GET_GROUPS:
    case UPDATE_GROUP:
    case CREATE_GROUP: {
      return {
        ...state,
        done: true,
        error: null,
        groups: payload.groups,
      };
    }
    case DELETE_GROUP: {
      return {
        ...state,
        done: true,
        error: null,
        groups: payload.groups,
        wallets: payload.wallets,
      };
    }
    case DELETE_WALLET:
    case CREATE_WALLET:
    case IMPORT_WALLET:
    case UPDATE_WALLET:
    case ADD_WALLET_TO_GROUP: {
      return {
        ...state,
        error: null,
        done: true,
        wallets: payload.wallets,
      };
    }
    case ADD_TOKEN: {
      return {
        ...state,
        error: null,
        done: true,
        wallet: payload.wallet,
        wallets: payload.wallets,
      };
    }
    case GET_ONE_WALLET: {
      return {
        ...state,
        error: null,
        done: true,
        wallet: payload.wallet,
      };
    }
    case GET_WALLETS: {
      return {
        ...state,
        error: null,
        done: true,
        wallets: payload.wallets,
      };
    }
    default: {
      return state;
    }
  }
}
