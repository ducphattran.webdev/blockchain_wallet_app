import {LOADING, SET_PIN_FAIL, SET_PIN_SUCCESS} from '../types/Pin';

export const pinInitStates = {
  done: false,
  error: null,
  length: 4,
  PIN: null,
  BLOCK_MAX_ATTEMPTS: 5,
  BLOCK_ATTEMPT_TIME: 60000,
  USERNAME: 'pincode',
  SERVICE_NAME: 'PIN',
  ACCESSIBLE: 'WHEN_UNLOCKED',
  STORAGE_TYPE: 'AES',
  STATES: {
    BLOCKED: -1,
    INCORRECT: 0,
    INPUT: 1,
    CONFIRM: 2,
  },
};

export function pinReducer(state, {type, payload}) {
  switch (type) {
    case LOADING: {
      return {...state, done: false};
    }
    case SET_PIN_FAIL: {
      return {...state, error: payload.error, done: true};
    }
    case SET_PIN_SUCCESS: {
      return {...state, PIN: payload.data, done: true};
    }
    default: {
      return state;
    }
  }
}
