import {
  LOADING,
  SEND_SUCCESS,
  SEND_FAIL,
  GET_TRANSACTIONS_FAIL,
  GET_TRANSACTIONS_SUCCESS,
  GET_MORE_TRANSACTIONS_FAIL,
  GET_MORE_TRANSACTIONS_SUCCESS,
  CLEAR_RECEIPT,
} from '../types/Transaction';

export const txInitStates = {
  error: null,
  done: false,
  receipt: null,
  transactions: [],
  page: 1,
  limit: 20,
  lastPage: 3,
  TRANSACTION_STORAGE: 'TRANSACTION_STORAGE',
};

export function transactionReducer(state, {type, payload}) {
  switch (type) {
    case LOADING: {
      return {...state, receipt: null, done: false};
    }
    case GET_TRANSACTIONS_FAIL: {
      return {
        ...state,
        receipt: null,
        done: true,
        transactions: [],
        error: payload.error,
      };
    }
    case GET_MORE_TRANSACTIONS_FAIL: {
      return {...state, done: true, receipt: null};
    }
    case GET_MORE_TRANSACTIONS_SUCCESS: {
      const newTransactions = payload.transactions.filter(
        tx => state.transactions.findIndex(i => i.hash === tx.hash) === -1,
      );
      return {
        ...state,
        done: true,
        error: null,
        receipt: null,
        lastPage: payload.lastPage,
        transactions: [...state.transactions, ...newTransactions],
        // transactions: payload.transactions,
      };
    }
    case GET_TRANSACTIONS_SUCCESS: {
      return {
        ...state,
        done: true,
        error: null,
        receipt: null,
        transactions: payload.transactions,
      };
    }
    case SEND_FAIL: {
      return {...state, error: payload.error, receipt: null, done: true};
    }
    case SEND_SUCCESS: {
      return {
        ...state,
        done: true,
        error: null,
        receipt: payload.receipt,
        transactions: [payload.receipt, ...state.transactions],
      };
    }
    case CLEAR_RECEIPT: {
      return {
        ...state,
        done: true,
        error: null,
        receipt: null,
      };
    }
    default: {
      return state;
    }
  }
}
