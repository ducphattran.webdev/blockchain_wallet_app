import {
  LOADING,
  LOGIN,
  LOGOUT,
  GET_AUTH_FAIL,
  GET_AUTH_SUCCESS,
  SET_AUTH_SUCCESS,
  SET_AUTH_FAIL,
  VERIFY_SUCCESS,
} from '../types/Auth';

export const authInitStates = {
  done: false,
  error: null,
  isLoggedIn: false,
  blocked: false,
  block_in: 0,
  isVerified: false,
  STORAGE_NAME: 'AUTH_STORAGE'
};

export function authReducer(state, {type, payload}) {
  switch (type) {
    case LOADING: {
      return {
        ...state,
        done: false,
        blocked: false,
        block_in: 0,
      };
    }
    case LOGIN: {
      return {
        ...state,
        done: true,
        blocked: false,
        block_in: 0,
        isLoggedIn: true,
      };
    }
    case LOGOUT: {
      return {
        ...state,
        done: true,
        isLoggedIn: false,
        isVerified: false,
        blocked: false,
        block_in: 0,
      };
    }
    case GET_AUTH_FAIL:
    case SET_AUTH_FAIL: {
      return {
        ...state,
        done: true,
        error: payload.error,
      };
    }
    case SET_AUTH_SUCCESS:
    case GET_AUTH_SUCCESS: {
      return {
        ...state,
        done: true,
        blocked: payload.blocked,
        block_in: payload.block_in,
      };
    }
    case VERIFY_SUCCESS: {
      return {
        ...state,
        done: true,
        isVerified: true,
      };
    }
    default: {
      return state;
    }
  }
}
