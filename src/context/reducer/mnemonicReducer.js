import {
  LOADING,
  GENERATE,
  STORE_LOADING,
  STORE_FAIL,
  STORE_SUCCESS,
  LOADED,
  GET_FAIL,
  GET_SUCCESS,
} from '../types/Mnemonic';

export const mnemonicInitStates = {
  storageName: 'MNEMONIC_STORAGE',
  done: false,
  words: [],
  randomString: null,
  error: null,
  isInit: false,
  STAGES: {
    'Secure wallet': 1,
    'Confirm Secure Phrase': 2,
  },
  USERNAME: 'secret_phrase',
  SERVICE_NAME: 'MNEMONIC',
  ACCESSIBLE: 'WHEN_UNLOCKED',
  STORAGE_TYPE: 'AES',
};

export function mnemonicReducer(state, {type, payload}) {
  switch (type) {
    case STORE_LOADING:
    case LOADING: {
      return {
        ...state,
        done: false,
      };
    }
    case LOADED: {
      return {
        ...state,
        done: true,
      };
    }
    case GENERATE: {
      return {
        ...state,
        error: null,
        done: true,
        isInit: true,
        randomString: payload.randomString,
        mnemonic: payload.mnemonic,
        words: payload.words,
      };
    }
    case GET_FAIL:
    case STORE_FAIL: {
      return {
        ...state,
        done: true,
        error: payload.error,
        isInit: true,
      };
    }
    case STORE_SUCCESS:
    case GET_SUCCESS: {
      return {
        ...state,
        done: true,
        isInit: false,
        error: null,
        randomString: payload.randomString,
        mnemonic: payload.mnemonic,
        words: payload.mnemonic
          .split(' ')
          .map((item, index) => ({id: index, value: item})),
      };
    }

    default: {
      return state;
    }
  }
}
