import { argonTheme } from '../../constants';
import {
  LOADING,
  GET_ALL_NETWORKS_FAIL,
  GET_ALL_NETWORKS_SUCCESS,
  ADD_NETWORK_SUCCESS,
  EDIT_NETWORK_FAIL,
  EDIT_NETWORK_SUCCESS,
  CHANGE_NETWORK_FAIL,
  CHANGE_NETWORK_SUCCESS,
} from '../types/Network';

export const DEFAULT_NETWORKS = [
  {
    id: 56,
    name: 'Binance Smart Chain',
    RPC: 'https://bsc-dataseed.binance.org/',
    explorer: 'https://www.bscscan.com/',
    color: argonTheme.COLORS.SUCCESS,
    current: true,
  },
  // {
  //   id: 2,
  //   name: 'Ropsten',
  //   RPC: 'https://ropsten.infura.io/v3/aae71ad4abb14859a9a7b91c34f8839e',
  //   explorer: 'https://ropsten.etherscan.io/',
  //   color: argonTheme.COLORS.purple,
  //   current: false,
  // },
  {
    id: 97,
    name: 'BSC Testnet',
    RPC: 'https://data-seed-prebsc-1-s3.binance.org:8545/',
    explorer: 'https://testnet.bscscan.com/',
    color: argonTheme.COLORS.WARNING,
    current: false,
  },
];

export const networkInitStates = {
  done: false,
  error: null,
  networks: DEFAULT_NETWORKS,
  STORAGE_NAME: 'NETWORK_STORAGE'
};

export function networkReducer(state, {type, payload}) {
  switch (type) {
    case LOADING: {
      return {
        ...state,
        done: false,
      };
    }
    case CHANGE_NETWORK_FAIL:
    case EDIT_NETWORK_FAIL:
    case GET_ALL_NETWORKS_FAIL: {
      return {
        ...state,
        done: true,
        error: payload.error,
      };
    }
    case GET_ALL_NETWORKS_SUCCESS: {
      return {
        ...state,
        done: true,
        error: null,
        networks: payload.networks,
      };
    }
    case CHANGE_NETWORK_SUCCESS:
    case EDIT_NETWORK_SUCCESS:
    case ADD_NETWORK_SUCCESS: {
      return {
        ...state,
        done: true,
        error: null,
        networks: payload.networks,
      };
    }

    default: {
      return state;
    }
  }
}
