export const GET_ALL_NETWORKS_SUCCESS = 'GET_ALL_NETWORKS_SUCCESS';
export const GET_ALL_NETWORKS_FAIL = 'GET_ALL_NETWORKS_FAIL';
export const LOAD_NETWORK_SUCCESS = 'LOAD_NETWORK_SUCCESS';
export const LOAD_NETWORK_FAIL = 'LOAD_NETWORK_FAIL';
export const ADD_NETWORK_SUCCESS = 'ADD_NETWORK_SUCCESS';
export const EDIT_NETWORK_SUCCESS = 'EDIT_NETWORK_SUCCESS';
export const EDIT_NETWORK_FAIL = 'EDIT_NETWORK_FAIL';
export const CHANGE_NETWORK_SUCCESS = 'CHANGE_NETWORK_SUCCESS';
export const CHANGE_NETWORK_FAIL = 'CHANGE_NETWORK_FAIL';
export const LOADING = 'LOADING';