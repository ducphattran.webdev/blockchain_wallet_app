export const LOADING = 'LOADING';
export const ERROR = 'ERROR';
// GROUP
export const GET_GROUPS = 'GET_GROUPS';
export const CREATE_GROUP = 'CREATE_GROUP';
export const UPDATE_GROUP = 'UPDATE_GROUP';
export const DELETE_GROUP = 'DELETE_GROUP';
// WALLET
export const GET_ONE_WALLET = 'GET_ONE_WALLET';
export const GET_WALLETS = 'GET_WALLETS';
export const IMPORT_WALLET = 'IMPORT_WALLET';
export const CREATE_WALLET = 'CREATE_WALLET';
export const DELETE_WALLET = 'DELETE_WALLET';
export const UPDATE_WALLET = 'UPDATE_WALLET';
export const ADD_WALLET_TO_GROUP = 'ADD_WALLET_TO_GROUP';
export const ADD_TOKEN = 'ADD_TOKEN';
// export const GET_BALANCE_SUCCESS = 'GET_BALANCE_SUCCESS';
// export const GET_BALANCE_FAIL = 'GET_BALANCE_FAIL';
// export const ADD_TOKEN_FAIL = 'GET_BALANCE_FAIL';
