import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LOADING,
  ADD_NETWORK_FAIL,
  ADD_NETWORK_SUCCESS,
} from '../../types/Network';
import {
  networkInitStates,
  DEFAULT_NETWORKS,
} from '../../reducer/networkReducer';
import { argonTheme } from '../../../constants';

export const addNetwork =
  ({name, chainID, blockExplorer, rpc}) =>
  dispatch => {
    dispatch({type: LOADING});
    const newNetwork = {
      name,
      id: chainID,
      explorer: blockExplorer,
      RPC: rpc,
      color: argonTheme.COLORS.GRAY,
      current: false,
    };

    AsyncStorage.getItem(networkInitStates.STORAGE_NAME)
      .then(data => {
        if (data) {
          const network_data = JSON.parse(data);

          AsyncStorage.setItem(
            networkInitStates.STORAGE_NAME,
            JSON.stringify({
              ...network_data,
              networks: [...network_data.networks, newNetwork],
            }),
          );

          return dispatch({
            type: ADD_NETWORK_SUCCESS,
            payload: {networks: [...network_data.networks, newNetwork]},
          });
        }

        AsyncStorage.setItem(
          networkInitStates.STORAGE_NAME,
          JSON.stringify({
            ...networkInitStates,
            networks: [...DEFAULT_NETWORKS, newNetwork],
          }),
        );

        return dispatch({
          type: ADD_NETWORK_SUCCESS,
          payload: {networks: [...DEFAULT_NETWORKS, newNetwork]},
        });
      })
      .catch(e =>
        dispatch({
          type: ADD_NETWORK_FAIL,
          payload: {error: 'Failed to get all networks in the storage.'},
        }),
      );
  };
