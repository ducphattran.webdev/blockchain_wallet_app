import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LOADING,
  CHANGE_NETWORK_SUCCESS,
  CHANGE_NETWORK_FAIL,
} from '../../types/Network';
import {networkInitStates} from '../../reducer/networkReducer';

export const changeNetwork =
  ({id}) =>
  dispatch => {
    dispatch({type: LOADING});

    AsyncStorage.getItem(networkInitStates.STORAGE_NAME)
      .then(data => {
        if (!data) {
          return dispatch({
            type: CHANGE_NETWORK_FAIL,
            payload: {
              error: 'No network in the storage.',
            },
          });
        }

        const {networks} = JSON.parse(data);
        const isExisted = networks.findIndex(
          i => i.id.toString() === id.toString(),
        );
        if (isExisted === -1) {
          return dispatch({
            type: CHANGE_NETWORK_FAIL,
            payload: {
              error: 'Not found network.',
            },
          });
        }

        // Update
        const updatedNetworks = networks.map((i, index) => {
          if (index === isExisted) {
            return {...i, current: true};
          }

          return {...i, current: false};
        });
        AsyncStorage.setItem(
          networkInitStates.STORAGE_NAME,
          JSON.stringify({
            ...JSON.parse(data),
            networks: updatedNetworks,
          }),
        );

        dispatch({
          type: CHANGE_NETWORK_SUCCESS,
          payload: {
            networks: updatedNetworks,
          },
        });
      })
      .catch(e =>
        dispatch({
          type: CHANGE_NETWORK_FAIL,
          payload: {error: 'Failed to get networks in the storage.'},
        }),
      );
  };
