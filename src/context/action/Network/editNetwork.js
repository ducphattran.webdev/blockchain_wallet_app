import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LOADING,
  EDIT_NETWORK_SUCCESS,
  EDIT_NETWORK_FAIL,
} from '../../types/Network';
import {networkInitStates} from '../../reducer/networkReducer';

export const editNetwork =
  ({id, name, blockExplorer, rpc}) =>
  dispatch => {
    dispatch({type: LOADING});
    const newNetwork = {
      name,
      blockExplorer,
      rpc,
    };

    AsyncStorage.getItem(networkInitStates.STORAGE_NAME)
      .then(data => {
        if (data) {
          const network_data = JSON.parse(data);
          const networks = network_data.networks.map(n => {
            if (n.id.toString() === id.toString()) {
              return {
                ...n,
                name: newNetwork.name ? newNetwork.name : n.name,
                explorer: newNetwork.blockExplorer
                  ? newNetwork.blockExplorer
                  : n.explorer,
                RPC: newNetwork.rpc ? newNetwork.rpc : n.RPC,
              };
            }

            return n;
          });
          // Store
          AsyncStorage.setItem(
            networkInitStates.STORAGE_NAME,
            JSON.stringify({
              ...network_data,
              networks,
            }),
          );

          return dispatch({
            type: EDIT_NETWORK_SUCCESS,
            payload: {networks},
          });
        }

        return dispatch({
          type: EDIT_NETWORK_FAIL,
          payload: {error: 'No network in the storage'},
        });
      })
      .catch(e =>
        dispatch({
          type: EDIT_NETWORK_FAIL,
          payload: {error: 'No network in the storage'},
        }),
      );
  };
