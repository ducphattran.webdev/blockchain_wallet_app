import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LOADING,
  GET_ALL_NETWORKS_SUCCESS,
  GET_ALL_NETWORKS_FAIL,
} from '../../types/Network';
import {networkInitStates} from '../../reducer/networkReducer';

export const getNetworks = () => dispatch => {
  dispatch({type: LOADING});

  AsyncStorage.getItem(networkInitStates.STORAGE_NAME)
    .then(data => {
      if (data) {
        return dispatch({
          type: GET_ALL_NETWORKS_SUCCESS,
          payload: {
            networks: JSON.parse(data).networks,
          },
        });
      }

      AsyncStorage.setItem(
        networkInitStates.STORAGE_NAME,
        JSON.stringify(networkInitStates),
      );

      dispatch({
        type: GET_ALL_NETWORKS_SUCCESS,
        payload: {
          networks: networkInitStates.networks,
        },
      });
    })
    .catch(e =>
      dispatch({
        type: GET_ALL_NETWORKS_FAIL,
        payload: {error: 'Failed to get all networks in the storage.'},
      }),
    );
};
