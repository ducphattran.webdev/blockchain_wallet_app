import {
  LOADING,
  GET_TRANSACTIONS_FAIL,
  GET_TRANSACTIONS_SUCCESS,
  GET_MORE_TRANSACTIONS_FAIL,
  GET_MORE_TRANSACTIONS_SUCCESS,
} from '../../types/Transaction';
import {
  getBscBEP20TxHistory,
  getBscNormalTxHistory,
} from '../../../utils/Web3/API';
import {txInitStates} from '../../reducer/transactionReducer';

export const getTransactions =
  ({fromAddress, network, type}) =>
  async dispatch => {
    dispatch({type: LOADING});

    // Binance
    const isBinanceNetwork = [97, 56].includes(network.id);
    if (!isBinanceNetwork) {
      return dispatch({
        type: GET_TRANSACTIONS_FAIL,
        payload: {error: 'Not a Binance network.'},
      });
    }

    try {
      const [normal_transactions] = await getBscNormalTxHistory({
        networkId: network.id,
        page: 1,
        limit: txInitStates.limit,
        fromAddress,
      });
      let [token_transactions] = await getBscBEP20TxHistory({
        networkId: network.id,
        page: 1,
        limit: txInitStates.limit,
        fromAddress,
      });

      const transactions =
        type === 'normal' ? normal_transactions : token_transactions;

      dispatch({type: GET_TRANSACTIONS_SUCCESS, payload: {transactions}});
    } catch (error) {
      return dispatch({
        type: GET_TRANSACTIONS_FAIL,
        payload: {error: error.message ? error.message : error},
      });
    }
  };

export const getHistoryByPage =
  ({fromAddress, network, page, type}) =>
  async dispatch => {
    // Binance
    const isBinanceNetwork = [97, 56].includes(network.id);
    if (!isBinanceNetwork) {
      return dispatch({
        type: GET_MORE_TRANSACTIONS_FAIL,
        payload: {error: 'Not a Binance network.'},
      });
    }

    try {
      const [normal_transactions] = await getBscNormalTxHistory({
        networkId: network.id,
        page,
        limit: txInitStates.limit,
        fromAddress,
      });
      const [token_transactions] = await getBscBEP20TxHistory({
        networkId: network.id,
        page,
        limit: txInitStates.limit,
        fromAddress,
      });

      let lastPage =
        normal_transactions.length === 0 && token_transactions.length === 0
          ? page
          : page + 1;

      const transactions =
        type === 'normal' ? normal_transactions : token_transactions;

      dispatch({
        type: GET_MORE_TRANSACTIONS_SUCCESS,
        payload: {
          transactions,
          page: page !== lastPage ? page + 1 : page,
          lastPage,
        },
      });
    } catch (error) {
      return dispatch({
        type: GET_MORE_TRANSACTIONS_FAIL,
        payload: {error: error.message ? error.message : error},
      });
    }
  };
