import {LOADING, SEND_FAIL, SEND_SUCCESS} from '../../types/Transaction';
import {sendTokenTransaction} from '../../../utils/Web3/Transaction';
import {decryptWithAES} from '../../../utils/Encrypt';

export const sendTokenTx =
  ({randomString, fromWallet, toAddress, amount, gasPrice, gasLimit, TOKEN}) =>
  dispatch => {
    dispatch({type: LOADING});

    const [privateKey] = decryptWithAES(fromWallet.privateKey, randomString);

    if (privateKey) {
      const wallet = {
        address: fromWallet.address,
        privateKey,
      };

      sendTokenTransaction({
        wallet,
        toAddress,
        amountOfToken: amount,
        TOKEN,
        _gasPrice: gasPrice,
        _gasLimit: gasLimit,
      }).then(([receipt, error]) => {
        if (receipt) {
          const transaction = {
            hash: receipt.transactionHash,
            from: receipt.from,
            to: toAddress,
            timestamp: Math.floor(Date.now() / 1000),
            value: amount,
            symbol: TOKEN.symbol,
            status: receipt.status,
            type: 'sent',
          };
          dispatch({type: SEND_SUCCESS, payload: {receipt: transaction}});
        } else {
          dispatch({type: SEND_FAIL, payload: {error}});
        }
      });
    } else {
      dispatch({type: SEND_FAIL, payload: {error: 'Private key is invalid.'}});
    }
  };
