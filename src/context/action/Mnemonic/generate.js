import {randomBytes} from 'react-native-randombytes';
import {LOADING, GENERATE} from '../../../context/types/Mnemonic';
import bip39 from 'react-native-bip39';
import {stringToWords} from '../../../utils/Array';

export const generate = () => dispatch => {
  dispatch({type: LOADING});
  const randomString = randomBytes(16).toString('hex');
  const mnemonic = bip39.entropyToMnemonic(randomString);
  const words = stringToWords(mnemonic).map(item => ({...item, chosen: false}));
  console.log(mnemonic, '----')
  dispatch({type: GENERATE, payload: {words, mnemonic, randomString}});
}; 
