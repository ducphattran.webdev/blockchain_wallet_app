import {LOADING, GET_SUCCESS, GET_FAIL} from '../../../context/types/Mnemonic';
import {mnemonicInitStates} from '../../../context/reducer/mnemonicReducer';
import Keychain from 'react-native-keychain';

export const getMnemonic = () => async dispatch => {
  dispatch({type: LOADING});
  // Get mnemonic_data
  const mnemonic_data = await Keychain.getGenericPassword({
    service: mnemonicInitStates.SERVICE_NAME,
  });
  if (mnemonic_data) {
    const result = JSON.parse(mnemonic_data.password);
    const mnemonic = result.mnemonic[result.mnemonic.length - 1];
    const randomString = result.randomString[result.randomString.length - 1];

    dispatch({type: GET_SUCCESS, payload: {randomString, mnemonic}});
  } else {
    dispatch({
      type: GET_FAIL,
      payload: {error: 'Failed to get mnemonic data.'},
    });
  }
};
