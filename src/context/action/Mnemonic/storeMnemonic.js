import {
  STORE_FAIL,
  STORE_LOADING,
  STORE_SUCCESS,
} from '../../../context/types/Mnemonic';
import {mnemonicInitStates} from '../../../context/reducer/mnemonicReducer';
import Keychain from 'react-native-keychain';

export const storeMnemonic =
  ({mnemonic, randomString}) =>
  async dispatch => {
    dispatch({type: STORE_LOADING});
    // Get old mnemonic_data
    const old_mnemonic_data = await Keychain.getGenericPassword({
      service: mnemonicInitStates.SERVICE_NAME,
    });
    // Existed
    if (old_mnemonic_data) {
      const old_data = JSON.parse(old_mnemonic_data);
      const mnemonic_data = {
        randomString: [...old_data.randomString, randomString],
        mnemonic: [...old_data.mnemonic, mnemonic],
      };

      const result = await Keychain.setGenericPassword(
        mnemonicInitStates.USERNAME,
        JSON.stringify(mnemonic_data),
        {
          accessible: mnemonicInitStates.ACCESSIBLE,
          storageType: mnemonicInitStates.STORAGE_TYPE,
          service: mnemonicInitStates.SERVICE_NAME,
        },
      );

      if (!result) {
        dispatch({
          type: STORE_FAIL,
          payload: {error: 'Fail to store Mnemonic.'},
        });
      } else {
        dispatch({
          type: STORE_SUCCESS,
          payload: {
            randomString,
            mnemonic,
          },
        });
      }
    }
    // Create new
    else {
      const result = await Keychain.setGenericPassword(
        mnemonicInitStates.USERNAME,
        JSON.stringify({mnemonic: [mnemonic], randomString: [randomString]}),
        {
          accessible: mnemonicInitStates.ACCESSIBLE,
          storageType: mnemonicInitStates.STORAGE_TYPE,
          service: mnemonicInitStates.SERVICE_NAME,
        },
      );

      if (!result) {
        dispatch({
          type: STORE_FAIL,
          payload: {error: 'Fail to store Mnemonic.'},
        });
      } else {
        dispatch({
          type: STORE_SUCCESS,
          payload: {
            randomString,
            mnemonic,
          },
        });
      }
    }
  };
