import Keychain from 'react-native-keychain';
import {LOADING, SET_PIN_FAIL, SET_PIN_SUCCESS} from '../../types/Pin';
import {pinInitStates} from '../../reducer/pinReducer';

export const getPIN = () => async dispatch => {
  dispatch({type: LOADING});
  const data = await Keychain.getGenericPassword({
    service: pinInitStates.SERVICE_NAME,
  });

  if (!data) {
    dispatch({
      type: SET_PIN_FAIL,
      payload: {error: 'No Pin to validate.'},
    });
  } else {
    dispatch({type: SET_PIN_SUCCESS, payload: {data: data.password}});
  }
};
