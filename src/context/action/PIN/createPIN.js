import Keychain from 'react-native-keychain';
import {LOADING, SET_PIN_FAIL, SET_PIN_SUCCESS} from '../../types/Pin';
import {pinInitStates} from '../../reducer/pinReducer';

export const createPIN =
  ({pinCode}) =>
  async dispatch => {
    dispatch({type: LOADING});
    try {
      const data = await Keychain.setGenericPassword(
        pinInitStates.USERNAME,
        pinCode.toString(),
        {
          accessible: pinInitStates.ACCESSIBLE,
          storageType: pinInitStates.STORAGE_TYPE,
          service: pinInitStates.SERVICE_NAME,
        },
      );

      if (!data) {
        dispatch({
          type: SET_PIN_FAIL,
          payload: {error: 'Fail to store the PIN code.'},
        });
      } else {
        dispatch({type: SET_PIN_SUCCESS, payload: {data: pinCode.toString()}});
      }
    } catch (error) {
      dispatch({
        type: SET_PIN_FAIL,
        payload: {error: 'Fail to store the PIN code.'},
      });
    }
  };
