import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LOADING,
  ERROR,
  GET_ONE_WALLET,
  GET_WALLETS,
} from '../../../context/types/Wallet';
import {walletInitStates} from '../../../context/reducer/walletReducer';
import {getBalanceBNB, getBalanceBEP20} from '../../../utils/Web3/Balance';
import ABI_BEP20 from '../../../constants/ABI/BEP20.json';
import {convertToToken} from '../../../utils/Web3/Utility';

const getBalances = async (assets, wallet_address) => {
  let balances = await Promise.all(
    assets.map(async asset => {
      if (asset.symbol === 'BNB') {
        const [amount, error] = await getBalanceBNB(wallet_address);

        if (!error) {
          return {...asset, amount: convertToToken(amount, 18).toString()};
        }
      } else {
        const TOKEN = {address: asset.address, ABI: ABI_BEP20};
        const [amount, error] = await getBalanceBEP20(wallet_address, TOKEN);

        if (!error) {
          return {
            ...asset,
            amount: convertToToken(amount, asset.decimals).toString(),
          };
        }
      }
    }),
  );
  return balances;
};

export const getOneWallet =
  ({address}) =>
  dispatch => {
    dispatch({type: LOADING});

    // Get wallet data
    AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
      .then(storageData => {
        if (storageData) {
          const wallets =
            storageData && JSON.parse(storageData).wallets
              ? JSON.parse(storageData).wallets
              : [];

          const wallet = address
            ? wallets.find(
                w => w.address.toLowerCase() === address.toLowerCase(),
              )
            : wallets[0];

          // Existed
          if (wallet) {
            return getBalances(wallet.assets, wallet.address)
              .then(assets => {
                dispatch({
                  type: GET_ONE_WALLET,
                  payload: {
                    wallet: {...wallet, assets},
                  },
                });
              })
              .catch(e => {
                dispatch({
                  type: ERROR,
                  payload: {error: 'Wallet is not found.'},
                });
              });
          }
        }
      })
      .catch(e => {
        dispatch({
          type: ERROR,
          payload: {error: 'Failed to get wallet data.'},
        });
      });
  };

export const getWallets = () => dispatch => {
  dispatch({type: LOADING});

  // Get wallet data
  AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
    .then(storageData => {
      if (storageData) {
        const wallets =
          storageData && JSON.parse(storageData).wallets
            ? JSON.parse(storageData).wallets
            : [];

        return dispatch({
          type: GET_WALLETS,
          payload: {
            wallets,
          },
        });
      }

      dispatch({
        type: ERROR,
        // payload: {error: 'No wallets.'},
        payload: {error: null},
      });
    })
    .catch(e =>
      dispatch({
        type: ERROR,
        payload: {error: 'Failed to get wallet data.'},
      }),
    );
};
