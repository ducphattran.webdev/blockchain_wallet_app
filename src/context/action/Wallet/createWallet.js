import AsyncStorage from '@react-native-async-storage/async-storage';
import {LOADING, ERROR, CREATE_WALLET} from '../../../context/types/Wallet';
import {walletInitStates} from '../../../context/reducer/walletReducer';
import {generateOneWallet} from '../../../utils/Web3/Utility';
import {encryptWithAES} from '../../../utils/Encrypt';

export const createWallet =
  ({randomString, groupId}) =>
  async dispatch => {
    dispatch({type: LOADING});

    const [wallet, error] = await generateOneWallet();

    if (error) {
      return dispatch({
        type: ERROR,
        payload: {error: 'Failed to create new wallet.'},
      });
    }

    // Get wallet data
    AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
      .then(storageData => {
        const wallets =
          storageData && JSON.parse(storageData).wallets
            ? JSON.parse(storageData).wallets
            : [];
        const id =
          wallets.length !== 0 ? wallets[wallets.length - 1].id + 1 : 1;
        const [hash_pk] = encryptWithAES(wallet.privateKey, randomString);

        const newWallet = {
          id,
          address: wallet.address,
          privateKey: hash_pk,
          name: `Wallet ${id}`,
          assets: [
            {
              id: 1839,
              symbol: 'BNB',
              address: null,
              decimals: 18,
              amount: 0,
              logo: 'https://s2.coinmarketcap.com/static/img/coins/64x64/1839.png',
              name: 'Binance Coin',
            },
          ],
          groupId: groupId ? groupId : 0
        };

        // Store to storage
        AsyncStorage.setItem(
          walletInitStates.STORAGE_NAME,
          JSON.stringify({
            ...JSON.parse(storageData),
            wallets: [...wallets, newWallet],
          }),
        );

        dispatch({
          type: CREATE_WALLET,
          payload: {wallet: {...newWallet}, wallets: [...wallets, newWallet]},
        });
      })
      .catch(e =>
        dispatch({
          type: ERROR,
          payload: {error: 'Failed to get wallet data.'},
        }),
      );
  };
