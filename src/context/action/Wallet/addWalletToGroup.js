import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LOADING,
  ERROR,
  ADD_WALLET_TO_GROUP,
} from '../../../context/types/Wallet';
import {walletInitStates} from '../../../context/reducer/walletReducer';

export const addWalletToGroup =
  ({address, groupId}) =>
  dispatch => {
    dispatch({type: LOADING});

    // Get wallet data
    AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
      .then(storageData => {
        let wallets =
          storageData && JSON.parse(storageData).wallets
            ? JSON.parse(storageData).wallets
            : [];

        // Check existed wallet
        const isExisted =
          wallets.length !== 0
            ? wallets.findIndex(
                w => w.address.toLowerCase() === address.toLowerCase(),
              )
            : -1;

        if (isExisted !== -1) {
          return dispatch({
            type: ERROR,
            payload: {error: 'The wallet is already added.'},
          });
        }

        let groups =
          storageData && JSON.parse(storageData).groups
            ? JSON.parse(storageData).groups
            : [];

        // Check existed group
        const isExistedGroup =
          groups.length !== 0 ? groups.findIndex(g => g.id === groupId) : -1;
        if (isExistedGroup === -1) {
          return dispatch({
            type: ERROR,
            payload: {error: 'Group is not found.'},
          });
        }

        const id =
          wallets.length !== 0 ? wallets[wallets.length - 1].id + 1 : 1;
        const newWallet = {
          id,
          address,
          privateKey: null,
          name: `Wallet ${id}`,
          assets: [
            {
              id: 1839,
              symbol: 'BNB',
              address: null,
              decimals: 18,
              amount: 0,
              logo: 'https://s2.coinmarketcap.com/static/img/coins/64x64/1839.png',
              name: 'Binance Coin',
            },
          ],
          groupId,
        };

        // Store to storage
        AsyncStorage.setItem(
          walletInitStates.STORAGE_NAME,
          JSON.stringify({
            ...JSON.parse(storageData),
            wallets: [...wallets, newWallet],
          }),
        );

        dispatch({
          type: ADD_WALLET_TO_GROUP,
          payload: {wallets: [...wallets, newWallet]},
        });

        return true;
      })
      .catch(e =>
        dispatch({
          type: ERROR,
          payload: {error: 'Failed to get groups.'},
        }),
      );
  };
