import AsyncStorage from '@react-native-async-storage/async-storage';
import {LOADING, ERROR, CREATE_GROUP} from '../../../context/types/Wallet';
import {walletInitStates} from '../../../context/reducer/walletReducer';

export const createGroup =
  ({name}) =>
  dispatch => {
    dispatch({type: LOADING});

    // Get wallet data
    AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
      .then(storageData => {
        const groups =
          storageData && JSON.parse(storageData).groups
            ? JSON.parse(storageData).groups
            : [];
        let isExisted =
          groups.length !== 0
            ? groups.findIndex(i => i.name.toLowerCase() === name.toLowerCase())
            : -1;

        if (isExisted !== -1) {
          return dispatch({
            type: ERROR,
            payload: {error: 'This group is already existed.'},
          });
        }

        const id = groups.length !== 0 ? groups[groups.length - 1].id + 1 : 0;
        const newGroup = {
          id,
          name,
        };

        // Store to storage
        AsyncStorage.setItem(
          walletInitStates.STORAGE_NAME,
          JSON.stringify({
            ...JSON.parse(storageData),
            groups: [...groups, newGroup],
          }),
        );

        return dispatch({
          type: CREATE_GROUP,
          payload: {groups: [...groups, newGroup]},
        });
      })
      .catch(e =>
        dispatch({
          type: ERROR,
          payload: {error: 'Failed to create new group.'},
        }),
      );
  };
