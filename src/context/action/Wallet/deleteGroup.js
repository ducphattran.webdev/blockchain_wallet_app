import AsyncStorage from '@react-native-async-storage/async-storage';
import {LOADING, ERROR, DELETE_GROUP} from '../../../context/types/Wallet';
import {walletInitStates} from '../../../context/reducer/walletReducer';

export const deleteGroup =
  ({id}) =>
  dispatch => {
    dispatch({type: LOADING});

    // Get wallet data
    AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
      .then(storageData => {
        const groups =
          storageData && JSON.parse(storageData).groups
            ? JSON.parse(storageData).groups
            : [];
        let isExisted =
          groups.length !== 0 ? groups.findIndex(i => i.id === id) : -1;

        if (isExisted === -1) {
          return dispatch({
            type: ERROR,
            payload: {error: 'This group is not found.'},
          });
        }

        const newGroups = groups.filter(g => g.id !== id);

        // Update groupId for all wallets
        let wallets =
          storageData && JSON.parse(storageData).wallets
            ? JSON.parse(storageData).wallets
            : [];

        wallets = wallets.map(wallet => {
          if (wallet.groupId === id) {
            return {...wallet, groupId: 0};
          }
          return wallet;
        });

        // Store to storage
        AsyncStorage.setItem(
          walletInitStates.STORAGE_NAME,
          JSON.stringify({
            ...JSON.parse(storageData),
            groups: newGroups,
            wallets: wallets,
          }),
        );

        dispatch({
          type: DELETE_GROUP,
          payload: {groups: newGroups, wallets},
        });

        return true;
      })
      .catch(e =>
        dispatch({
          type: ERROR,
          payload: {error: 'Failed to create new group.'},
        }),
      );
  };
