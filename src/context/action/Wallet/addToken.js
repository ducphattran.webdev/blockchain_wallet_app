import AsyncStorage from '@react-native-async-storage/async-storage';
import {LOADING, ERROR, ADD_TOKEN} from '../../types/Wallet';
import {walletInitStates} from '../../reducer/walletReducer';
import axios from 'axios';

export const addToken =
  (wallet, {address, symbol, decimals, networkId}) =>
  dispatch => {
    dispatch({type: LOADING});

    // Get wallet data
    AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
      .then(async storageData => {
        let wallets =
          storageData && JSON.parse(storageData).wallets
            ? JSON.parse(storageData).wallets
            : [];
        // const isExisted =
        //   wallets.length !== 0
        //     ? wallets.findIndex(
        //         w => w.address.toLowerCase() === wallet.address.toLowerCase(),
        //       )
        //     : -1;

        // if (isExisted === -1) {
        //   return dispatch({
        //     type: ERROR,
        //     payload: {error: 'Wallet is not found.'},
        //   });
        // }
        
        // if (isExisted !== -1) {
        // Check existed assets
        const existedToken = wallet.assets.findIndex(
          i => i.address && i.address.toLowerCase() === address.toLowerCase(),
        );
        if (existedToken !== -1) {
          return dispatch({
            type: ERROR,
            payload: {error: 'This token is already in the list.'},
          });
        }

        // Get data from Coin Market Cap
        const response = await axios.get(
          `https://pro-api.coinmarketcap.com/v1/cryptocurrency/info?symbol=${symbol}`,
          {
            headers: {
              'Content-Type': 'application/json',
              'X-CMC_PRO_API_KEY': 'd4abb66c-62bf-4b35-9ee7-a138c50a1bf5',
            },
          },
        );

        const newToken = {
          address,
          symbol,
          amount: 0,
          decimals: parseInt(decimals),
          networkId: networkId,
        };

        if (response.status === 200) {
          newToken.id = response.data.data[symbol].id;
          newToken.logo = response.data.data[symbol].logo;
          newToken.name = response.data.data[symbol].name;
        }
        
        // Add to list of assets
        wallet.assets = [...wallet.assets, newToken];
        const newWallets = wallets.map(w => {
          if (w.address === wallet.address) {
            return wallet;
          }
          return w;
        });
        // Store to storage
        AsyncStorage.setItem(
          walletInitStates.STORAGE_NAME,
          JSON.stringify({
            ...JSON.parse(storageData),
            wallets: newWallets,
          }),
        );

        return dispatch({
          type: ADD_TOKEN,
          payload: {
            wallet,
            wallets: newWallets,
          },
        });
      })
      .catch(e => {
        console.log(e);
        dispatch({
          type: ERROR,
          payload: {error: 'Failed to get wallet data.'},
        });
      });
  };
