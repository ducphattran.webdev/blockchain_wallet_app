import AsyncStorage from '@react-native-async-storage/async-storage';
import {LOADING, ERROR, UPDATE_WALLET} from '../../../context/types/Wallet';
import {walletInitStates} from '../../../context/reducer/walletReducer';

export const updateInfo =
  ({name, address, groupId}) =>
  dispatch => {
    dispatch({type: LOADING});

    // Get wallet data
    AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
      .then(storageData => {
        let wallets =
          storageData && JSON.parse(storageData).wallets
            ? JSON.parse(storageData).wallets
            : [];
        const isExisted =
          wallets.length !== 0
            ? wallets.findIndex(
                w => w.address.toLowerCase() === address.toLowerCase(),
              )
            : 1;

        if (isExisted === -1) {
          return dispatch({
            type: ERROR,
            payload: {error: 'This wallet is not found.'},
          });
        }

        if (isExisted !== -1) {
          wallets[isExisted].name = name;
          wallets[isExisted].groupId = groupId;

          // Store to storage
          AsyncStorage.setItem(
            walletInitStates.STORAGE_NAME,
            JSON.stringify({
              ...JSON.parse(storageData),
              wallets,
            }),
          );

          return dispatch({
            type: UPDATE_WALLET,
            payload: {
              wallets,
            },
          });
        }
      })
      .catch(e =>
        dispatch({
          type: ERROR,
          payload: {error: 'Failed to get wallet data.'},
        }),
      );
  };
