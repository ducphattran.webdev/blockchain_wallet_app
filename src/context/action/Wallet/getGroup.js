import AsyncStorage from '@react-native-async-storage/async-storage';
import {LOADING, ERROR, GET_GROUPS} from '../../../context/types/Wallet';
import {walletInitStates} from '../../../context/reducer/walletReducer';

export const getGroups = () => dispatch => {
  dispatch({type: LOADING});

  // Get wallet data
  AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
    .then(storageData => {
      const groups =
        storageData && JSON.parse(storageData).groups
          ? JSON.parse(storageData).groups
          : [];

      return dispatch({
        type: GET_GROUPS,
        payload: {groups},
      });
    })
    .catch(e =>
      dispatch({
        type: ERROR,
        payload: {error: 'Failed to get groups.'},
      }),
    );
};
