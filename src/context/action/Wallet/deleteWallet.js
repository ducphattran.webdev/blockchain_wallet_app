import AsyncStorage from '@react-native-async-storage/async-storage';
import {LOADING, ERROR, DELETE_WALLET} from '../../../context/types/Wallet';
import {walletInitStates} from '../../../context/reducer/walletReducer';

export const deleteWallet =
  ({address}) =>
  dispatch => {
    dispatch({type: LOADING});

    // Get wallet data
    AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
      .then(storageData => {
        let wallets =
          storageData && JSON.parse(storageData).wallets
            ? JSON.parse(storageData).wallets
            : [];
        const isExisted =
          wallets.length !== 0
            ? wallets.findIndex(
                w => w.address.toLowerCase() === address.toLowerCase(),
              )
            : -1;

        if (isExisted === -1) {
          return dispatch({
            type: ERROR,
            payload: {error: 'Wallet is not found.'},
          });
        }

        if (isExisted !== -1) {
          wallets = wallets.filter(
            wallet => wallet.address.toLowerCase() !== address.toLowerCase(),
          );

          if (wallets.length === 0) {
            return dispatch({
              type: ERROR,
              payload: {error: 'Must be at least one wallet.'},
            });
          }

          // Store to storage
          AsyncStorage.setItem(
            walletInitStates.STORAGE_NAME,
            JSON.stringify({...JSON.parse(storageData), wallets}),
          );

          return dispatch({
            type: DELETE_WALLET,
            payload: {
              wallets,
            },
          });
        }
      })
      .catch(e =>
        dispatch({
          type: ERROR,
          payload: {error: 'Failed to get wallet data.'},
        }),
      );
  };
