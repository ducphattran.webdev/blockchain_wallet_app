import AsyncStorage from '@react-native-async-storage/async-storage';
import {LOADING, ERROR, UPDATE_GROUP} from '../../../context/types/Wallet';
import {walletInitStates} from '../../../context/reducer/walletReducer';

export const updateGroup =
  ({name, id}) =>
  dispatch => {
    dispatch({type: LOADING});

    // Get wallet data
    AsyncStorage.getItem(walletInitStates.STORAGE_NAME)
      .then(storageData => {
        const groups =
          storageData && JSON.parse(storageData).groups
            ? JSON.parse(storageData).groups
            : [];
        let isExisted =
          groups.length !== 0 ? groups.findIndex(i => i.id === id) : -1;

        if (isExisted === -1) {
          return dispatch({
            type: ERROR,
            payload: {error: 'This group is not found.'},
          });
        }

        const existedName =
          groups.length !== 0 ? groups.findIndex(i => i.name === name && i.id !== id) : -1;

        if (existedName !== -1) {
          return dispatch({
            type: ERROR,
            payload: {error: 'This name is already taken.'},
          });
        }

        if (isExisted !== -1 && existedName === -1) {
          // Check name
          groups[isExisted].name = name;
        }

        // Store to storage
        AsyncStorage.setItem(
          walletInitStates.STORAGE_NAME,
          JSON.stringify({
            ...JSON.parse(storageData),
            groups,
          }),
        );

        dispatch({
          type: UPDATE_GROUP,
          payload: {groups},
        });

        return true;
      })
      .catch(e =>
        dispatch({
          type: ERROR,
          payload: {error: 'Failed to update group.'},
        }),
      );
  };
