import {LOGIN, LOADING} from '../../types/Auth';

export const loginWithPIN = () => dispatch => {
  dispatch({type: LOADING});
  dispatch({type: LOGIN});
};
