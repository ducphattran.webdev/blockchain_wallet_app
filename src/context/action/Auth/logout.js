import {LOGOUT, LOADING} from '../../types/Auth';

export default () => dispatch => {
  dispatch({type: LOADING});
  dispatch({type: LOGOUT});
};
