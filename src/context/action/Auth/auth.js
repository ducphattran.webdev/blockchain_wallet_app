import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LOADING,
  GET_AUTH_SUCCESS,
  GET_AUTH_FAIL,
  SET_AUTH_SUCCESS,
} from '../../types/Auth';
import {authInitStates} from '../../reducer/authReducer';

export const getAuth = () => dispatch => {
  dispatch({type: LOADING});

  AsyncStorage.getItem(authInitStates.STORAGE_NAME)
    .then(result => {
      if (result) {
        dispatch({type: GET_AUTH_SUCCESS, payload: {...JSON.parse(result)}});
      } else {
        AsyncStorage.setItem(
          authInitStates.STORAGE_NAME,
          JSON.stringify({
            blocked: false,
            block_in: 0,
          }),
        );
        dispatch({
          type: GET_AUTH_SUCCESS,
          payload: {
            blocked: false,
            block_in: 0,
          },
        });
      }
    })
    .catch(e =>
      dispatch({type: GET_AUTH_FAIL, payload: {error: 'Failed to get auth.'}}),
    );
};

export const setAuth =
  ({blocked, block_in}) =>
  dispatch => {
    dispatch({type: LOADING});

    AsyncStorage.setItem(
      authInitStates.STORAGE_NAME,
      JSON.stringify({
        blocked,
        block_in,
      }),
    );

    dispatch({type: SET_AUTH_SUCCESS, payload: {blocked, block_in}});
  };

export const blockAuth =
  ({blocked, block_in}) =>
  dispatch => {
    dispatch({type: LOADING});
    setAuth({block_in, blocked})(dispatch);
  };
