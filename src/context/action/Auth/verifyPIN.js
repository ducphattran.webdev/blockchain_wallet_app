import {VERIFY_SUCCESS, LOADING} from '../../types/Auth';

export const verifyPIN = () => dispatch => {
  dispatch({type: LOADING});
  dispatch({type: VERIFY_SUCCESS});
};
