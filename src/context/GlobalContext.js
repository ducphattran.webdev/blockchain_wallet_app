import React, {
  useContext,
  createContext,
  useReducer,
  useState,
  useEffect,
} from 'react';
// Auth
import {authInitStates, authReducer} from './reducer/authReducer';
// PIN
import {pinInitStates, pinReducer} from './reducer/pinReducer';
// Mnemonic
import {mnemonicInitStates, mnemonicReducer} from './reducer/mnemonicReducer';
// Wallet
import {walletInitStates, walletReducer} from './reducer/walletReducer';
// Network
import {networkInitStates, networkReducer} from './reducer/networkReducer';
// Transaction
import {txInitStates, transactionReducer} from './reducer/transactionReducer';

import {getGroups} from './action/Wallet/getGroup';
import {getPIN} from './action/PIN/getPIN';
import {getMnemonic} from './action/Mnemonic/getMnemonic';
import {getNetworks} from './action/Network/getNetworks';
import {getWallets} from './action/Wallet/getWallet';
import {createGroup} from './action/Wallet/createGroup';

export const GlobalContext = createContext();

export const GlobalProvider = ({children}) => {
  const [version] = useState('0.9.0');
  const [authState, authDispatch] = useReducer(authReducer, authInitStates);
  const [pinState, pinDispatch] = useReducer(pinReducer, pinInitStates);
  const [mnemonicState, mnemonicDispatch] = useReducer(
    mnemonicReducer,
    mnemonicInitStates,
  );
  const [walletState, walletDispatch] = useReducer(
    walletReducer,
    walletInitStates,
  );
  const [networkState, networkDispatch] = useReducer(
    networkReducer,
    networkInitStates,
  );
  const [transactionState, transactionDispatch] = useReducer(
    transactionReducer,
    txInitStates,
  );

  useEffect(() => {
    getPIN()(pinDispatch);
    getMnemonic()(mnemonicDispatch);
    getNetworks()(networkDispatch);
    getGroups()(walletDispatch);
    if (walletState.groups.length === 0) {
      createGroup({name: 'Unassigned'})(walletDispatch);
    }
    getWallets()(walletDispatch);
  }, []);

  return (
    <GlobalContext.Provider
      value={{
        // Version
        version,
        // Auth
        authState,
        authDispatch,
        // PIN
        pinState,
        pinDispatch,
        // Mnemonic
        mnemonicState,
        mnemonicDispatch,
        // Wallet
        walletState,
        walletDispatch,
        // Network
        networkState,
        networkDispatch,
        // Transaction
        transactionState,
        transactionDispatch,
      }}>
      {children}
    </GlobalContext.Provider>
  );
};

export const useGlobalContext = function () {
  return useContext(GlobalContext);
};
