import {StyleSheet} from 'react-native';
import {argonTheme} from '../../../constants';

export default StyleSheet.create({
  container: {flex: 1},
  list_container: {padding: 5, flex: 1},
  load_more_wrapper: {flex: 0.05},
  btn_group_container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  btn_text: {fontWeight: 'bold'},
  tabTitle: {
    color: argonTheme.COLORS.DEFAULT,
    fontWeight: 'bold',
  },
});
