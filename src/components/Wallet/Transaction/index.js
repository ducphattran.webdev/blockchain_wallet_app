import React, {useState, useEffect} from 'react';
import {View, Alert, Text, Linking, FlatList} from 'react-native';
import Loading from '../../common/Loading';
import DetailModal from '../../Modal/DetailTransaction';
import TransactionItem from '../../List/Transaction';
import {
  getHistoryByPage,
  getTransactions,
} from '../../../context/action/Transaction/getHistory';
import {useGlobalContext} from '../../../context/GlobalContext';
import styles from './styles';
import {argonTheme} from '../../../constants';
import {Button, Block, NavBar, theme, Icon} from 'galio-framework';
import {Box, Center, Pressable, Divider} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const HISTORY_TYPES = {
  NORMAL: 0,
  TOKEN: 1,
};
export default function Transaction() {
  const {networkState, walletState, transactionDispatch, transactionState} =
    useGlobalContext();
  const [loading, setLoading] = useState(true);
  const [loadMore, setLoadMore] = useState(false);
  const [transactions, setTransactions] = useState([]);
  const [transaction, setTransaction] = useState(undefined);
  const [showDetailModal, setShowDetailModal] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [historyType, setHistoryType] = useState(HISTORY_TYPES.NORMAL);

  useEffect(() => {
    setLoading(true);
    if (walletState.done && networkState.done) {
      const currentNetwork = networkState.networks.find(n => n.current);
      getTransactions({
        fromAddress: walletState.wallet.address,
        network: currentNetwork,
        type: historyType === HISTORY_TYPES.NORMAL ? 'normal' : 'token',
      })(transactionDispatch);
      setCurrentPage(1);
    }
  }, [walletState, networkState]);

  useEffect(() => {
    if (transactionState.done) {
      // setTransactions(prevList => [
      //   ...prevList,
      //   ...transactionState.transactions,
      // ]);
      setTransactions(transactionState.transactions);
      if (loadMore) {
        setCurrentPage(prevPage => prevPage + 1);
        setLoadMore(false);
      }
      setLoading(false);
    }
  }, [transactionState]);

  useEffect(() => {
    setLoading(true);
    setTransactions([]);
    setCurrentPage(1);
    const currentNetwork = networkState.networks.find(n => n.current);
    getTransactions({
      fromAddress: walletState.wallet.address,
      network: currentNetwork,
      type: historyType === HISTORY_TYPES.NORMAL ? 'normal' : 'token',
    })(transactionDispatch);
  }, [historyType]);

  const showDetail = tx => {
    setTransaction(tx);
    setShowDetailModal(true);
  };

  const viewTxOnExplorer = _hash => {
    setLoading(true);
    const currentNetwork = networkState.networks.find(n => n.current);
    if (currentNetwork) {
      Linking.openURL(`${currentNetwork.explorer}/tx/${_hash}`);
    } else {
      Alert.alert('Error', 'No explorer URL provided.', [
        {
          text: 'OK',
          onPress: () => {},
        },
      ]);
    }
    setLoading(false);
  };

  const loadMoreTransactions = () => {
    setLoadMore(true);
    const currentNetwork = networkState.networks.find(n => n.current);
    getHistoryByPage({
      fromAddress: walletState.wallet.address,
      network: currentNetwork,
      page: currentPage + 1,
      type: historyType === HISTORY_TYPES.NORMAL ? 'normal' : 'token',
    })(transactionDispatch);
  };

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          <Box flex={1}>
            <Box
              flexDirection="row"
              justifyContent="space-around"
              bgColor={argonTheme.COLORS.SECONDARY}>
              <Pressable
                onPress={() => setHistoryType(HISTORY_TYPES.NORMAL)}
                _pressed={{opacity: 0.5}}
                flex={1}
                p={2}
                flexDirection="row"
                justifyContent="center"
                alignItems="center"
                borderRightWidth={1}
                borderRightColor="black">
                <Text
                  size={16}
                  style={[
                    styles.tabTitle,
                    {
                      color:
                        historyType === HISTORY_TYPES.NORMAL
                          ? argonTheme.COLORS.PRIMARY
                          : argonTheme.COLORS.GRAY_300,
                    },
                  ]}>
                  Normal
                </Text>
              </Pressable>
              <Pressable
                onPress={() => setHistoryType(HISTORY_TYPES.TOKEN)}
                _pressed={{opacity: 0.5}}
                flex={1}
                p={2}
                flexDirection="row"
                justifyContent="center"
                alignItems="center"
                borderRightWidth={1}
                borderRightColor="black">
                <Text
                  size={16}
                  style={[
                    styles.tabTitle,
                    {
                      color:
                        historyType === HISTORY_TYPES.TOKEN
                          ? argonTheme.COLORS.PRIMARY
                          : argonTheme.COLORS.GRAY_300,
                    },
                  ]}>
                  Token
                </Text>
              </Pressable>
            </Box>

            <View style={styles.list_container}>
              {/* History */}
              {transactions && (
                <FlatList
                  data={transactions}
                  renderItem={({item, index}) => (
                    <TransactionItem
                      timestamp={item.timestamp}
                      status={item.status}
                      type={item.type}
                      amount={item.value}
                      symbol={item.symbol}
                      onPress={() => showDetail(item)}
                    />
                  )}
                  keyExtractor={item => item.hash}
                  onEndReached={({distanceFromEnd}) => loadMoreTransactions()}
                  onEndReachedThreshold={0.01}
                />
              )}
            </View>

            {/* Load more */}
            {loadMore && (
              <View style={styles.load_more_wrapper}>
                <Loading />
              </View>
            )}
          </Box>

          <DetailModal
            visible={showDetailModal}
            setShowModal={setShowDetailModal}
            transaction={transaction}
            onScan={viewTxOnExplorer}
          />
        </>
      )}
    </>
  );
}
