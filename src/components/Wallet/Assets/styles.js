import {StyleSheet} from 'react-native';
import { argonTheme } from '../../../constants';

export default StyleSheet.create({
  balance_container: {flex: 1},
  list_container: {backgroundColor: argonTheme.COLORS.GRAY_100},
  token_wrapper: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderBottomColor: argonTheme.COLORS.GRAY_300,
  },
  token_img: {
    width: 50,
    height: 50,
    marginRight: 20,
  },
  token_detail_icon: {textAlign: 'right', flex: 1, fontWeight: 'bold'},
  add_token_container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 20,
  },
  add_token_wrapper: {
    backgroundColor: argonTheme.COLORS.BLUE,
    width: '80%',
    borderRadius: 40,
    paddingHorizontal: 20,
    paddingVertical: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  add_token_text: {
    textAlignVertical: 'center',
    textAlign: 'center',
    color: argonTheme.COLORS.WHITE,
    marginLeft: 5,
  },
});
