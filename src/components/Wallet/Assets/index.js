import React, {useState, useEffect} from 'react';
import {View, Text, Pressable, Image} from 'react-native';
import styles from './styles';
import Container from '../../common/Container';
import Loading from '../../common/Loading';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {useGlobalContext} from '../../../context/GlobalContext';
import {argonTheme} from '../../../constants';
import {Box, Center} from 'native-base';
import Button from '../../common/Button';

const Assets = ({setShowAddModal}) => {
  const {walletState} = useGlobalContext();
  const [loading, setLoading] = useState(true);
  const [assets, setAssets] = useState([]);

  useEffect(() => {
    setLoading(true);
    if (walletState.done && walletState.wallet) {
      setAssets(walletState.wallet.assets)
      setLoading(false);
    }
  }, [walletState.wallet]);

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          {/* Assets */}
          <Box flex={1}>
            {/* List */}
            <View style={styles.list_container}>
              <Container style={{padding: 0}}>
                {assets.map(
                  (asset, index) =>
                    asset && (
                      <View key={index} style={styles.token_wrapper}>
                        <View style={styles.token_img}>
                          <View
                            style={[
                              styles.default_token_logo,
                              {
                                backgroundColor: asset.logo
                                  ? 'transparent'
                                  : styles.default_token_logo.backgroundColor,
                              },
                            ]}>
                            {asset.logo && (
                              <Image
                                source={{uri: asset.logo}}
                                resizeMode={'center'}
                                style={{width: '100%', height: '100%'}}
                              />
                            )}
                          </View>
                        </View>
                        <Text>
                          {asset.amount.toString()} {asset.symbol}
                        </Text>
                      </View>
                    ),
                )}
              </Container>
            </View>

            {/* Add token */}
            <Center mt={3}>
              <Button
                onPress={() => setShowAddModal(true)}
                style={{flexDirection: 'row'}}>
                <MaterialIcon
                  name={'add'}
                  size={16}
                  color={argonTheme.COLORS.WHITE}
                />
                <Text style={styles.add_token_text}>Add tokens</Text>
              </Button>
            </Center>
          </Box>
        </>
      )}
    </>
  );
};

export default Assets;
