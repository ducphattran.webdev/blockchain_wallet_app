import React, {useState, useEffect} from 'react';
import { Text} from 'react-native';
import {Box, Center, IconButton, Divider, Pressable} from 'native-base';
import styles from './styles';
import QRCode from '../../QRCode';
import SelectGroup from '../../Modal/SelectGroup';
import Loading from '../../common/Loading';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {useGlobalContext} from '../../../context/GlobalContext';
import {argonTheme} from '../../../constants';
import Input from '../../common/Input';
import Button from '../../common/Button';
import Container from '../../common/Container';

const Info = ({
  wallet,
  privateKey,
  setShowPINModal,
  handleDeleteWallet,
  handleViewWalletOnNetwork,
  copyToClipboard,
  handleUpdate,
}) => {
  const {walletState} = useGlobalContext();
  const [loading, setLoading] = useState(true);
  const [name, setName] = useState(undefined);
  const [group, setGroup] = useState(undefined);
  const [showGroupModal, setShowGroupModal] = useState(false);

  useEffect(() => {
    setName(wallet.name);
    const _group = walletState.groups.find(g => g.id === wallet.groupId);
    setGroup(_group);
    setLoading(false);
  }, []);

  useEffect(() => {
    if (walletState.done) {
      setLoading(false);
    }
  }, [walletState]);

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <Container style={{padding: 0}}>
          <Box flex={1} p={4}>
            {/* QR Code */}
            <Center flex={1}>
              <QRCode size={130} value={wallet && wallet.address} />
              <Text style={styles.qr_code_message}>
                Scan QR code to get the address
              </Text>

              {/* Button */}
              <Center flexDirection="row" m={1}>
                {/* Delete */}
                <Center mr={2}>
                  <IconButton
                    onPress={() => handleDeleteWallet(false)}
                    variant="solid"
                    icon={
                      <MaterialCommunityIcons
                        name={'delete'}
                        size={14}
                        color={argonTheme.COLORS.WHITE}
                      />
                    }
                    bgColor={argonTheme.COLORS.WARNING}
                    _pressed={{opacity: 0.5}}
                  />
                  <Text style={styles.btn_icon_text}>Delete</Text>
                </Center>

                {/* View */}
                <Center>
                  <IconButton
                    onPress={handleViewWalletOnNetwork}
                    variant="solid"
                    icon={
                      <MaterialCommunityIcons
                        name={'search-web'}
                        size={14}
                        color={argonTheme.COLORS.WHITE}
                      />
                    }
                    bgColor={'cyan.700'}
                    _pressed={{opacity: 0.5}}
                  />
                  <Text style={styles.btn_icon_text}>View</Text>
                </Center>
              </Center>
            </Center>

            <Divider m={2} />

            {/* Form */}
            <Box flex={1.5}>
              {/* Name */}
              <Box mb={2}>
                <Text style={styles.form_label}>Name</Text>
                <Input value={name} onChangeText={val => setName(val)} />
              </Box>

              {/* Address */}
              <Box mb={2}>
                <Text style={styles.form_label}>Address</Text>
                <Pressable
                  onPress={() => copyToClipboard(wallet.address)}
                  _pressed={{opacity: 0.5}}
                  flexDirection="row"
                  mt={2}
                  p={2}
                  borderRadius={10}
                  justifyContent="space-between"
                  alignItems="center"
                  bgColor="indigo.100">
                  <Text style={styles.address}>{wallet.address}</Text>
                  <MaterialIcon
                    name={'content-copy'}
                    size={14}
                    color={argonTheme.COLORS.GRAY}
                  />
                </Pressable>
              </Box>

              {/* Private key */}
              {wallet.privateKey !== '' && wallet.privateKey && (
                <Box mb={2}>
                  <Text style={styles.form_label}>Private Key</Text>
                  {privateKey ? (
                    <Pressable
                      onPress={() => copyToClipboard(privateKey)}
                      _pressed={{opacity: 0.5}}
                      flexDirection="row"
                      mt={2}
                      p={2}
                      borderRadius={10}
                      justifyContent="space-between"
                      alignItems="center"
                      bgColor="indigo.100">
                      <Text style={styles.private_key}>{privateKey}</Text>
                      <MaterialIcon
                        name={'content-copy'}
                        size={14}
                        color={argonTheme.COLORS.GRAY}
                      />
                    </Pressable>
                  ) : (
                    <Pressable
                      onPress={() => setShowPINModal(true)}
                      _pressed={{opacity: 0.5}}
                      flexDirection="row"
                      mt={2}
                      p={2}
                      borderRadius={10}
                      justifyContent="center"
                      alignItems="center"
                      bgColor="gray.300">
                      <MaterialCommunityIcons
                        name={'eye'}
                        size={14}
                        color={argonTheme.COLORS.BLACK}
                        style={{marginRight: 5}}
                      />
                      <Text style={styles.show_text}>Show</Text>
                    </Pressable>
                  )}
                </Box>
              )}
              {/* Group */}
              <Box mb={2}>
                <Text style={{fontSize: 12, fontWeight: 'bold'}}>Group</Text>
                <Pressable
                  onPress={() => setShowGroupModal(true)}
                  _pressed={{opacity: 0.5}}
                  flexDirection="row"
                  mt={2}
                  p={2}
                  borderRadius={10}
                  justifyContent="center"
                  alignItems="center"
                  bgColor="indigo.100">
                  <Text
                    style={{fontSize: 11, marginRight: 10, fontWeight: '600'}}>
                    {group && group.name}
                  </Text>
                </Pressable>
              </Box>
              {/* Save */}
              <Center>
                <Button
                  onPress={() => handleUpdate(name, group)}
                  style={{backgroundColor: argonTheme.COLORS.DEFAULT}}>
                  <MaterialIcon
                    name={'save'}
                    size={16}
                    color={argonTheme.COLORS.WHITE}
                  />
                  <Text style={styles.button_text}> Save</Text>
                </Button>
              </Center>
            </Box>

            {showGroupModal && (
              <SelectGroup
                group={group}
                setGroup={setGroup}
                groups={walletState.groups}
                showGroupModal={showGroupModal}
                setShowGroupModal={setShowGroupModal}
              />
            )}
          </Box>
        </Container>
      )}
    </>
  );
};

export default Info;
