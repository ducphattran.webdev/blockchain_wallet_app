import {StyleSheet} from 'react-native';
import {argonTheme} from '../../../constants';

export default StyleSheet.create({
  // Info
  qr_code_message: {color: argonTheme.COLORS.GRAY, padding: 5, fontSize: 10},
  btn_icon_text: {
    fontSize: 9,
    fontWeight: 'bold',
    marginTop: 3,
  },
  form_input: {
    marginVertical: 5,
  },
  form_label: {fontSize: 11, fontWeight: 'bold'},
  field: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
  },
  input: {
    flex: 1,
    color: argonTheme.COLORS.BLACK,
    fontSize: 10,
    borderWidth: 0.3,
    borderRadius: 5,
    paddingHorizontal: 10,
  },
  address_wrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10,
    padding: 10,
  },
  address: {
    padding: 2,
    fontSize: 10,
  },
  pk_wrapper: {
    borderRadius: 10,
    padding: 10,
    marginTop: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  private_key: {
    flex: 1,
    color: argonTheme.COLORS.BLACK,
    fontSize: 10,
    justifyContent: 'center',
    padding: 2,
  },
  show_pk_wrapper: {
    marginTop: 5,
    flexDirection: 'row',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 8,
  },
  show_text: {color: argonTheme.COLORS.BLACK, fontSize: 14},
  action_btn_container: {
    padding: 20,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  button_wrapper: {
    width: '40%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 13,
    borderRadius: 30,
  },
  button_text: {
    color: argonTheme.COLORS.WHITE,
    textAlignVertical: 'center',
    textAlign: 'center',
    textTransform: 'capitalize',
  },
});
