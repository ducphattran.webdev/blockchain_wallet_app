import React from 'react';
import {IconButton} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {argonTheme} from '../../constants';

const CloseButton = function ({setShowModal, bgColor, color}) {
  const _bgColor = bgColor ? bgColor : argonTheme.COLORS.ERROR;
  const _color = color ? color : argonTheme.COLORS.SECONDARY;
  return (
    <IconButton
      onPress={() => setShowModal(false)}
      variant="solid"
      icon={
        <MaterialCommunityIcons name={'close-thick'} size={15} color={_color} />
      }
      bgColor={_bgColor}
      _pressed={{opacity: 0.5}}
    />
  );
};

export default CloseButton;
