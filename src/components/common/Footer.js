import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {useGlobalContext} from '../../context/GlobalContext';
import {argonTheme} from '../../constants';

const Footer = () => {
  const {version} = useGlobalContext();
  return (
    <View style={styles.container}>
      <Text style={styles.wrapper}>v{version}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {alignItems: 'flex-end'},
  wrapper: {fontSize: 10, color: argonTheme.COLORS.GRAY},
});

export default Footer;
