import React from 'react';
import {ScrollView, StyleSheet} from 'react-native';

const Container = ({style, children}) => {
  return <ScrollView style={[styles.container, style]}>{children}</ScrollView>;
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
});

export default Container;
