import React from 'react';
import {ActivityIndicator, View, StyleSheet} from 'react-native';
import {argonTheme} from '../../constants';

const Loading = ({size, color, ...props}) => {
  return (
    <View style={styles.container}>
      <ActivityIndicator
        size={size ? size : 'large'}
        color={color ? color : argonTheme.COLORS.PRIMARY}
        {...props}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Loading;
