import React, {useState, useEffect} from 'react';
import {Text, Alert, Keyboard} from 'react-native';
import styles from './styles';
import {Box, Center, Divider, Heading, Modal} from 'native-base';
import {useGlobalContext} from '../../../context/GlobalContext';
import {createGroup} from '../../../context/action/Wallet/createGroup';
import {updateGroup} from '../../../context/action/Wallet/updateGroup';
import {deleteGroup} from '../../../context/action/Wallet/deleteGroup';
import {argonTheme} from '../../../constants';
import Button from '../../common/Button';
import CloseButton from '../../common/CloseButton';
import Input from '../../common/Input';

const GroupFormModal = ({visible, setShowModal, group, type}) => {
  const {walletState, walletDispatch} = useGlobalContext();
  const [name, setName] = useState('');
  const [error, setError] = useState('');

  useEffect(() => {
    if (type === 'update' && group) {
      setName(group.name);
    }
  }, [type, group]);

  const handleCreate = () => {
    if (name) {
      createGroup({name})(walletDispatch);
      setShowModal(false);
      setName('');
      setError('');
    } else {
      setError('Please provide a name');
    }
  };

  const handleUpdate = () => {
    const currentGroup = walletState.groups.find(g => g.id === group.id);

    const existedName = walletState.groups.findIndex(
      g => g.id !== group.id && name === g.name,
    );

    if (existedName !== -1) {
      return Alert.alert('Error', 'This name is already taken.', [
        {text: 'OK', onPress: () => {}},
      ]);
    }

    if (currentGroup.name !== name && name !== '') {
      updateGroup({name, id: group.id})(walletDispatch);
      setName('');
      setShowModal(false);
    }
  };

  const handleDelete = () => {
    deleteGroup({id: group.id})(walletDispatch);
    setName('');
    setShowModal(false);
  };

  return (
    <Modal isOpen={visible} onClose={setShowModal} size={'lg'}>
      <Modal.Content bgColor={'white'}>
        <Modal.Header
          flexDirection="row"
          justifyContent="space-between"
          alignItems="center">
          <Heading size="sm" color={argonTheme.COLORS.PRIMARY}>
            {type === 'add' ? 'New Group' : 'Update'}
          </Heading>
          <CloseButton setShowModal={setShowModal} />
        </Modal.Header>
        <Modal.Body>
          <Divider my={1} />
          <Box bgColor={'white'} flex={1} borderRadius={10}>
            <Box flex={2} p={2}>
              <Text style={styles.label}>Name</Text>
              <Input
                error={error !== '' ? true : false}
                placeholder="Enter new name here"
                value={name}
                onBlur={() => Keyboard.dismiss()}
                onChangeText={val => setName(val)}
              />
              {error !== '' && <Text style={styles.error}>{error}</Text>}
            </Box>

            <Box flex={1}>
              {type === 'add' ? (
                <Center mt={5}>
                  <Button onPress={handleCreate}>
                    <Text style={styles.btn_white_text}>Add</Text>
                  </Button>
                </Center>
              ) : (
                <Box
                  flexDirection="row"
                  justifyContent="space-around"
                  alignItems="center">
                  <Button onPress={handleDelete} style={styles.btn_delete}>
                    <Text style={styles.btn_white_text}>Delete</Text>
                  </Button>
                  <Button onPress={handleUpdate} style={styles.btn_save}>
                    <Text style={styles.btn_white_text}>Save</Text>
                  </Button>
                </Box>
              )}
            </Box>
          </Box>
        </Modal.Body>
      </Modal.Content>
    </Modal>
  );
};

export default GroupFormModal;
