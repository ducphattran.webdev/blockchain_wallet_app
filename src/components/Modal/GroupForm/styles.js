import {StyleSheet} from 'react-native';
import {argonTheme} from '../../../constants';

export default StyleSheet.create({
  label: {
    fontWeight: 'bold',
  },
  error: {fontSize: 11, color: argonTheme.COLORS.ERROR},
  btn_white_text: {color: argonTheme.COLORS.WHITE},
  btn_save: {backgroundColor: argonTheme.COLORS.DEFAULT, flex: 0.5},
  btn_delete: {backgroundColor: argonTheme.COLORS.ERROR, flex: 0.5},
});
