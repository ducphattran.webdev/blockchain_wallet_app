import {StyleSheet} from 'react-native';
import {argonTheme} from '../../../constants';

export default StyleSheet.create({
  header_title: {
    textTransform: 'capitalize',
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: 'bold',
  },
  body_container: {
    margin: 2,
    padding: 5,
    justifyContent: 'space-between',
  },
  line_title: {color: argonTheme.COLORS.GRAY, fontWeight: "700"},
  line_content_wrapper: {alignItems: 'flex-end'},
  line_content: {
    padding: 5,
    backgroundColor: argonTheme.COLORS.INDIGO_100,
    borderRadius: 10,
    fontSize: 12,
    color: argonTheme.COLORS.DEFAULT,
  },
  amount: {
    padding: 5,
    fontSize: 15,
    fontWeight: 'bold',
    color: argonTheme.COLORS.BLACK,
  },
  fee: {
    padding: 5,
    fontSize: 12,
    fontStyle: 'italic',
    color: argonTheme.COLORS.BLACK,
  },
  status: {
    padding: 5,
    fontSize: 15,
    fontWeight: 'bold',
  },
  date: {
    padding: 5,
    fontSize: 13,
    color: argonTheme.COLORS.BLACK,
  },
  scan_btn_wrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    marginTop: 20,
  },
  scan_btn_text: {color: argonTheme.COLORS.BLUE_50, fontSize: 15},
});
