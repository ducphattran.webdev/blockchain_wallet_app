import React from 'react';
import {View, Text, Modal, Pressable} from 'react-native';
import styles from './styles';
import {trimAddress} from '../../../utils/Wallet';
import moment from 'moment';
import {argonTheme} from '../../../constants';
import CloseButton from '../../common/CloseButton';
import Button from '../../common/Button';
import {Box, Center, Divider} from 'native-base';

const Index = ({visible, setShowModal, transaction, onScan}) => {
  return (
    <>
      {transaction && (
        <Modal animationType={'fade'} visible={visible} transparent={true}>
          <Center flex={1} bgColor={argonTheme.COLORS.TRANSPARENT}>
            <Box
              width="90%"
              bgColor={argonTheme.COLORS.SECONDARY}
              p={2}
              borderRadius={10}>
              {/* Header */}
              <Box
                mb={3}
                flexDirection="row"
                justifyContent="space-between"
                alignItems="center">
                <Center flex={1} flexDirection="row" alignItems="center">
                  <Text
                    style={[
                      styles.header_title,
                      {textTransform: 'capitalize'},
                    ]}>
                    {transaction.type}{' '}
                  </Text>
                  <Text
                    style={[styles.header_title, {textTransform: 'uppercase'}]}>
                    {transaction.symbol}
                  </Text>
                </Center>
                <CloseButton setShowModal={setShowModal} />
              </Box>

              {/* Body */}
              <Box p={2}>
                {/* Hash */}
                <Box my={1} flexDirection="row" justifyContent="space-between">
                  <Text style={styles.line_title}>Hash</Text>
                  <Box>
                    <Text style={styles.line_content}>
                      {trimAddress(transaction.hash, 20)}
                    </Text>
                  </Box>
                </Box>
                {/* From */}
                <Box my={1} flexDirection="row" justifyContent="space-between">
                  <Text style={styles.line_title}>From</Text>
                  <Box>
                    <Text style={styles.line_content}>
                      {trimAddress(transaction.from, 15)}
                    </Text>
                  </Box>
                </Box>
                {/* To */}
                <Box my={1} flexDirection="row" justifyContent="space-between">
                  <Text style={styles.line_title}>To</Text>
                  <Box>
                    <Text style={styles.line_content}>
                      {trimAddress(transaction.to, 15)}
                    </Text>
                  </Box>
                </Box>
                {/* Fee */}
                <Box my={1} flexDirection="row" justifyContent="space-between">
                  <Text style={styles.line_title}>Fee</Text>
                  <Box>
                    <Text style={styles.fee}>
                      {transaction.fee.toString()} {'BNB'}
                    </Text>
                  </Box>
                </Box>
                {/* Amount */}
                <Box my={1} flexDirection="row" justifyContent="space-between">
                  <Text style={styles.line_title}>Amount</Text>
                  <Box>
                    <Text style={styles.amount}>
                      {transaction.value}{' '}
                      {transaction.symbol && transaction.symbol.toUpperCase()}
                    </Text>
                  </Box>
                </Box>
                {/* Status */}
                <Box my={1} flexDirection="row" justifyContent="space-between">
                  <Text style={styles.line_title}>Status</Text>
                  <Box>
                    <Text
                      style={[
                        styles.status,
                        {
                          color: transaction.status
                            ? argonTheme.COLORS.SUCCESS
                            : argonTheme.COLORS.ERROR,
                        },
                      ]}>
                      {transaction.status ? 'Success' : 'Fail'}
                    </Text>
                  </Box>
                </Box>
                {/* Date */}
                <Box my={1} flexDirection="row" justifyContent="space-between">
                  <Text style={styles.line_title}>Date</Text>
                  <Box>
                    <Text style={styles.date}>
                      {moment(transaction.timestamp * 1000).format(
                        'MMM DD YYYY [at] HH:mma',
                      )}
                    </Text>
                  </Box>
                </Box>

                <Divider m={2} />

                <Box  width="100%" alignItems="center">
                  <Button
                    onPress={() => onScan(transaction.hash)}
                    style={{width: "100%"}}>
                    <Text style={styles.scan_btn_text}>See on Network</Text>
                  </Button>
                </Box>
              </Box>
            </Box>
          </Center>
        </Modal>
      )}
    </>
  );
};

export default Index;
