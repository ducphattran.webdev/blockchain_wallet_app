import React, {useState, useEffect} from 'react';
import {View, Modal, SafeAreaView, StyleSheet} from 'react-native';
import Loading from '../../common/Loading';
import {useGlobalContext} from '../../../context/GlobalContext';
import EnterScreen from '../../../screens/Pin/EnterScreen';
import SetupScreen from '../../../screens/Pin/SetupScreen';
import {verifyPIN} from '../../../context/action/Auth/verifyPIN';
import logout from '../../../context/action/Auth/logout';
import {createPIN} from '../../../context/action/PIN/createPIN';
import {navigationRef, navigate} from '../../../navigation/RootNavigator';
import {LOGIN, AUTH_NAVIGATOR} from '../../../constants/routeName';
import CloseButton from '../../../components/common/CloseButton';
import {argonTheme} from '../../../constants';

const PINModal = ({visible, setShowModal, onlyVerify, onFinish}) => {
  const {pinDispatch, authState, authDispatch} = useGlobalContext();
  const [loading, setLoading] = useState(true);
  const [verified, setVerified] = useState(false);

  useEffect(() => {
    if (authState.done) {
      setVerified(authState.isVerified);
      setLoading(false);
    }
  }, [authState]);

  const handleEnterPIN = () => {
    if (!onlyVerify && !onFinish) {
      verifyPIN()(authDispatch);
    } else {
      onFinish();
    }
  };

  const handleSetupPIN = newPinCode => {
    setLoading(true);
    createPIN({pinCode: newPinCode})(pinDispatch);
    logout()(authDispatch);
    setShowModal(false);
    navigate(AUTH_NAVIGATOR, {screen: LOGIN});
  };

  return (
    <Modal animationType={'slide'} visible={visible} transparent={false}>
      {loading ? (
        <Loading />
      ) : (
        <SafeAreaView
          flex={1}
          style={{backgroundColor: argonTheme.COLORS.GRAY_100}}>
          <View style={{flex: 1}}>
            {/* Body */}
            <View style={styles.container}>
              <View style={styles.header}>
                {/* Close Button */}
                <CloseButton setShowModal={setShowModal} bgColor={argonTheme.COLORS.ERROR} color={argonTheme.COLORS.WHITE} />
              </View>
              {!verified && <EnterScreen handleSubmit={handleEnterPIN} />}

              {verified && !onlyVerify && (
                <SetupScreen handleSubmit={handleSetupPIN} />
              )}
            </View>
          </View>
        </SafeAreaView>
      )}
    </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  container: {
    flex: 1,
    backgroundColor: argonTheme.COLORS.GRAY_100,
    padding: 20,
  },
});

export default PINModal;
