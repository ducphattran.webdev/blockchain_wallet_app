import React from 'react';
import {Modal, Pressable, Text, Center, Heading} from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { argonTheme } from '../../../constants';
import CloseButton from '../../common/CloseButton'

const Index = ({
  group,
  setGroup,
  groups,
  showGroupModal,
  setShowGroupModal,
}) => {
  return (
    <>
      <Modal isOpen={showGroupModal} onClose={setShowGroupModal} size="lg" >
        <Modal.Content >
          <Modal.Header
            flexDirection="row"
            justifyContent="space-between"
            alignItems="center">
            <Center>
              <Heading size="md" color={argonTheme.COLORS.PRIMARY}>
                Groups
              </Heading>
            </Center>
            {/* Close Button */}
            <CloseButton setShowModal={setShowGroupModal} />
          </Modal.Header>
          <Modal.Body>
            <Center>
            {groups.map((_group, index) => (
              <Pressable
                onPress={() => setGroup(_group)}
                _pressed={{backgroundColor: 'blue.200'}}
                key={index}
                flex={1}
                p={3}
                bg={_group.id === group.id ? 'blue.100' : 'gray.100'}
                width="100%"
                flexDirection="row"
                alignItems="center"
                justifyContent="space-between">
                <Text>{_group.name}</Text>
                {_group.id === group.id && (
                  <Icon name={'check'} size={20} color={'green'} />
                )}
              </Pressable>
            ))}
            </Center>
          </Modal.Body>
        </Modal.Content>
      </Modal>
    </>
  );
};

export default Index;
