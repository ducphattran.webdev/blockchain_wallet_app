import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Modal,
  TextInput,
  Pressable,
  Alert,
  SafeAreaView,
} from 'react-native';
import Loading from '../../common/Loading';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useGlobalContext} from '../../../context/GlobalContext';
import {importWalletByPK} from '../../../context/action/Wallet/importWallet';
import styles from './styles';
import {Box, IconButton} from 'native-base';
import {argonTheme} from '../../../constants';

const ImportWalletModal = ({visible, setShowImportModal, groupId}) => {
  const {walletState, mnemonicState, walletDispatch} = useGlobalContext();
  const [loading, setLoading] = useState(true);
  const [privateKey, setPrivateKey] = useState('');

  useEffect(() => {
    if (walletState.done) {
      setLoading(false);
      if (walletState.error) {
        return Alert.alert('Failed', walletState.error, [
          {
            text: 'OK',
            onPress: () => {},
          },
        ]);
      }
      setShowImportModal(false);
    }
  }, [walletState]);

  const handleImportWallet = () => {
    setLoading(true);
    importWalletByPK({
      randomString: mnemonicState.randomString,
      privateKey,
      groupId,
    })(walletDispatch);
  };

  return (
    <Modal animationType={'slide'} visible={visible} transparent={false}>
      {loading ? (
        <Loading />
      ) : (
        <SafeAreaView flex={1}>
          {/* Header */}
          <Box flex={1} p={5} bgColor={argonTheme.COLORS.INDIGO_100}>
            {/* Close Button */}
            <Box style={styles.close_btn}>
              <IconButton
                onPress={() => setShowImportModal(false)}
                variant="solid"
                icon={
                  <MaterialCommunityIcons
                    name={'close-thick'}
                    size={16}
                    color="black"
                  />
                }
                bgColor="gray.100"
                _pressed={{backgroundColor: 'gray.300'}}
              />
            </Box>
            <Box mb={2}>
              <MaterialIcon name={'save-alt'} size={50} />
              <Text style={styles.title}>Import wallet</Text>
            </Box>
            <View style={{paddingVertical: 5}}>
              <Text>Imported wallet by private key</Text>
            </View>
          </Box>
          {/* Body */}
          <Box flex={2} bgColor="gray.100" p={6}>
            <View style={{}}>
              <Text style={styles.form_title}>
                Paste your private key string
              </Text>
              <TextInput
                onChangeText={val => setPrivateKey(val)}
                blurOnSubmit={true}
                style={styles.form_input}
                multiline={true}
                placeholder={
                  'eg: 39A337a84CEdAE92679EA0bc2aa2396D325c8F66396D325c8F66'
                }
              />
            </View>

            <Pressable
              onPress={handleImportWallet}
              style={({pressed}) => [
                styles.form_btn,
                {opacity: pressed ? 0.6 : 1},
              ]}>
              <Text style={styles.form_btn_text}>import</Text>
            </Pressable>
          </Box>
        </SafeAreaView>
      )}
    </Modal>
  );
};

export default ImportWalletModal;
