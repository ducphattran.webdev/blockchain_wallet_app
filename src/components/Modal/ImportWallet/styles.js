import {StyleSheet} from 'react-native';
import { argonTheme } from '../../../constants';

export default StyleSheet.create({
  close_btn: {alignItems: 'flex-end'},
  title: {
    fontSize: 30,
    marginVertical: 15,
    textTransform: 'capitalize',
  },
  form_title: {fontSize: 16, fontWeight: 'bold', marginBottom: 10},
  form_input: {
    padding: 15,
    borderWidth: 1,
    borderColor: argonTheme.COLORS.GRAY_300,
    borderRadius: 10,
    marginVertical: 10,
    height: 80,
  },
  form_btn: {
    backgroundColor: argonTheme.COLORS.PRIMARY,
    marginTop: 10,
    padding: 10,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  form_btn_text: {color: argonTheme.COLORS.WHITE, textTransform: 'uppercase'},
});
