import {StyleSheet} from 'react-native';
import { argonTheme } from '../../../constants';

export default StyleSheet.create({
  panel: {
    flex: 1,
    backgroundColor: argonTheme.COLORS.WHITE,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    backgroundColor: argonTheme.COLORS.BLUE_100,
  },
  title_wrapper: {flex: 1, alignItems: "center"},
  title: {
    fontSize: 20,
    fontWeight: "bold"
  },
  close_btn: {
    color: argonTheme.COLORS.BLACK,
  },
  wrapper: {
    flex: 1,
    borderTopWidth: 0.5,
    borderTopColor: argonTheme.COLORS.GRAY_300,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  info_container: {flexDirection: 'row', flex: 1, alignItems: "center"},
  avatar: {
    backgroundColor: argonTheme.COLORS.DEFAULT,
    width: 50,
    height: 50,
    marginRight: 15,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar_name: {fontSize: 8, color: argonTheme.COLORS.WHITE},
  info_wrapper: {justifyContent: 'space-around'},
  name: {
    color: argonTheme.COLORS.BLACK,
    fontSize: 16,
    fontWeight: '700',
  },
  address: {
    width: '100%',
    backgroundColor: argonTheme.COLORS.GRAY_300,
    borderWidth: 1,
    fontSize: 10,
    paddingHorizontal: 5,
    paddingVertical: 2,
    color: argonTheme.COLORS.BLACK,
    borderColor: argonTheme.COLORS.GRAY_300,
    borderRadius: 10,
    textAlignVertical: 'center',
    marginVertical: 5,
  },
  transfer_icon_wrapper: {
    justifyContent: 'center',
    width: 20,
    height: 20,
    borderRadius: 20,
    alignItems: 'center',
  },
  balance: {color: argonTheme.COLORS.GRAY, fontSize: 12},
  btn_wrapper: {
    backgroundColor: argonTheme.COLORS.GRAY_100,
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn_text: {color: argonTheme.COLORS.BLUE, textTransform: 'capitalize'},
  delete_btn_wrapper: {
    justifyContent: 'center',
    padding: 10,
  },
});
