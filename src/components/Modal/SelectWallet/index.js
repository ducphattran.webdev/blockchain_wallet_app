import React, {useEffect, useState, useMemo} from 'react';
import {View, Text, Modal, Pressable, SafeAreaView} from 'react-native';
import {Avatar} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';
import Container from '../../common/Container';
import Loading from '../../common/Loading';
import {useGlobalContext} from '../../../context/GlobalContext';
import {getOneWallet} from '../../../context/action/Wallet/getWallet';
import {trimAddress} from '../../../utils/Wallet';
import {Box, Center, Heading} from 'native-base';
import CloseButton from '../../common/CloseButton';
import {argonTheme} from '../../../constants';

const WalletModal = ({
  visible,
  setShowWalletModal,
  fromWallet,
  setFromWallet,
  toAddress,
  setToAddress,
  selectType,
}) => {
  const {walletState, walletDispatch} = useGlobalContext();
  const [loading, setLoading] = useState(true);
  const list = useMemo(() => {
    let wallets = walletState.wallets;
    if (selectType === 'from') {
      wallets = wallets.filter(
        wallet => wallet.privateKey !== '' && wallet.privateKey != null,
      );
    }

    const groupIds = [...new Set(wallets.map(w => w.groupId))];
    const groups = walletState.groups.filter(w => groupIds.includes(w.id));
    return {
      wallets,
      groups,
    };
  }, [walletState.groups, walletState.wallets, selectType]);

  useEffect(() => {
    setLoading(true);
    if (walletState.done) {
      setLoading(false);
    }
  }, [walletState]);

  const handleSelectWallet = _wallet => {
    setLoading(true);
    if (selectType === 'from') {
      setFromWallet(_wallet);
      if (toAddress && toAddress === _wallet.address) {
        setToAddress(fromWallet.address);
      }
      getOneWallet({address: _wallet.address})(walletDispatch);
    } else if (selectType === 'to') {
      setToAddress(_wallet.address);
      if (
        fromWallet &&
        fromWallet.address.toLowerCase() === _wallet.address.toLowerCase()
      ) {
        const duplicated = walletState.wallets.find(
          w => toAddress && w.address.toLowerCase() === toAddress.toLowerCase(),
        );

        if (duplicated) {
          setFromWallet(duplicated);
          getOneWallet({address: duplicated.address})(walletDispatch);
        }
      }
      setLoading(false);
      setShowWalletModal(false);
    }
  };

  return (
    <Modal transparent={true} visible={visible} animationType={'fade'}>
      <SafeAreaView flex={1} width="100%">
        <View style={[styles.panel]}>
          <Box flex={1} p={2}>
            {/* Header */}
            <Box
              flexDirection="row"
              justifyContent="space-between"
              alignItems="center">
              <Center>
                <Heading size="lg" color={argonTheme.COLORS.PRIMARY}>
                  Wallets
                </Heading>
              </Center>
              {/* Close Button */}
              <CloseButton setShowModal={setShowWalletModal} />
            </Box>

            {/* Wallets */}
            {loading ? (
              <Loading />
            ) : (
              <>
                <Container style={{padding: 10}}>
                  {/* Body */}
                  {list.groups.map(
                    (group, index) =>
                      group !== 0 && (
                        <View key={index}>
                          <View style={{padding: 5}}>
                            <Text
                              style={{
                                color: argonTheme.COLORS.ERROR,
                                fontWeight: 'bold',
                              }}>
                              {group.name}
                            </Text>
                          </View>
                          {list.wallets.map(
                            (wallet, i) =>
                              wallet.groupId === group.id && (
                                <View key={i} style={{marginBottom: 5}}>
                                  <Pressable
                                    disabled={
                                      selectType === 'from'
                                        ? fromWallet !== undefined &&
                                          fromWallet.address.toLowerCase() ===
                                            wallet.address.toLowerCase()
                                        : toAddress !== undefined &&
                                          toAddress.toLowerCase() ===
                                            wallet.address.toLowerCase()
                                    }
                                    onPress={() => handleSelectWallet(wallet)}
                                    style={({pressed}) => [
                                      styles.wrapper,
                                      {opacity: pressed ? 0.5 : 1},
                                    ]}>
                                    <View style={styles.info_container}>
                                      <Avatar
                                        size="md"
                                        mr={4}
                                        bgColor={argonTheme.COLORS.DEFAULT}>
                                        <Text style={{color: 'white'}}>
                                          {wallet.name.slice(0, 1)}
                                        </Text>
                                      </Avatar>
                                      <View style={styles.info_wrapper}>
                                        <Text style={styles.name}>
                                          {wallet.name}
                                        </Text>
                                        <Text style={styles.address}>
                                          {wallet !== undefined &&
                                            trimAddress(wallet.address, 18)}
                                        </Text>
                                      </View>
                                    </View>

                                    {fromWallet !== undefined &&
                                      fromWallet.address.toLowerCase() ===
                                        wallet.address.toLowerCase() && (
                                        <Box
                                          bgColor={argonTheme.COLORS.WARNING}
                                          style={styles.transfer_icon_wrapper}>
                                          <MaterialCommunityIcons
                                            name={
                                              'arrow-top-right-bold-outline'
                                            }
                                            size={15}
                                            color={argonTheme.COLORS.WHITE}
                                          />
                                        </Box>
                                      )}

                                    {toAddress !== undefined &&
                                      toAddress.toLowerCase() ===
                                        wallet.address.toLowerCase() && (
                                        <Box
                                          bgColor={argonTheme.COLORS.BLUE}
                                          style={styles.transfer_icon_wrapper}>
                                          <MaterialCommunityIcons
                                            name={'arrow-down-bold-outline'}
                                            size={15}
                                            color={argonTheme.COLORS.WHITE}
                                          />
                                        </Box>
                                      )}
                                  </Pressable>
                                </View>
                              ),
                          )}
                        </View>
                      ),
                  )}
                </Container>
              </>
            )}
          </Box>
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export default WalletModal;
