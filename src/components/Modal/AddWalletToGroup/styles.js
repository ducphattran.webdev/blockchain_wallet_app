import {StyleSheet} from 'react-native';
import {argonTheme} from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: argonTheme.COLORS.TRANSPARENT,
  },
  wrapper: {
    backgroundColor: argonTheme.COLORS.WHITE,
    flex: 0.5,
    marginVertical: '50%',
    alignSelf: 'center',
    width: '90%',
    padding: 10,
    borderRadius: 10,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  button: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'flex-start',
    paddingHorizontal: 15,
  },
  btn_white_text: {marginLeft: 8, color: argonTheme.COLORS.WHITE},
});
