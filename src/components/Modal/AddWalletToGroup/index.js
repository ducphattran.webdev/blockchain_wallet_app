import React, {useState} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {Box, Center, Divider, Heading, Modal} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';
import {useGlobalContext} from '../../../context/GlobalContext';
import {createWallet} from '../../../context/action/Wallet/createWallet';
import {argonTheme} from '../../../constants';
import CloseButton from '../../common/CloseButton';
import Button from '../../common/Button';
import Input from '../../common/Input';
import {checkValidAddress} from '../../../utils/Web3/Utility';
import {addWalletToGroup} from '../../../context/action/Wallet/addWalletToGroup';

const Index = ({
  visible,
  setShowModal,
  groupId,
  setShowImportModal,
  showScanner,
}) => {
  const {
    mnemonicState: {randomString},
    walletDispatch,
  } = useGlobalContext();
  const [address, setAddress] = useState('');
  const [error, setError] = useState('');

  const onCreateWallet = () => {
    createWallet({randomString, groupId})(walletDispatch);
    setShowModal(false);
  };

  const onImportWallet = () => {
    setShowImportModal(true);
    setShowModal(false);
  };

  const onScanQRCode = () => {
    setShowModal(false);
    showScanner(true);
  };

  const onAddAddress = async () => {
    if (address === '' || !address) {
      return setError('Please input an address.');
    }
    const [isValid] = await checkValidAddress(address);

    if (isValid) {
      console.log(address, groupId);
      addWalletToGroup({address, groupId})(walletDispatch);
      setShowModal(false);
      setError('');
    } else {
      setError('Invalid address.');
    }
  };

  return (
    <Modal isOpen={visible} onClose={setShowModal} size={'lg'}>
      <Modal.Content>
        <Modal.Header>
          <Center mb={1}>
            <CloseButton setShowModal={setShowModal} />
          </Center>
        </Modal.Header>
        <Modal.Body>
          <Center p={1}>
            <Box
              flexDirection="row"
              alignItems="center"
              p={1}
              justifyContent="flex-start"
              width="100%">
              <Box mr={1}>
                <MaterialCommunityIcons
                  name={'shield-key'}
                  size={17}
                  color={argonTheme.COLORS.DEFAULT}
                />
              </Box>
              <Heading size="xs">Own Private Key</Heading>
            </Box>
            <Divider my={1} />

            <Button style={styles.button} onPress={onCreateWallet}>
              <MaterialCommunityIcons
                name={'wallet-plus'}
                size={20}
                color={argonTheme.COLORS.WHITE}
              />
              <Text style={styles.btn_white_text}>Create new wallet</Text>
            </Button>

            <Button style={styles.button} onPress={onImportWallet}>
              <MaterialCommunityIcons
                name={'import'}
                size={20}
                color={argonTheme.COLORS.WHITE}
              />
              <Text style={styles.btn_white_text}>Import by private key</Text>
            </Button>

            <Box
              flexDirection="row"
              alignItems="center"
              p={1}
              mt={4}
              justifyContent="flex-start"
              width="100%">
              <Box mr={1}>
                <MaterialCommunityIcons
                  name={'alpha-a-box'}
                  size={17}
                  color={argonTheme.COLORS.DEFAULT}
                />
              </Box>
              <Heading size="xs">Only address</Heading>
            </Box>
            <Divider my={1} />

            <Center width="100%">
              <Input
                style={{width: '100%'}}
                error={error !== '' ? true : false}
                placeholder="Put address here"
                value={address}
                onChangeText={val => setAddress(val)}
              />
              {error !== '' && (
                <Text style={{fontSize: 10, color: argonTheme.COLORS.ERROR}}>
                  {error}
                </Text>
              )}
              <Button style={styles.button} size="small" onPress={onAddAddress}>
                <Text
                  style={[
                    styles.btn_white_text,
                    {marginLeft: 0, textAlign: 'center', flex: 1},
                  ]}>
                  Add address
                </Text>
              </Button>
            </Center>

            {/* Divider */}
            <Box bgColor="white" width="100%" flexDirection="row">
              <Divider my={1} />
              <Box
                width="100%"
                position="absolute"
                top={-1}
                alignItems="center">
                <Text style={{fontSize: 10, color: argonTheme.COLORS.GRAY}}>
                  OR
                </Text>
              </Box>
            </Box>

            <Button style={styles.button} onPress={onScanQRCode}>
              <MaterialCommunityIcons
                name={'qrcode-scan'}
                size={20}
                color={argonTheme.COLORS.WHITE}
              />
              <Text style={styles.btn_white_text}>Scan QR Code</Text>
            </Button>
          </Center>
        </Modal.Body>
      </Modal.Content>
    </Modal>
  );
};

export default Index;
