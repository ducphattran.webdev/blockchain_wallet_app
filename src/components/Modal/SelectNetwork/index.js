import React from 'react';
import {View, Modal, StyleSheet} from 'react-native';
import {argonTheme} from '../../../constants';
import Network from '../../List/Network';

const SelectNetwork = ({
  visible,
  networks,
  handleSwitching,
  setShowModal,
  selectedNet,
}) => {
  return (
    <Modal transparent={true} visible={visible} animationType={'fade'}>
      <View style={styles.container}>
        <Network
          networks={networks}
          handleSwitching={handleSwitching}
          setShowModal={setShowModal}
          selectedNet={selectedNet}
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: argonTheme.COLORS.TRANSPARENT,
  },
});

export default SelectNetwork;
