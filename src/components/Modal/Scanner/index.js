import React from 'react';
import {View, Text, Modal, SafeAreaView} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from './styles';
import CloseButton from '../../../components/common/CloseButton';
import {argonTheme} from '../../../constants';

const Index = ({visible, setShowModal, onSuccess}) => {
  return (
    <Modal visible={visible} animationType={'fade'}>
      <SafeAreaView flex={1} width="100%">
        <View style={styles.container}>
          <QRCodeScanner
            onRead={onSuccess}
            fadeIn={true}
            showMarker={true}
            containerStyle={styles.scanner_container}
            cameraType={'back'}
            cameraStyle={styles.camera}
            markerStyle={styles.marker}
            topViewStyle={styles.top_content}
            topContent={
              <CloseButton
                setShowModal={setShowModal}
                bgColor={argonTheme.COLORS.ERROR}
                color={argonTheme.COLORS.WHITE}
              />
            }
            bottomViewStyle={styles.bottom_content}
            bottomContent={<Text>scanning...</Text>}
          />
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export default Index;
