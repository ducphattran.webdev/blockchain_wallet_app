import {StyleSheet} from 'react-native';
import { argonTheme } from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: argonTheme.COLORS.GRAY_100,
  },
  scanner_container: {flex: 1},
  camera: {flex: 1, height: '100%'},
  marker: {borderColor: argonTheme.COLORS.GRAY, borderRadius: 20},
  top_content: {
    flex: 0.1,
    alignItems: 'flex-end',
    paddingHorizontal: 10,
  },
  bottom_content: {flex: 0.1},
});
