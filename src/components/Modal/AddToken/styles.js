import {StyleSheet} from 'react-native';
import { argonTheme } from '../../../constants';


export default StyleSheet.create({
  header: {flexDirection: 'row', backgroundColor: argonTheme.COLORS.BLUE_50},
  close_btn: {flex: 0.1, alignItems: 'center', justifyContent: 'center'},
  title_container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  title: {
    padding: 15,
    fontSize: 14,
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  form_label: {
    fontWeight: 'bold',
    marginBottom: 5,
  },
  form_input: {
    fontSize: 12,
    padding: 15,
    borderColor: argonTheme.COLORS.GRAY,
    borderWidth: 1,
    borderRadius: 5,
  },
  form_btn: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderWidth: 1,
    margin: 10,
    padding: 10,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  form_cancel_btn: {
    borderColor: argonTheme.COLORS.GRAY,
  },
  form_add_btn: {
    borderColor: argonTheme.COLORS.BLUE,
  },
  form_btn_text: {color: argonTheme.COLORS.WHITE, textTransform: 'uppercase'},
});
