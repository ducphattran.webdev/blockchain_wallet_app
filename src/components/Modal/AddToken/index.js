import React, {useState, useEffect} from 'react';
import {Text, SafeAreaView} from 'react-native';
import {Divider, Box, Center, Heading, Modal} from 'native-base';
import styles from './styles';
import {argonTheme} from '../../../constants';
import Loading from '../../../components/common/Loading';
import CloseButton from '../../../components/common/CloseButton';
import Button from '../../../components/common/Button';
import Input from '../../../components/common/Input';
import {useGlobalContext} from '../../../context/GlobalContext';
import {getToken} from '../../../utils/Web3/Utility';
import {addToken} from '../../../context/action/Wallet/addToken';
import {getOneWallet} from '../../../context/action/Wallet/getWallet';

const STAGES = {
  GET_INFO: 0,
  VALID: 1,
  ERROR: -1,
};
const Index = ({visible, setShowModal, wallet}) => {
  const {walletState, walletDispatch, networkState} = useGlobalContext();
  const [stage, setStage] = useState(STAGES.GET_INFO);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');
  const [address, setAddress] = useState('');
  const [name, setName] = useState('');
  const [symbol, setSymbol] = useState('');
  const [decimals, setDecimals] = useState('');

  useEffect(() => {
    resetInput();
    setStage(STAGES.GET_INFO);
  }, [visible, setShowModal]);

  useEffect(() => {
    if (walletState.done) {
      setLoading(false);
    }
  }, [walletState]);

  const resetInput = () => {
    setName('');
    setSymbol('');
    setDecimals('');
  };

  const getInfo = () => {
    setLoading(true);
    if (address === '') {
      setError('Please provide an address.');
      return setLoading(false);
    }
    getToken(address).then(data => {
      const [token, err] = data;

      if (token) {
        setName(token.name);
        setSymbol(token.symbol);
        setDecimals(token.decimals);
        // setIsValid(true);
        setError('');
        setStage(STAGES.VALID);
        // setSubmitted(true);
      }
      if (err) {
        // setIsValid(false);
        setStage(STAGES.ERROR);
        setError('Can not find any token with this address.');
        resetInput();
      }
      setLoading(false);
    });
  };

  const handleSubmit = () => {
    const isExisted = wallet.assets.findIndex(
      i => i && i.address && i.address.toLowerCase() === address.toLowerCase(),
    );
    if (isExisted !== -1) {
      setError('This token is already in the list.');
      setStage(STAGES.ERROR);
    }
    if (address && isExisted === -1) {
      const currentNetwork = networkState.networks.find(n => n.current);
      addToken(wallet, {
        address: address,
        symbol: symbol,
        decimals: decimals,
        networkId: currentNetwork && currentNetwork.id,
      })(walletDispatch);
      getOneWallet({address: wallet.address})(walletDispatch);
      setShowModal(false);
      setStage(STAGES.GET_INFO);
    }
  };

  return (
    <Modal isOpen={visible} onClose={setShowModal} size="full">
      <Modal.Content>
        <Modal.Header
          flexDirection="row"
          justifyContent="space-between"
          alignItems="center">
          <Center>
            <Heading size="md" color={argonTheme.COLORS.PRIMARY}>
              Wallets
            </Heading>
          </Center>
          {/* Close Button */}
          <CloseButton setShowModal={setShowModal} />
        </Modal.Header>

        <Modal.Body>
          <Divider />
          <Box mt={4}>
            {/* Token address */}
            <Box>
              <Text style={styles.form_label}>Token Address</Text>
              <Input
                error={error ? true : false}
                onChangeText={val => {
                  setAddress(val);
                  setStage(STAGES.GET_INFO);
                }}
                onEndEditing={() => getInfo()}
                placeholder={'eg: 0xe9e7cea3dedca5984780bafc599bd69add087d56'}
                value={address}
              />
            </Box>

            {loading && <Loading />}

            {error && (
              <Box p={2}>
                <Text
                  style={{
                    color: argonTheme.COLORS.ERROR,
                    fontSize: 12,
                    fontWeight: '600',
                  }}>
                  {error}
                </Text>
              </Box>
            )}

            {stage === STAGES.VALID && (
              <>
                <Box bgColor="white" width="100%" flexDirection="row">
                  <Divider my={1} />
                  <Box
                    width="100%"
                    position="absolute"
                    top={-1}
                    alignItems="center">
                    <Text style={{fontSize: 10, color: argonTheme.COLORS.GRAY}}>
                      INFORMATION
                    </Text>
                  </Box>
                </Box>

                <Box>
                  {/* Name */}
                  <Box m={2}>
                    <Text style={styles.form_label}>Name</Text>
                    <Input editable={false} value={name} />
                  </Box>

                  {/* Symbol */}
                  <Box m={2}>
                    <Text style={styles.form_label}>Symbol</Text>
                    <Input editable={false} value={symbol} />
                  </Box>

                  {/* Decimals */}
                  <Box m={2}>
                    <Text style={styles.form_label}>Precision</Text>
                    <Input editable={false} value={decimals} />
                  </Box>
                </Box>
              </>
            )}

            {/* Button */}
            {[STAGES.GET_INFO, STAGES.ERROR].includes(stage) && (
              <Center>
                <Button
                  onPress={() => getInfo()}
                  // disabled={address.length === 0}
                >
                  <Text style={[styles.form_btn_text]}>Find</Text>
                </Button>
              </Center>
            )}

            {stage === STAGES.VALID && (
              <Center>
                <Button
                  // disabled={error}
                  onPress={() => handleSubmit()}>
                  <Text style={[styles.form_btn_text]}>Add</Text>
                </Button>
              </Center>
            )}
          </Box>
        </Modal.Body>
      </Modal.Content>
    </Modal>
  );
};

export default Index;
