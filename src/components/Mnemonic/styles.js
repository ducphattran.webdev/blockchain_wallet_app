import {StyleSheet} from 'react-native';
import {argonTheme} from '../../constants';

export default StyleSheet.create({
  progress_stage_text: {
    flex: 1,
    marginTop: 8,
    textAlign: 'center',
    fontSize: 8,
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: 'bold',
  },
  message_title: {fontSize: 18, fontWeight: 'bold'},
  message_text: {
    paddingHorizontal: 10,
    marginTop: 15,
    fontSize: 12,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});
