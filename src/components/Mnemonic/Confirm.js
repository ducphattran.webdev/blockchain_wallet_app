import React, {useState} from 'react';
import {SafeAreaView, Alert} from 'react-native';
import Header from './Header';
import ProgressBar from './ProgressBar';
import {mnemonicInitStates} from '../../context/reducer/mnemonicReducer';
import {Center, Box, Pressable, Text} from 'native-base';
import {argonTheme} from '../../constants';

const STAGES = mnemonicInitStates.STAGES;
const title = 'Confirm Secret Phrase';
const message = 'Select each word in the order it was presented to you.';

const Confirm = ({
  onFinish,
  shuffleWords,
  setShuffleWords,
  chosenWords,
  setChosenWords,
}) => {
  const [currentIndex, setCurrentIndex] = useState(0);

  const unselect = word => {
    setChosenWords(prevWords =>
      prevWords.map(w => {
        if (w.value === word.value) {
          return {
            ...w,
            chosen: false,
            isCorrect: false,
            value: '',
          };
        }
        return w;
      }),
    );
    setShuffleWords(prevWords =>
      prevWords.map(w => {
        if (w.value === word.value) {
          return {...w, chosen: false};
        }
        return w;
      }),
    );
    setCurrentIndex(word.id);
  };

  const select = word => {
    setChosenWords(prevWords =>
      prevWords.map(w => {
        if (w.id === currentIndex) {
          return {
            ...w,
            chosen: true,
            isCorrect: w.orderValue === word.value,
            value: word.value,
          };
        }
        return w;
      }),
    );
    setShuffleWords(prevWords =>
      prevWords.map(w => {
        if (w.id === word.id) {
          return {...w, chosen: true};
        }
        return w;
      }),
    );

    setCurrentIndex(prevIndex => {
      if (chosenWords.filter(item => item.chosen).length === 0) {
        return prevIndex + 1;
      } else {
        const nextIndex = chosenWords.findIndex(
          item => !item.chosen && item.value === '' && item.id !== currentIndex,
        );

        if (nextIndex === -1) {
          return chosenWords.length;
        }
        return nextIndex;
      }
    });
  };

  const handleSelection = word => {
    if (word.chosen) {
      return unselect(word);
    } else if (!word.chosen) {
      return select(word);
    }
  };

  const handleComplete = () => {
    const isCorrect = chosenWords.findIndex(word => !word.isCorrect);
    if (isCorrect !== -1) {
      Alert.alert('Error', 'Secret phrase is not in the correct order.', [
        {
          text: 'Try again',
        },
      ]);
    } else {
      return onFinish();
    }
  };

  return (
    <>
      <SafeAreaView flex={1}>
        {/* Progress bar */}
        <ProgressBar currentStage={STAGES['Confirm Secure Phrase']} />

        {/* Header */}
        <Header title={title} message={message} />

        {/* Phrase */}
        <Center flex={3} p={6}>
          <Box
            flex={1}
            borderWidth={0.5}
            borderColor={argonTheme.COLORS.GRAY_700}
            borderRadius={10}
            width="100%"
            flexWrap="wrap"
            flexDirection="row"
            py={3}
            justifyContent="center"
            alignItems="center">
            {chosenWords.map(word => (
              <Box key={word.id} width="45%" m={1.5} flexDirection="row">
                <Text
                  p={1}
                  mr={1}
                  width={8}
                  textAlign="center"
                  textAlignVertical="center">
                  {word.id + 1}.
                </Text>
                {word.chosen ? (
                  <Pressable
                    onPress={() => handleSelection(word)}
                    _pressed={{backgroundColor: 'gray.300'}}
                    flex={1}
                    p={1}
                    borderWidth={1}
                    borderColor={argonTheme.COLORS.PRIMARY}
                    borderRadius={50}>
                    <Text textAlign="center" fontSize="sm">
                      {word.value}
                    </Text>
                  </Pressable>
                ) : (
                  <Pressable
                    onPress={() => setCurrentIndex(word.id)}
                    _pressed={{backgroundColor: 'indigo.400'}}
                    flex={1}
                    p={1}
                    borderWidth={1}
                    borderColor={
                      currentIndex === word.id ? 'green.500' : 'gray.300'
                    }
                    borderRadius={50}
                    borderStyle="dashed">
                    <Text textAlign="center" fontSize="sm"></Text>
                  </Pressable>
                )}
              </Box>
            ))}
          </Box>
        </Center>

        {/* Select */}
        <Center flex={2} p={2}>
          <Box
            flex={1}
            flexDirection="row"
            flexWrap="wrap"
            justifyContent="space-evenly"
            px={3}
            py={1}>
            {shuffleWords &&
              shuffleWords.map(word => (
                <Pressable
                  disabled={word.chosen}
                  onPress={() => handleSelection(word)}
                  _pressed={{backgroundColor: argonTheme.COLORS.INDIGO_400}}
                  key={word.id}
                  width="30%"
                  bgColor={word.chosen ? 'gray.300' : argonTheme.COLORS.PRIMARY}
                  p={1.5}
                  m={1}
                  borderRadius={50}>
                  <Text
                    width="100%"
                    color="white"
                    textAlign="center"
                    fontSize="sm">
                    {word.value}
                  </Text>
                </Pressable>
              ))}
          </Box>
        </Center>

        {/* Complete Button  */}
        <Pressable
          flex={0.2}
          onPress={handleComplete}
          disabled={currentIndex !== shuffleWords.length}
          _pressed={{backgroundColor: argonTheme.COLORS.INDIGO_400}}
          bgColor={
            currentIndex === shuffleWords.length
              ? argonTheme.COLORS.PRIMARY
              : 'gray.300'
          }
          p={4}
          mt={1}
          alignItems="center">
          <Text
            color={currentIndex === shuffleWords.length ? 'white' : 'gray.400'}>
            Complete
          </Text>
        </Pressable>
      </SafeAreaView>
    </>
  );
};

export default Confirm;
