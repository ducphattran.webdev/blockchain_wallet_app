import React from 'react';
import styles from './styles';
import {mnemonicInitStates} from '../../context/reducer/mnemonicReducer';
import {Center, Box, HStack, VStack, Text} from 'native-base';
import {argonTheme} from '../../constants';

const STAGES = mnemonicInitStates.STAGES;

const ProgressBar = ({currentStage}) => {
  return (
    <Center flex={0.5} p={3}>
      <HStack flex={1}>
        {Object.keys(STAGES).map(name => (
          <Box
            flex={1}
            justifyContent="center"
            alignItems="center"
            key={STAGES[name]}>
            <VStack justifyContent="center" alignItems="center">
              {STAGES[name] !== Object.keys(STAGES).length && (
                <Box
                  position="absolute"
                  height={1}
                  bgColor={argonTheme.COLORS.PRIMARY}
                  width={'100%'}
                  top={2}
                  left={5}
                />
              )}
              <Center
                width={5}
                height={5}
                bgColor={
                  STAGES[name] !== currentStage
                    ? argonTheme.COLORS.SECONDARY
                    : argonTheme.COLORS.PRIMARY
                }
                borderColor={argonTheme.COLORS.PRIMARY}
                borderWidth={1}
                borderRadius={50}>
                <Text
                  fontSize="xxs"
                  color={
                    STAGES[name] !== currentStage
                      ? argonTheme.COLORS.PRIMARY
                      : argonTheme.COLORS.WHITE
                  }>
                  {STAGES[name]}
                </Text>
              </Center>
              <Text style={styles.progress_stage_text}>{name}</Text>
            </VStack>
          </Box>
        ))}
      </HStack>
    </Center>
  );
};

export default ProgressBar;
