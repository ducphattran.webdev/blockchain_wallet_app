import React, {useState, useEffect} from 'react';
import {SafeAreaView} from 'react-native';
import {Center, Box, Text} from 'native-base';
import ProgressBar from './ProgressBar';
import Header from './Header';
import {mnemonicInitStates} from '../../context/reducer/mnemonicReducer';
import Clipboard from '@react-native-clipboard/clipboard';
import {CONFIRM_MNEMONIC} from '../../constants/routeName';
import CopiedModal from '../../components/Popup/Copied';
import Button from '../../components/common/Button';
import {argonTheme} from '../../constants';

const STAGES = mnemonicInitStates.STAGES;
const title = 'Write down your Secret Phrase';
const message =
  "This is your Secret Phrase. Write it down on a paper and keep it in a safe place. You'll be asked to re-enter this phrase (in order) on the next step.";

const Init = ({mnemonic, words, navigation}) => {
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    if (showModal) {
      setTimeout(() => setShowModal(false), 2000);
    }
  }, [showModal]);

  const copyToClipboard = () => {
    Clipboard.setString(mnemonic);
    setShowModal(true);
  };

  return (
    <>
      <SafeAreaView flex={1}>
        {/* Progress bar */}
        <ProgressBar stages={STAGES} currentStage={STAGES['Secure wallet']} />

        {/* Header */}
        <Header title={title} message={message} />

        {/* Phrase */}
        <Center flex={2} p={6}>
          <Box
            flex={1}
            borderWidth={0.5}
            borderColor={argonTheme.COLORS.PRIMARY}
            borderRadius={10}
            width="100%"
            flexWrap="wrap"
            flexDirection="row"
            py={3}
            justifyContent="center">
            {words &&
              words.map(word => (
                <Box
                  key={word.id}
                  width="40%"
                  p={1}
                  m={2}
                  borderWidth={1}
                  borderRadius={10}
                  borderColor={argonTheme.COLORS.PRIMARY}
                  alignItems="center">
                  <Text fontSize="sm">
                    {word.id + 1}. {word.value}
                  </Text>
                </Box>
              ))}
          </Box>
        </Center>

        <Center flex={0.8} width="100%" p={4}>
          {/* Button */}
          <Button
            onPress={copyToClipboard}
            style={buttonWrapper}
            color="indigo_400"
            opacity={0.5}>
            <Text color="white" fontWeight="bold">
              Copy
            </Text>
          </Button>
          <Button
            onPress={() => navigation.navigate(CONFIRM_MNEMONIC)}
            style={buttonWrapper}
            color="primary"
            opacity={0.5}>
            <Text color="white" fontWeight="bold">
              Continue
            </Text>
          </Button>
        </Center>

        {/* Modal */}
        <CopiedModal
          transparent={true}
          visible={showModal}
          animationType={'fade'}
          text={'Copied'}
        />
      </SafeAreaView>
    </>
  );
};

const buttonWrapper = {
  flex: 1,
  width: '100%',
  margin: 4,
  borderRadius: 30,
};

export default Init;
