import React from 'react';
import {Center, Text} from 'native-base';
import styles from './styles';

const Header = ({title, message}) => {
  return (
    <Center flex={0.5} p={2}>
      <Text style={styles.message_title}>{title}</Text>
      <Text style={styles.message_text}>{message}</Text>
    </Center>
  );
};

export default Header;
