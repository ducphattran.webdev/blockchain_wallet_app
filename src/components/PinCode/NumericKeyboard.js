import React from 'react';
import FAIcon from 'react-native-vector-icons/FontAwesome5';
import NumericButton from './NumericButton';
import {Center, HStack, VStack, Box} from 'native-base';

const Keyboard = ({enterCode, deleteCode}) => {
  return (
    <VStack flex={0.9}>
      <Center my={1}>
        <HStack>
          <NumericButton handlePress={enterCode} number={1} />
          <NumericButton handlePress={enterCode} number={2} />
          <NumericButton handlePress={enterCode} number={3} />
        </HStack>
      </Center>
      <Center my={1}>
        <HStack>
          <NumericButton handlePress={enterCode} number={4} />
          <NumericButton handlePress={enterCode} number={5} />
          <NumericButton handlePress={enterCode} number={6} />
        </HStack>
      </Center>
      <Center my={1}>
        <HStack>
          <NumericButton handlePress={enterCode} number={7} />
          <NumericButton handlePress={enterCode} number={8} />
          <NumericButton handlePress={enterCode} number={9} />
        </HStack>
      </Center>

      <Center my={1}>
        <HStack>
          <Box width={20} height={20} margin={3} bgColor={'transparent'}></Box>
          <NumericButton handlePress={enterCode} number={0} />

          <NumericButton
            handlePress={deleteCode}
            icon={<FAIcon size={26} name="backspace" />}
          />
        </HStack>
      </Center>
    </VStack>
  );
};

export default Keyboard;
