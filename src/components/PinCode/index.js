import React, {useEffect, useState} from 'react';
import {Pressable, SafeAreaView} from 'react-native';
import NumericKeyboard from './NumericKeyboard';
import {styles} from './styles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Box, Center, Text, HStack, Heading} from 'native-base';
import {argonTheme} from '../../constants';

const setWhiteDots = length => {
  let arr = [];
  for (let i = 0; i < length; i++) {
    arr.push({order: i, isBlackBg: false});
  }
  return arr;
};

const Index = ({
  title,
  message,
  messageType,
  length,
  setPinCode,
  pinCode,
  authWithBiometrics,
}) => {
  const [dots, setDots] = useState(() => setWhiteDots(length));

  useEffect(() => {
    if (pinCode === '') {
      setDots(() => setWhiteDots(length));
    }
  }, [pinCode]);

  const toggleBlackDot = pinCodeLength => {
    setDots(current => {
      return current.map(i => {
        if (i.order < pinCodeLength) {
          return {...i, isBlackBg: true};
        }
        return {...i, isBlackBg: false};
      });
    });
  };

  const enterCode = value => {
    if (pinCode !== undefined && pinCode.length < length && setPinCode) {
      setPinCode(prevPinCode => prevPinCode + value);
      toggleBlackDot(pinCode.length + 1);
    }
  };

  const deleteCode = () => {
    if (pinCode !== undefined && pinCode.length > 0 && setPinCode) {
      setPinCode(prevPinCode => prevPinCode.slice(0, prevPinCode.length - 1));
      toggleBlackDot(pinCode.length - 1);
    }
  };

  return (
    <>
      <SafeAreaView style={{flex: 1}}>
        <Center flex={1}>
          <Box py={3} width="100%" flex={0.15}>
            <Heading
              alignSelf={{
                base: 'center',
                md: 'flex-start',
              }}
              p={2}
              size="lg"
              color={messageType !== '' ? 'red.500' : 'black'}>
              {title}
            </Heading>
            {message && (
              <Text
                py={3}
                px={2}
                textAlign="center"
                color={messageType !== '' ? 'red.500' : 'black'}
                fontSize="sm">
                {message}
              </Text>
            )}
          </Box>

          {/* Dots */}
          <Center flex={0.05}>
            <HStack justifyContent="center">
              {dots.map((dot, index) =>
                dot !== length ? (
                  <Box
                    bgColor="red.500"
                    key={index}
                    style={[
                      styles.dot,
                      {
                        backgroundColor:
                          messageType === 'error'
                            ? argonTheme.COLORS.ERROR
                            : dot.isBlackBg
                            ? argonTheme.COLORS.BLACK
                            : argonTheme.COLORS.WHITE,
                        borderColor:
                          messageType === 'error'
                            ? argonTheme.COLORS.ERROR
                            : argonTheme.COLORS.GRAY_300,
                      },
                    ]}
                  />
                ) : (
                  <Box
                    key={index}
                    style={[
                      styles.dot,
                      {
                        marginRight: 0,
                        backgroundColor:
                          messageType === 'error'
                            ? argonTheme.COLORS.ERROR
                            : dot.isBlackBg
                            ? argonTheme.COLORS.BLACK
                            : argonTheme.COLORS.WHITE,
                        borderColor:
                          messageType === 'error'
                            ? argonTheme.COLORS.ERROR
                            : argonTheme.COLORS.GRAY_300,
                      },
                    ]}
                  />
                ),
              )}
            </HStack>
          </Center>

          <Center flex={1}>
            {/* Numeric pad */}
            <NumericKeyboard enterCode={enterCode} deleteCode={deleteCode} />
            {/* Biometrics */}
            {authWithBiometrics !== undefined && (
              <Center>
                <Pressable onPress={() => authWithBiometrics(true)}>
                  <Icon
                    name={'fingerprint'}
                    size={45}
                    color={argonTheme.COLORS.ERROR}
                  />
                </Pressable>
              </Center>
            )}
          </Center>
        </Center>
      </SafeAreaView>
    </>
  );
};

export default Index;
