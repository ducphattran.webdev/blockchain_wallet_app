import React, {useState, useEffect} from 'react';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import {Box, Center, Text, View} from 'native-base';
import {styles} from './styles';
import {SafeAreaView} from 'react-native';
import { argonTheme } from '../../constants';

const Block = ({time, blocked}) => {
  const [minutes, setMinutes] = useState();
  const [seconds, setSeconds] = useState();

  useEffect(() => {
    if (blocked) {
      if (seconds > 0 && seconds <= 60) {
        setSeconds(seconds - 1);
        return;
      }

      if (seconds === 0 && minutes !== 0) {
        setMinutes(prevMinutes => prevMinutes && prevMinutes - 1);
        setSeconds(60);
        return;
      }

      setMinutes(
        Math.floor(time / 60000) > 1 ? Math.floor(time / 60000) - 1 : 0,
      );
      setSeconds(() =>
        Math.floor((time - Math.floor(time / 60000) * 60000) / 1000),
      );
    }
    console.log(time, blocked)
  }, [time]);

  return (
    <>
      <SafeAreaView flex={1}>
        <Center flex={1}>
          <Box flex={1} width="100%" p={2} justifyContent="flex-end">
            <Text fontWeight="bold" color="red.700" textAlign="center">
              Maximum attempts reached.
            </Text>
          </Box>

          <Center flex={2}>
            <Text style={styles.timer}>
              {minutes}
              {minutes !== undefined && seconds !== undefined ? ':' : '-'}
              {seconds}
            </Text>
          </Center>

          <Center flex={1} >
            <Box bgColor="red.500">
              <FAIcon
                name="lock"
                size={30}
                color={argonTheme.COLORS.WHITE}
                style={styles.icon}
              />
            </Box>
          </Center>

          <View style={styles.message_container}>
            <Text style={styles.message}>
              To protect your information, access has been locked for{' '}
              <Text style={{color: argonTheme.COLORS.ERROR}}>
                {minutes}
                {minutes !== undefined && seconds !== undefined ? ':' : '-'}
                {seconds}
              </Text>
              .
            </Text>
            <Text style={styles.message}>Come back later and try again.</Text>
          </View>
        </Center>
      </SafeAreaView>
    </>
  );
};

export default Block;
