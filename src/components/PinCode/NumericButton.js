import React from 'react';
import {Text, Center} from 'native-base';
import Button from '../common/Button';

const NumericButton = ({handlePress, number, icon}) => {
  return (
    <Button
      style={{
        width: 80,
        height: 80,
        margin: 10,
        borderRadius: 20,
      }}
      opacity={0.3}
      color={'secondary'}
      onPress={() => handlePress(number)}>
      <Center flex={1}>
        <Text fontWeight="700" fontSize="xl" color={'black'}>
          {number !== undefined ? number : icon}
        </Text>
      </Center>
    </Button>
  );
};

export default NumericButton;
