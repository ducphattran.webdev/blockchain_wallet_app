import React, {useState} from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {trimAddress} from '../../../utils/Wallet';
import {IconButton, Box, Text, Pressable, Avatar, Badge} from 'native-base';
import {argonTheme} from '../../../constants';

const ListItem = ({group, items, onAddWallet, onViewWallet, onUpdateGroup}) => {
  const [expanded, setExpanded] = useState(
    () => items.length !== 0 && group.id === 0,
  );

  const handlePress = _group => {
    setExpanded(!expanded);
  };

  return (
    <>
      <Box m={1}>
        <Pressable
          flexDirection="row"
          justifyContent="space-between"
          alignItems="center"
          bgColor={argonTheme.COLORS.INDIGO_100}
          p={2}
          onPress={handlePress}
          _pressed={{backgroundColor: argonTheme.COLORS.GRAY_100}}>
          <Box py={1} px={2} flex={1} flexDirection="row" alignItems="center">
            <Text fontWeight="bold" color={argonTheme.COLORS.DEFAULT}>
              {group.name}
            </Text>
            <Badge
              colorScheme={'indigo'}
              ml={1}
              py={1}
              rounded="md"
              alignItems="center">
              <Text fontSize="xs" fontWeight="800">
                {items.length}
              </Text>
            </Badge>
          </Box>
          {expanded && (
            <Box height="100%" flexDirection="row" justifyContent="center">
              {group.id !== 0 && (
                <IconButton
                  onPress={() => onUpdateGroup(group)}
                  variant="solid"
                  icon={
                    <MaterialCommunityIcons
                      name={'pencil'}
                      size={24}
                      color={argonTheme.COLORS.DEFAULT}
                    />
                  }
                  bgColor={argonTheme.COLORS.INDIGO_100}
                  p={0}
                  mx={1}
                  _pressed={{backgroundColor: 'gray.300'}}
                />
              )}
              <IconButton
                onPress={() => onAddWallet(group)}
                variant="solid"
                icon={
                  <MaterialCommunityIcons
                    name={'wallet-plus'}
                    size={24}
                    color={argonTheme.COLORS.DEFAULT}
                  />
                }
                bgColor={argonTheme.COLORS.INDIGO_100}
                p={0}
                mx={1}
                _pressed={{backgroundColor: 'gray.300'}}
              />
            </Box>
          )}
        </Pressable>
        {/* Wallets */}
        {expanded && (
          <Box flex={1} bgColor={argonTheme.COLORS.SECONDARY} p={2}>
            {items.map(wallet => (
              <Pressable
                key={wallet.address}
                _pressed={{backgroundColor: 'gray.200'}}
                onPress={() => onViewWallet(wallet)}
                p={1}
                flexDirection="row"
                alignItems="center"
                justifyContent="space-evenly"
                borderRadius={10}>
                <Avatar size="md" mr={2} bgColor={argonTheme.COLORS.DEFAULT}>
                  <Text color="white">{wallet.name.slice(0, 1)}</Text>
                </Avatar>
                <Box p={1} flex={1} px={3}>
                  <Text fontSize="xs" fontWeight="bold">
                    {wallet.name}
                  </Text>
                  <Box bg="gray.300" px={4} borderRadius={20} my={1}>
                    <Text py={1} fontSize={'xxs'} fontWeight="500">
                      {trimAddress(wallet.address, 20)}
                    </Text>
                  </Box>
                </Box>

                <Box flex={0.15} alignItems="center">
                  <MaterialCommunityIcons
                    name="shield-key"
                    size={25}
                    color={argonTheme.COLORS.DEFAULT}
                  />
                </Box>
              </Pressable>
            ))}
          </Box>
        )}
      </Box>
    </>
  );
};

export default ListItem;
