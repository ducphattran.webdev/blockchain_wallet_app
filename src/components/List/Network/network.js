import React from 'react';
import {View, Text, Pressable} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {argonTheme} from '../../../constants';

const NetworkItem = ({network, style, color, onPress, showCurrent}) => {
  return (
    <Pressable
      onPress={onPress}
      style={({pressed}) => [
        styles.network_container,
        {
          opacity: pressed ? 0.5 : 1,
          backgroundColor: pressed
            ? argonTheme.COLORS.BLUE_100
            : argonTheme.COLORS.WHITE,
        },
        style,
      ]}>
      <View style={styles.current_icon_container}>
        {network.current && showCurrent && (
          <Icon name={'check'} size={12} color={argonTheme.COLORS.SUCCESS} />
        )}
      </View>
      <View style={[styles.network_color, {backgroundColor: color}]}></View>
      <Text style={styles.current_name}>{network.name}</Text>
    </Pressable>
  );
};

export default NetworkItem;
