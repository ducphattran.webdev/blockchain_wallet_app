import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Container from '../../common/Container';
import NetworkItem from './network';
import {Box, Center} from 'native-base';
import Button from '../../common/Button';
import {argonTheme} from '../../../constants';

const Network = ({networks, handleSwitching, setShowModal, selectedNet}) => {
  return (
    <>
      {selectedNet && (
        <View style={styles.container}>
          <View style={styles.wrapper}>
            <Box flex={1}>
              {/* Title */}
              <View style={styles.title_container}>
                <Icon
                  name={'public'}
                  size={20}
                  color={argonTheme.COLORS.GRAY}
                />
                <Text style={styles.title}>Networks</Text>
              </View>

              {/* Current network */}
              <NetworkItem
                network={selectedNet}
                color={selectedNet.color}
                style={{borderBottomWidth: 0}}
                showCurrent={true}
                onPress={() => handleSwitching(selectedNet)}
              />

              {/* Others */}
              <View style={styles.others_title_container}>
                <Text style={styles.others_title}>Other Networks</Text>
              </View>

              <Container style={{padding: 0}}>
                {networks.map(
                  network =>
                    network.id !== selectedNet.id && (
                      <NetworkItem
                        key={network.id}
                        network={network}
                        color={network.color}
                        onPress={() => handleSwitching(network)}
                      />
                    ),
                )}
              </Container>
            </Box>

            {/* Button */}
            <Center>
              <Button
                onPress={() => setShowModal(false)}
                style={{
                  borderRadius: 20,
                }}
                opacity={0.5}
                color={'default'}>
                <Text style={{color: argonTheme.COLORS.WHITE}}>Close</Text>
              </Button>
            </Center>
          </View>
        </View>
      )}
    </>
  );
};

export default Network;
