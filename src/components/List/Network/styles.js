import {StyleSheet} from 'react-native';
import { argonTheme } from '../../../constants';

const borderBottom = {
  borderBottomColor: argonTheme.COLORS.GRAY_300,
  borderBottomWidth: 0.5,
};

export default StyleSheet.create({
  container: {
    flex: 0.5,
    width: '90%',
    backgroundColor: argonTheme.COLORS.WHITE,
    padding: 20,
    borderRadius: 10,
  },
  wrapper: {flex: 1, padding: 0},
  title_container: {
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    ...borderBottom,
  },
  title: {
    textAlign: 'center',
    textAlignVertical: 'center',
    fontWeight: 'bold',
    marginLeft: 5,
  },
  network_container: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    ...borderBottom,
  },
  current_icon_container: {
    justifyContent: 'center',
    width: 15,
    height: 15,
  },
  network_color: {
    marginHorizontal: 10,
    marginVertical: 5,
    backgroundColor: argonTheme.COLORS.WARNING,
    width: 10,
    height: 10,
    borderRadius: 10,
  },
  current_name: {textAlign: 'left', textAlignVertical: 'center'},
  others_title_container: {
    paddingBottom: 10,
    paddingLeft: 10,
    marginTop: 10,
    ...borderBottom,
  },
  others_title: {
    textAlign: 'left',
    textAlignVertical: 'center',
    fontSize: 12,
    fontWeight: 'bold',
  },
});
