import {StyleSheet} from 'react-native';
import { argonTheme } from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    borderBottomWidth: 0.5,
    borderBottomColor: argonTheme.COLORS.GRAY_300,
    padding: 10,
  },
  timestamp: {paddingVertical: 5, color: argonTheme.COLORS.GRAY, fontSize: 12},
  wrapper: {
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  info_wrapper: {flexDirection: 'row', alignItems: 'stretch'},
  icon_wrapper: {padding: 5, justifyContent: 'center'},
  received_icon: {
    borderWidth: 1,
    borderRadius: 50,
    padding: 5,
    textAlignVertical: 'center',
    textAlign: 'center',
    borderColor: argonTheme.COLORS.BLUE,
    backgroundColor: argonTheme.COLORS.BLUE_400,
  },
  sent_icon: {
    borderWidth: 1,
    borderRadius: 50,
    padding: 5,
    textAlignVertical: 'center',
    textAlign: 'center',
    borderColor: argonTheme.COLORS.GRAY,
    backgroundColor: argonTheme.COLORS.GRAY_300,
  },
  detail_container: {padding: 5},
  detail_wrapper: {flexDirection: 'row', justifyContent: 'space-between'},
  detail_text: {fontSize: 13, textTransform: 'capitalize'},
  status_text: {fontSize: 11, color: argonTheme.COLORS.lightGreen},
  amount_wrapper: {justifyContent: 'center'},
});
