import React from 'react';
import {View, Text, Pressable} from 'react-native';
import styles from './styles';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import {argonTheme} from '../../../constants';

const Index = ({timestamp, status, type, amount, symbol, onPress}) => {
  return (
    <Pressable style={styles.container} onPress={onPress}>
      <Text style={styles.timestamp}>
        {moment(timestamp * 1000).format('MMM DD [at] HH:mm a')}
      </Text>
      <View style={styles.wrapper}>
        {/* Info */}
        <View style={styles.info_wrapper}>
          <View style={styles.icon_wrapper}>
            <MaterialIcon
              name={
                type === 'received'
                  ? 'vertical-align-bottom'
                  : 'vertical-align-top'
              }
              color={
                type === 'received'
                  ? argonTheme.COLORS.WHITE
                  : argonTheme.COLORS.BLACK
              }
              size={15}
              style={
                type === 'received' ? styles.received_icon : styles.sent_icon
              }
            />
          </View>
          <View style={styles.detail_container}>
            <View style={styles.detail_wrapper}>
              <Text style={styles.detail_text}>{type}</Text>
              <Text style={{marginLeft: 5}}>
                {symbol && symbol.toUpperCase()}
              </Text>
            </View>
            <Text
              style={[
                styles.status_text,
                {
                  color: status
                    ? argonTheme.COLORS.INFO
                    : argonTheme.COLORS.ERROR,
                },
              ]}>
              {status ? 'Success' : 'Fail'}
            </Text>
          </View>
        </View>
        {/* Amount */}
        <View style={styles.amount_wrapper}>
          <Text>
            {amount} {symbol && symbol.toUpperCase()}
          </Text>
        </View>
      </View>
    </Pressable>
  );
};

export default Index;
