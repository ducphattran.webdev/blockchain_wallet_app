import React from 'react';
import QRCode from 'react-native-qrcode-svg';

const Index = ({size, value}) => {
  return <QRCode size={size} value={value} />;
};

export default Index;
