import React from 'react';
import {View, Text, Modal, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {argonTheme} from '../../constants';

const CopiedModal = ({visible, animationType, transparent, text}) => {
  return (
    <Modal
      transparent={transparent}
      visible={visible}
      animationType={animationType}>
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Icon name={'check-circle'} color={styles.message.color} size={30} />
          <Text style={styles.message}>{text}</Text>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapper: {
    backgroundColor: argonTheme.COLORS.TRANSPARENT,
    padding: 15,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    borderColor: argonTheme.COLORS.BLACK,
  },
  message: {
    color: argonTheme.COLORS.WHITE,
    fontSize: 18,
    textAlignVertical: 'center',
  },
});

export default CopiedModal;
