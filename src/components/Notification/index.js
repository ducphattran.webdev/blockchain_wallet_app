import React from 'react';
import {View, Text, Modal} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {argonTheme} from '../../constants';

const NotificationModal = ({visible, text, status}) => {
  return (
    <Modal transparent={true} visible={visible} animationType={'fade'}>
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.title_container}>
            <Icon
              name={'check-circle'}
              color={
                status === 'SUCCESSFUL'
                  ? argonTheme.COLORS.SUCCESS
                  : status === 'FAILED'
                  ? argonTheme.COLORS.ERROR
                  : argonTheme.COLORS.WARNING
              }
              size={20}
            />
            <Text
              style={[
                styles.title_text,
                {
                  color:
                    status === 'SUCCESSFUL'
                      ? argonTheme.COLORS.SUCCESS
                      : status === 'FAILED'
                      ? argonTheme.COLORS.ERROR
                      : argonTheme.COLORS.WARNING,
                },
              ]}>
              {status}
            </Text>
          </View>
          <Text style={styles.message}>{text}</Text>
        </View>
      </View>
    </Modal>
  );
};

export default NotificationModal;
