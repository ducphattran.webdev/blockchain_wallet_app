import {StyleSheet} from 'react-native';
import {argonTheme} from '../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapper: {
    backgroundColor: argonTheme.COLORS.TRANSPARENT,
    padding: 15,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 20,
    borderColor: argonTheme.COLORS.GRAY,
  },
  title_container: {flexDirection: 'row', padding: 5},
  title_text: {
    fontSize: 16,
    textTransform: 'uppercase',
    color: argonTheme.COLORS.SUCCESS,
    marginLeft: 5,
  },
  message: {
    color: argonTheme.COLORS.WHITE,
    fontSize: 14,
    textAlignVertical: 'center',
  },
});
