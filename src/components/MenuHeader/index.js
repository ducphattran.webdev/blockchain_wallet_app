import React, {useEffect, useState} from 'react';
import {Text, SafeAreaView, StyleSheet} from 'react-native';
import SelectNetworkModal from '../Modal/SelectNetwork';
import {useGlobalContext} from '../../context/GlobalContext';
import {changeNetwork} from '../../context/action/Network/changeNetwork';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import {argonTheme} from '../../constants';
import {Box, Center, Pressable} from 'native-base';

const Header = ({navigation}) => {
  const {networkState, networkDispatch, walletState} = useGlobalContext();
  const [networks, setNetworks] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [selectedNet, setSelectedNet] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (
      networkState.done &&
      networkState.networks &&
      walletState.done &&
      walletState.wallets.length !== 0
    ) {
      setNetworks(networkState.networks);
      setSelectedNet(networkState.networks.find(n => n.current));
      setLoading(false);
    }
  }, [walletState, networkState]);

  const handleSwitching = network => {
    setLoading(true);
    const selected_index = networks.findIndex(n => network.id === n.id);
    if (selected_index !== -1) {
      setNetworks(prevArr =>
        prevArr.map(n => {
          if (n.id === networks[selected_index].id) {
            return {...n, current: true};
          }

          return {...n, current: false};
        }),
      );
      setSelectedNet({...networks[selected_index], current: true});
      changeNetwork({id: networks[selected_index].id})(networkDispatch);
      setShowModal(false);
      setLoading(false);
    }
  };

  return (
    <SafeAreaView>
      {!loading && (
        <Center
          bgColor={argonTheme.COLORS.PRIMARY}
          flexDirection="row"
          justifyContent="space-evenly"
          p={2}>
          <Pressable
            onPress={() => setShowModal(!showModal)}
            _pressed={{opacity: 0.5}}
            alignItems="center">
            <Box flexDirection="row" alignItems="center">
              <EntypoIcon
                size={12}
                name={'network'}
                color={argonTheme.COLORS.WHITE}
              />
              <Text style={styles.title_page}> Network</Text>
            </Box>
            <Box>
              {selectedNet && (
                <Box flexDirection="row" alignItems="center">
                  <Box
                    width={2}
                    height={2}
                    mr={1}
                    borderRadius={10}
                    bgColor={
                      selectedNet.color
                        ? selectedNet.color
                        : argonTheme.COLORS.BLUE
                    }></Box>
                  <Text style={styles.network_name}>{selectedNet.name}</Text>
                </Box>
              )}
            </Box>
          </Pressable>

          <SelectNetworkModal
            visible={showModal}
            networks={networks}
            handleSwitching={handleSwitching}
            setShowModal={setShowModal}
            selectedNet={selectedNet}
          />
        </Center>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  title_page: {
    fontSize: 11,
    fontWeight: '600',
    color: argonTheme.COLORS.WHITE,
  },
  network_name: {
    fontSize: 14,
    color: argonTheme.COLORS.WHITE,
    fontWeight: 'bold',
  },
});

export default Header;
