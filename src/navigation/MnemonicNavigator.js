import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import InitScreen from '../screens/Mnemonic/InitScreen';
import ConfirmScreen from '../screens/Mnemonic/ConfirmScreen';
import {mnemonicInitStates} from '../context/reducer/mnemonicReducer';
import {CONFIRM_MNEMONIC, INIT_MNEMONIC} from '../constants/routeName';
import {useGlobalContext} from '../context/GlobalContext';
import {argonTheme} from '../constants';

const Stack = createStackNavigator();
const STAGES = mnemonicInitStates.STAGES;
const MnemonicNavigator = ({navigation}) => {
  const {mnemonicState} = useGlobalContext();

  useEffect(() => {
    if (mnemonicState.done) {
      navigation.setOptions({headerShown: false});
    }
  }, []);

  return (
    <Stack.Navigator initialRouteName={INIT_MNEMONIC}>
      <Stack.Screen
        name={INIT_MNEMONIC}
        component={InitScreen}
        options={{
          title: Object.keys(STAGES)[0],
          ...headerStyles,
        }}
      />
      <Stack.Screen
        name={CONFIRM_MNEMONIC}
        component={ConfirmScreen}
        options={{
          title: Object.keys(STAGES)[1],
          ...headerStyles,
        }}
      />
    </Stack.Navigator>
  );
};

const headerStyles = {
  headerStyle: {
    backgroundColor: argonTheme.COLORS.PRIMARY,
  },
  headerTintColor: argonTheme.COLORS.WHITE,
  headerTitleStyle: {
    fontWeight: 'bold',
  },
  headerTitleAlign: 'center',
};

export default MnemonicNavigator;
