import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {TAB_NAVIGATOR} from '../../constants/routeName';
import MenuHeader from '../../components/MenuHeader';
import TabNavigator from './TabNavigator';

const Drawer = createDrawerNavigator();
const renderHeader = navigation => {
  return <MenuHeader navigation={navigation} />;
};

function MyTabs() {
  return (
    <Drawer.Navigator
      initialRouteName={TAB_NAVIGATOR}
      screenOptions={{
        header: ({navigation}) => renderHeader(navigation),
      }}>
      <Drawer.Screen name={TAB_NAVIGATOR} component={TabNavigator} />
    </Drawer.Navigator>
  );
}

export default MyTabs;
