import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import WalletsScreen from '../../screens/Wallet';
import DetailScreen from '../../screens/Wallet/detail';
import {WALLETS, WALLET_DETAIL} from '../../constants/routeName';

const Stack = createStackNavigator();

const WalletNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName={WALLETS}
      screenOptions={{headerShown: false}}>
      <Stack.Screen name={WALLETS} component={WalletsScreen} />
      <Stack.Screen name={WALLET_DETAIL} component={DetailScreen} />
    </Stack.Navigator>
  );
};

export default WalletNavigator;
