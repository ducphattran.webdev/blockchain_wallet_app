import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {SETTING, SETTING_NETWORK} from '../../../constants/routeName';
import SettingScreen from '../../../screens/Setting';
import NetworkNavigator from './NetworkNavigator';

const Stack = createStackNavigator();

const SettingNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName={SETTING}
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name={SETTING} component={SettingScreen} />
      <Stack.Screen name={SETTING_NETWORK} component={NetworkNavigator} />
    </Stack.Navigator>
  );
};

export default SettingNavigator;
