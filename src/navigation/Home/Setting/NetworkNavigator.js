import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {SETTING_NETWORK_LIST,SETTING_NETWORK_FORM} from '../../../constants/routeName';
import NetworkListScreen from '../../../screens/Setting/Network/List';
import NetworkFormScreen from '../../../screens/Setting/Network/Form';

const Stack = createStackNavigator();

const NetworkNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName={SETTING_NETWORK_LIST}
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name={SETTING_NETWORK_LIST} component={NetworkListScreen} />
      <Stack.Screen name={SETTING_NETWORK_FORM} component={NetworkFormScreen} />
    </Stack.Navigator>
  );
};

export default NetworkNavigator;
