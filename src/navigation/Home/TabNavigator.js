import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  WALLET_NAVIGATOR,
  TRANSFER,
  SETTING_NAVIGATOR,
} from '../../constants/routeName';
import WalletNavigator from './WalletNavigator';
import TransferScreen from '../../screens/Transfer';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {argonTheme} from '../../constants';
import SettingNavigator from './Setting/SettingNavigator';

const Tab = createBottomTabNavigator();

const HomeTab = () => {
  return (
    <Tab.Navigator
      initialRouteName={WALLET_NAVIGATOR}
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: argonTheme.COLORS.BLOCK,
        },
        tabBarInactiveTintColor: argonTheme.COLORS.BLACK,
        tabBarActiveTintColor: argonTheme.COLORS.WHITE,
        tabBarActiveBackgroundColor: argonTheme.COLORS.PRIMARY,
        tabBarInactiveBackgroundColor: argonTheme.COLORS.BLOCK,
      }}>
      <Tab.Screen
        name={WALLET_NAVIGATOR}
        component={WalletNavigator}
        options={{
          tabBarLabel: 'Wallets',
          tabBarIcon: ({color}) => (
            <MaterialIcon
              name="account-balance-wallet"
              color={color}
              size={20}
            />
          ),
        }}
      />
      <Tab.Screen
        name={TRANSFER}
        component={TransferScreen}
        options={{
          tabBarLabel: 'Transfer',
          tabBarIcon: ({color, size}) => (
            <MaterialIcon name="list-alt" color={color} size={20} />
          ),
        }}
      />
      <Tab.Screen
        name={SETTING_NAVIGATOR}
        component={SettingNavigator}
        options={{
          tabBarLabel: 'Setting',
          tabBarIcon: ({color, size}) => (
            <MaterialIcon name="settings" color={color} size={20} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default HomeTab;
