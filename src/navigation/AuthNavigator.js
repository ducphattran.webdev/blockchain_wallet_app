import React, {useEffect} from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import EnterScreen from '../screens/Pin/EnterScreen';
import SetupScreen from '../screens/Pin/SetupScreen';
import {
  LOGIN,
  REGISTER,
  MNEMONIC_NAVIGATOR,
  INIT_MNEMONIC,
  HOME_NAVIGATOR,
  TAB_NAVIGATOR,
  AUTH_NAVIGATOR,
  INIT_NAVIGATOR,
} from '../constants/routeName';
import {useGlobalContext} from '../context/GlobalContext';
import {loginWithPIN} from '../context/action/Auth/loginWithPIN';
import {createPIN} from '../context/action/PIN/createPIN';
import {navigationRef, navigate} from './RootNavigator';

const Drawer = createDrawerNavigator();

const AuthNavigator = () => {
  const {walletState, pinState, authDispatch, mnemonicState, pinDispatch} =
    useGlobalContext();

  const handleLogin = () => {
    loginWithPIN()(authDispatch);
    if (mnemonicState.isInit) {
      navigate(MNEMONIC_NAVIGATOR, {
        screen: INIT_MNEMONIC,
      });
    } else {
      if (walletState.done && walletState.wallets.length !== 0) {
        navigate(HOME_NAVIGATOR, {screen: TAB_NAVIGATOR});
      } else {
        navigate(INIT_NAVIGATOR);
      }
    }
  };

  const handleRegister = pinCode => {
    createPIN({pinCode})(pinDispatch);
    navigate(AUTH_NAVIGATOR, {screen: LOGIN, params: {biometricsOff: true}});
  };

  return (
    <Drawer.Navigator
      initialRouteName={pinState.PIN ? LOGIN : REGISTER}
      screenOptions={{headerShown: false}}>
      <Drawer.Screen name={LOGIN}>
        {({route, navigation}) => (
          <EnterScreen
            handleSubmit={handleLogin}
            route={route}
            navigation={navigation}
          />
        )}
      </Drawer.Screen>
      <Drawer.Screen name={REGISTER}>
        {() => <SetupScreen handleSubmit={handleRegister} />}
      </Drawer.Screen>
    </Drawer.Navigator>
  );
};

export default AuthNavigator;
