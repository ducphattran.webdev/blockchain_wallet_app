import React, {useState, useEffect} from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {useGlobalContext} from '../context/GlobalContext';
import {
  HOME_NAVIGATOR,
  MNEMONIC_NAVIGATOR,
  AUTH_NAVIGATOR,
  INIT_NAVIGATOR,
} from '../constants/routeName';
import MnemonicNavigator from './MnemonicNavigator';
import HomeNavigator from './Home';
import AuthNavigator from './AuthNavigator';
import InitScreen from '../screens/Init';
import SplashScreen from 'react-native-splash-screen';
import Keychain from 'react-native-keychain'

const DrawerNavigator = createDrawerNavigator();

const Index = () => {
  const {pinState, mnemonicState, walletState, networkState} =
    useGlobalContext();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // AsyncStorage.clear();
    // Keychain.resetGenericPassword({service: 'MNEMONIC'});
    // Keychain.resetGenericPassword({service: 'PIN'});
  }, []);

  useEffect(() => {
    if (
      pinState.done &&
      mnemonicState.done &&
      walletState.done &&
      networkState.done &&
      networkState.networks
    ) {
      setLoading(false)
      SplashScreen.hide();
    }
  }, [mnemonicState, pinState, networkState, walletState]);

  return (
    <>
      {!loading && (
        <DrawerNavigator.Navigator
          screenOptions={{headerShown: false}}
          initialRouteName={AUTH_NAVIGATOR}>
          {/* AUTH_NAVIGATOR */}
          <DrawerNavigator.Screen
            name={AUTH_NAVIGATOR}
            component={AuthNavigator}
          />
          {/* MNEMONIC_NAVIGATOR */}
          <DrawerNavigator.Screen
            name={MNEMONIC_NAVIGATOR}
            component={MnemonicNavigator}
          />
          {/* INIT_NAVIGATOR */}
          <DrawerNavigator.Screen
            name={INIT_NAVIGATOR}
            component={InitScreen}
          />
          {/* HOME_NAVIGATOR */}
          <DrawerNavigator.Screen
            name={HOME_NAVIGATOR}
            component={HomeNavigator}
          />
        </DrawerNavigator.Navigator>
      )}
    </>
  );
};

export default Index;
